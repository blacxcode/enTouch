package com.blacxcode.entouch.config;

/**
 * Created by blacXcode on 5/8/2017.
 */

public class ETConfig {
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 0; //100
    public static final int NOTIFICATION_ID_BIG_IMAGE = 1; //101

    public static final String SHARED_PREF = "prefs.config.entouchdev";

    public static final String DB_ENTOUCH = "entouchdb";
    public static final String DB_ENTOUCHCLIENT = "entouchclient";
    public static final String DB_ENTOUCHDATA = "entouchdata";
    public static final String DB_ENTOUCHDEVICE = "entouchdevice";

    public static final String CONFIG_DIRNAME = "/.enTouchConf";
    public static final String CONFIG_FILENAME = "device.conf";
    public static final String CONFIG_KEY = "@blacxcode";

    public static final String AUTH_USERNAME = "entouch";
    public static final String AUTH_PASSWORD = "entouch";
}
