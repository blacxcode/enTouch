package com.blacxcode.entouch.ui.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.activity.ETDevicesActivity;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.data.ETUserlistData;
import com.blacxcode.entouch.fragment.ETUserlistFragment;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by blacXcode on 2/2/2018.
 */

public class ETUserlistAdapter extends RecyclerView.Adapter<ETUserlistAdapter.UserlistViewHolder> {
    protected SharedPreferences sharedPreferences;

    protected ETUserlistFragment fragment;
    protected ETDevicesActivity devicesActivity;

    private List<ETUserlistData> userList;
    private String getThemes;


    public ETUserlistAdapter(Context context, List<ETUserlistData> userList, ETUserlistFragment fragment) {
        if (context instanceof ETDevicesActivity) {
            devicesActivity = (ETDevicesActivity) context;
        }

        this.userList = userList;
        this.userList.addAll(userList);
        this.fragment = fragment;
    }

    @Override
    public UserlistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_userlist, parent, false);
        UserlistViewHolder viewHolder = new UserlistViewHolder(view);

        sharedPreferences = devicesActivity.getSharedPreferences(ETConfig.SHARED_PREF, Context.MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final UserlistViewHolder holder, int position) {
        final ETUserlistData userlistData = userList.get(position);

        String ContentInfo = "";

        holder._userid = userlistData.getUserId();
        holder._devicekey = userlistData.getDevicekey();
        holder._devicename = userlistData.getDeviceName();
        holder._devicestatus = userlistData.getDeviceStatus();
        holder._statusdate = userlistData.getStatusDate();
        holder._username = userlistData.getUsername();

        holder.userlistCardview.setCardBackgroundColor(holder.itemView.getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        holder.devicestatus.setText(userlistData.getDeviceStatus());
        holder.userInfoCardview.setBackgroundColor(holder.itemView.getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        holder.userInfo.setMovementMethod(LinkMovementMethod.getInstance());

        holder.acceptBtn.setTextColor(holder.itemView.getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        holder.rejectBtn.setTextColor(holder.itemView.getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        try {
            if (holder._devicestatus.equals("approved")) {
                Glide.with(devicesActivity)
                        .load(R.drawable.ic_phonelink_ring_white_48dp)
                        .apply(RequestOptions.overrideOf(150, 150))
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(holder.userListPic);
                holder.acceptBtn.setEnabled(false);
                holder.rejectBtn.setEnabled(true);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    ContentInfo = "<b>" + holder.itemView.getResources().getString(R.string.userlist_username) + "</b>" + holder._username +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicename) + "</b>" + holder._devicename +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicestatus) + "</b>" + holder._devicestatus +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_approveddate) + "</b>" + holder._statusdate;
                    holder.userInfo.setText(Html.fromHtml(ContentInfo, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    ContentInfo = "<b>" + holder.itemView.getResources().getString(R.string.userlist_username) + "</b>" + holder._username +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicename) + "</b>" + holder._devicename +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicestatus) + "</b>" + holder._devicestatus +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_approveddate) + "</b>" + holder._statusdate;
                    holder.userInfo.setText(Html.fromHtml(ContentInfo));
                }
            } else if (holder._devicestatus.equals("rejected")) {
                Glide.with(devicesActivity)
                        .load(R.drawable.ic_phonelink_erase_white_48dp)
                        .apply(RequestOptions.overrideOf(150, 150))
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(holder.userListPic);
                holder.acceptBtn.setEnabled(true);
                holder.rejectBtn.setEnabled(false);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    ContentInfo = "<b>" + holder.itemView.getResources().getString(R.string.userlist_username) + "</b>" + holder._username +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicename) + "</b>" + holder._devicename +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicestatus) + "</b>" + holder._devicestatus +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_rejectdate) + "</b>" + holder._statusdate;
                    holder.userInfo.setText(Html.fromHtml(ContentInfo, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    ContentInfo = "<b>" + holder.itemView.getResources().getString(R.string.userlist_username) + "</b>" + holder._username +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicename) + "</b>" + holder._devicename +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicestatus) + "</b>" + holder._devicestatus +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_rejectdate) + "</b>" + holder._statusdate;
                    holder.userInfo.setText(Html.fromHtml(ContentInfo));
                }
            } else {
                Glide.with(devicesActivity)
                        .load(R.drawable.ic_phonelink_lock_white_48dp)
                        .apply(RequestOptions.overrideOf(150, 150))
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(holder.userListPic);
                holder.acceptBtn.setEnabled(true);
                holder.rejectBtn.setEnabled(true);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    ContentInfo = "<b>" + holder.itemView.getResources().getString(R.string.userlist_username) + "</b>" + holder._username +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicename) + "</b>" + holder._devicename +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicestatus) + "</b>" + holder._devicestatus +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_requestdate) + "</b>" + holder._statusdate;
                    holder.userInfo.setText(Html.fromHtml(ContentInfo, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    ContentInfo = "<b>" + holder.itemView.getResources().getString(R.string.userlist_username) + "</b>" + holder._username +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicename) + "</b>" + holder._devicename +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_devicestatus) + "</b>" + holder._devicestatus +
                            "<br><b>" + holder.itemView.getResources().getString(R.string.userlist_requestdate) + "</b>" + holder._statusdate;
                    holder.userInfo.setText(Html.fromHtml(ContentInfo));
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.username.setText(Html.fromHtml(userlistData.getUsername(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.username.setText(Html.fromHtml(userlistData.getUsername()));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            if (holder.acceptBtn.isEnabled()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.acceptBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
                } else {
                    holder.acceptBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.acceptBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
                } else {
                    holder.acceptBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            if (holder.rejectBtn.isEnabled()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.rejectBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
                } else {
                    holder.rejectBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.rejectBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
                } else {
                    holder.rejectBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    public void setFilter(List<ETUserlistData> userlistData) {
        userList = new ArrayList<>();
        userList.addAll(userlistData);
        notifyDataSetChanged();
    }

    class UserlistViewHolder extends RecyclerView.ViewHolder {
        CardView userlistCardview, userInfoCardview, btnCardview;
        TextView username, devicestatus, userInfo;
        ImageView userListPic, userDelete;
        ImageButton userInfoExpand;
        Button acceptBtn, rejectBtn;

        ETJSONParseData parseData;
        ETUserlistData userlistData;
        DatabaseReference mFirebaseDB;
        FirebaseDatabase mFirebaseInstance;

        String _userid = "";
        String _username = "";
        String _devicestatus = "";
        String _devicekey = "";
        String _devicename = "";
        String _statusdate = "";

        UserlistViewHolder(View itemView) {
            super(itemView);

            userlistCardview = itemView.findViewById(R.id.userlistCardview);
            userInfoCardview = itemView.findViewById(R.id.userInfoCardview);
            btnCardview = itemView.findViewById(R.id.btnCardview);
            userInfoExpand = itemView.findViewById(R.id.userInfoExpand);
            username = itemView.findViewById(R.id.usernameText);
            userListPic = itemView.findViewById(R.id.userListPic);
            userDelete = itemView.findViewById(R.id.userDelete);
            acceptBtn = itemView.findViewById(R.id.acceptBtn);
            rejectBtn = itemView.findViewById(R.id.rejectBtn);
            devicestatus = itemView.findViewById(R.id.statusText);
            userInfo = itemView.findViewById(R.id.userInfo);

            parseData = new ETJSONParseData(devicesActivity);
            userlistData = new ETUserlistData();
            mFirebaseInstance = FirebaseDatabase.getInstance();
            mFirebaseDB = mFirebaseInstance.getReference(ETConfig.DB_ENTOUCH);

            username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.vibrate));
                }
            });

            userInfoExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userInfoExpand.setSelected(!userInfoExpand.isSelected());

                    if (userInfoExpand.isSelected()) {
                        AnimationSet animationSet = new AnimationSet(false);
                        animationSet.addAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.rotate_up));
                        animationSet.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                userInfoCardview.setAlpha(1.0f);
                                userInfoCardview.setVisibility(View.VISIBLE);
                                userInfoCardview.setAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.show_up));
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        v.setAnimation(animationSet);
                        v.startAnimation(animationSet);
                    } else {
                        AnimationSet animationSet = new AnimationSet(false);
                        animationSet.addAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.rotate_down));
                        animationSet.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                userInfoCardview.animate().alpha(0.0f).setDuration(1000).setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        super.onAnimationStart(animation);
                                        //userInfoExpand.setSelected(true);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        //userInfoExpand.setSelected(false);
                                        userInfoCardview.setVisibility(View.GONE);
                                    }
                                });
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        v.setAnimation(animationSet);
                        v.startAnimation(animationSet);
                    }
                }
            });

            acceptBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                    if (fragment.checkConnection()) {
                        //Toast.makeText(context, "accept user ...", Toast.LENGTH_LONG).show();
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
                        _statusdate = simpledateformat.format(calendar.getTime());

                        String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHCLIENT).child(_userid).child("devicestatus")
                                .setValue("approved");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHCLIENT).child(_userid).child("statusdate")
                                .setValue(_statusdate);

                        fragment.retrieveData();
                        setThemes();
                        parseData.writeLogActivity("logact.log", "Accept user : " + username.getText().toString());
                        parseData.writeLogActivity("logact-in.log", "Terima pengguna : " + username.getText().toString());
                        parseData.writeLogActivity("logact-ja.log", "ユーザーを承認する : " + username.getText().toString());
                    } else {
                        Snackbar.make(v, itemView.getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });

            rejectBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                    if (fragment.checkConnection()) {
                        //Toast.makeText(context, "reject user ...", Toast.LENGTH_LONG).show();
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
                        _statusdate = simpledateformat.format(calendar.getTime());

                        String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHCLIENT).child(_userid).child("devicestatus")
                                .setValue("rejected");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHCLIENT).child(_userid).child("statusdate")
                                .setValue(_statusdate);

                        fragment.retrieveData();
                        setThemes();
                        parseData.writeLogActivity("logact.log", "Reject user : " + username.getText().toString());
                        parseData.writeLogActivity("logact-in.log", "Tolak pengguna : " + username.getText().toString());
                        parseData.writeLogActivity("logact-ja.log", "ユーザーを拒否する : " + username.getText().toString());
                    } else {
                        Snackbar.make(v, itemView.getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }

                }
            });

            userDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fragment.checkConnection()) {
                        AnimationSet animationSet = new AnimationSet(false);
                        animationSet.addAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.shake));
                        animationSet.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");
                                mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHCLIENT).child(_userid).removeValue();

                                fragment.retrieveData();
                                setThemes();
                                parseData.writeLogActivity("logact.log", "Delete user : " + username.getText().toString());
                                parseData.writeLogActivity("logact-in.log", "Hapus pengguna : " + username.getText().toString());
                                parseData.writeLogActivity("logact-ja.log", "ユーザーを削除 : " + username.getText().toString());
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        v.setAnimation(animationSet);
                        v.startAnimation(animationSet);
                    } else {
                        Snackbar.make(v, itemView.getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }


        private void setThemes() {
            acceptBtn.setTextColor(itemView.getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
            rejectBtn.setTextColor(itemView.getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

            if (acceptBtn.isEnabled()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    acceptBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
                } else {
                    acceptBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    acceptBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
                } else {
                    acceptBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
                }
            }

            if (rejectBtn.isEnabled()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    rejectBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
                } else {
                    rejectBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    rejectBtn.setBackgroundTintList(ColorStateList.valueOf(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
                } else {
                    rejectBtn.setBackgroundColor(devicesActivity.getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
                }
            }
        }
    }
}
