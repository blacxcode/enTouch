package com.blacxcode.entouch.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import java.util.Locale;

public class ETSetLocalLanguage {
    private static String TAG = ETSetLocalLanguage.class.getSimpleName();

    private static final int TAG_EN = 0;
    private static final int TAG_IN = 1;
    private static final int TAG_JA = 2;

    private Context context;

    public ETSetLocalLanguage(Context context) {
        this.context = context;
    }

    public String getLocale(int getLocale) {
        switch (getLocale) {
            case TAG_EN:
                return "en";

            case TAG_IN:
                return "id";

            case TAG_JA:
                return "ja";

            default:
                return "en";
        }
    }

    /*change language at Run-time*/
    //use method like that:
    //setLocale("en");
    @SuppressWarnings("deprecation")
    public Locale setLocale(String localCode) {
        String localCodeLowerCase = localCode.toLowerCase();

        //Locale locale = new Locale(localCodeLowerCase);
        //Locale.setDefault(locale);

        Resources resources = context.getResources();
        Configuration overrideConfiguration = resources.getConfiguration();
        //DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Locale overrideLocale = new Locale(localCodeLowerCase);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            overrideConfiguration.setLocale(overrideLocale);
        } else {
            overrideConfiguration.locale = overrideLocale;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.createConfigurationContext(overrideConfiguration);
        } else {
            resources.updateConfiguration(overrideConfiguration, null);
        }

        return overrideLocale;
    }

//    @SuppressWarnings("deprecation")
//    public Locale getSystemLocaleLegacy(Configuration config){
//        return config.locale;
//    }
//
//    @TargetApi(Build.VERSION_CODES.N)
//    public Locale getSystemLocale(Configuration config){
//        return config.getLocales().get(0);
//    }
//
//    @SuppressWarnings("deprecation")
//    private static  void setSystemLocaleLegacy(Configuration config, Locale locale){
//        config.locale = locale;
//    }
//
//    @TargetApi(Build.VERSION_CODES.N)
//    private static void setSystemLocale(Configuration config, Locale locale){
//        config.setLocale(locale);
//    }
//
//    public static void setLocale(String lang, Context mContext){
//        Locale locale = new Locale(lang);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            setSystemLocale(config, locale);
//        }else{
//            setSystemLocaleLegacy(config, locale);
//        }
//        mContext.getApplicationContext().getResources().updateConfiguration(config,
//                mContext.getResources().getDisplayMetrics());
//    }
}
