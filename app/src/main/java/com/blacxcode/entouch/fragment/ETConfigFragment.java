package com.blacxcode.entouch.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.format.Formatter;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.activity.ETAppsActivity;
import com.blacxcode.entouch.activity.ETDevicesActivity;
import com.blacxcode.entouch.activity.ETHelpActivity;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.service.ETConectivityReceiver;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.WIFI_SERVICE;

/**
 * Created by blacXcode on 2/1/2018.
 */

public class ETConfigFragment extends Fragment implements ETConectivityReceiver.ETConectivityReceiverListener {
    private static final String TAG = ETConfigFragment.class.getSimpleName();

    private static final int ACCESS_COARSE_LOCATION_REQUEST_CODE = 100;

    protected SharedPreferences sharedPreferences;

    protected ETDevicesActivity devicesActivity;
    protected ETJSONParseData parseData;

    protected LinearLayout configFragment;
    protected CardView wifiTitleCardView, wifiCardView, groupWifiCardView, groupSSIDNameCardView, essidCardView, groupSSIDPassCardView, groupIPAddrCardView, ipAddrCardView,
            deviceInfoTitleCardView, deviceInfoCardView, groupDeviceStatusCardView, groupDeviceNameCardView, groupDeviceMsgCardView, statusCardView,
    //databaseTitleCardview, databaseCardview, groupHostnameCardView, groupDatabaseCardView, groupServerCardView,
            configTitleCardView, configCardView, readConfCardView, readConfBtnCardView, writeConfCardView, writeConfBtnCardView, clearConfCardView, clearConfBtnCardView,
            authConfTitleCardView, authConfCardView, groupAuthUserCardView, groupAuthPassCardView, authUpdateCardView;
    protected EditText epassword, eusername, emessage,
            authUserInput, authPassInput;
    //ehostname, edatabase_secret, eserver_key;
    protected Button readConfigBtn, writeConfigBtn, clearConfigBtn,
            authUpdateBtn;
    protected ImageView refreshSSID, helpSSID,
            statusDevice, helpDevice,
            helpConfig,
            helpAuth;
    protected ImageButton showPassWifiBtn,
            showPassAuthBtn;
    protected TextView wifiTitle, wifiNetworkText, ssidNameText, ebssid, ssidPasswordText, ipAddrText, eipaddr,
            deviceInfoTitle, deviceStatusText, estatus, deviceNameText, deviceMessageText,
    //dbhostnameText, dbsecretText, serverkeyText,
            configTitle, readConfText, writeConfText, clearConfText,
            authConfTitle, authUserText, authPassText;
    protected ProgressDialog progressDialog;
    protected CheckBox AuthRemember;
    protected MaterialSpinner spinnerSSID;
    protected View view;

    protected WifiManager mWifiManager;
    protected WifiInfo mWifiInfo;
    protected WifiScanReceiver mWifiScanReceiver;

    protected DatabaseReference mFirebaseDB;
    protected FirebaseDatabase mFirebaseInstance;

    protected Locale locale = null;

    private String getThemes = "ETColorBlueGrey", getLang = "en";
    private boolean wifiDefaultState = false;

    public ETConfigFragment() {
        // Required empty public constructor
    }

    public static ETConfigFragment newInstance() {
        return new ETConfigFragment();
    }

    @Override
    public void onAttach(Context context) {
        Log.e(TAG, "onAttach");
        super.onAttach(context);

        if (context instanceof ETDevicesActivity) {
            devicesActivity = (ETDevicesActivity) context;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            devicesActivity.getResources().updateConfiguration(newConfig, devicesActivity.getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceStat) {
        Log.e(TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragment_config, container, false);
        configFragment = view.findViewById(R.id.configFragment);

        sharedPreferences = devicesActivity.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        parseData = new ETJSONParseData(devicesActivity);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDB = mFirebaseInstance.getReference(ETConfig.DB_ENTOUCH);

        wifiTitleCardView = view.findViewById(R.id.wifiTitleCardView);
        wifiTitle = view.findViewById(R.id.wifiTitle);
        helpSSID = view.findViewById(R.id.helpSSID);
        wifiCardView = view.findViewById(R.id.wifiCardView);
        groupWifiCardView = view.findViewById(R.id.groupWifiCardView);
        wifiNetworkText = view.findViewById(R.id.wifiNetworkText);
        refreshSSID = view.findViewById(R.id.refreshSSID);
        essidCardView = view.findViewById(R.id.essidCardView);
        groupSSIDNameCardView = view.findViewById(R.id.groupSSIDNameCardView);
        ssidNameText = view.findViewById(R.id.ssidNameText);
        ebssid = view.findViewById(R.id.ebssid);
        groupSSIDPassCardView = view.findViewById(R.id.groupSSIDPassCardView);
        ssidPasswordText = view.findViewById(R.id.ssidPasswordText);
        epassword = view.findViewById(R.id.epassword);
        showPassWifiBtn = view.findViewById(R.id.showPassWifiBtn);
        ipAddrCardView = view.findViewById(R.id.ipAddrCardView);
        groupIPAddrCardView = view.findViewById(R.id.groupIPAddrCardView);
        ipAddrText = view.findViewById(R.id.ipAddrText);
        eipaddr = view.findViewById(R.id.eipaddr);

        mWifiManager = (WifiManager) devicesActivity.getBaseContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        mWifiInfo = mWifiManager.getConnectionInfo();
        mWifiScanReceiver = new WifiScanReceiver();

        spinnerSSID = view.findViewById(R.id.spinnerSSID);
        //spinnerSSID.setItems(SSID);

        deviceInfoTitleCardView = view.findViewById(R.id.deviceInfoTitleCardView);
        deviceInfoTitle = view.findViewById(R.id.deviceInfoTitle);
        helpDevice = view.findViewById(R.id.helpDevice);
        deviceInfoCardView = view.findViewById(R.id.deviceInfoCardView);
        statusDevice = view.findViewById(R.id.statusDevice);
        statusCardView = view.findViewById(R.id.statusCardView);
        groupDeviceStatusCardView = view.findViewById(R.id.groupDeviceStatusCardView);
        deviceStatusText = view.findViewById(R.id.deviceStatusText);
        estatus = view.findViewById(R.id.estatus);
        groupDeviceNameCardView = view.findViewById(R.id.groupDeviceNameCardView);
        deviceNameText = view.findViewById(R.id.deviceNameText);
        eusername = view.findViewById(R.id.eusername);
        groupDeviceMsgCardView = view.findViewById(R.id.groupDeviceMsgCardView);
        deviceMessageText = view.findViewById(R.id.deviceMessageText);
        emessage = view.findViewById(R.id.emessage);

//        databaseTitleCardview = view.findViewById(R.id.databaseTitleCardview);
//        databaseCardview = view.findViewById(R.id.databaseCardview);
//        groupHostnameCardView = view.findViewById(R.id.groupHostnameCardView);
//        dbhostnameText = view.findViewById(R.id.dbhostnameText);
//        ehostname = view.findViewById(R.id.ehostname);
//        groupDatabaseCardView = view.findViewById(R.id.groupDatabaseCardView);
//        dbsecretText = view.findViewById(R.id.dbsecretText);
//        edatabase_secret = view.findViewById(R.id.edatabase_secret);
//        groupServerCardView = view.findViewById(R.id.groupServerCardView);
//        serverkeyText = view.findViewById(R.id.serverkeyText);
//        eserver_key = view.findViewById(R.id.eserver_key);

        configTitleCardView = view.findViewById(R.id.configTitleCardView);
        configTitle = view.findViewById(R.id.configTitle);
        helpConfig = view.findViewById(R.id.helpConfig);
        configCardView = view.findViewById(R.id.configCardView);
        readConfCardView = view.findViewById(R.id.readConfCardView);
        readConfText = view.findViewById(R.id.readConfText);
        readConfBtnCardView = view.findViewById(R.id.readConfBtnCardView);
        readConfigBtn = view.findViewById(R.id.readConfBtn);
        writeConfCardView = view.findViewById(R.id.writeConfCardView);
        writeConfText = view.findViewById(R.id.writeConfText);
        writeConfBtnCardView = view.findViewById(R.id.writeConfBtnCardView);
        writeConfigBtn = view.findViewById(R.id.writeConfBtn);
        clearConfCardView = view.findViewById(R.id.clearConfCardView);
        clearConfText = view.findViewById(R.id.clearConfText);
        clearConfBtnCardView = view.findViewById(R.id.clearConfBtnCardView);
        clearConfigBtn = view.findViewById(R.id.clearConfBtn);

        authConfTitleCardView = view.findViewById(R.id.authConfTitleCardView);
        authConfTitle = view.findViewById(R.id.authConfTitle);
        helpAuth = view.findViewById(R.id.helpAuth);
        authConfCardView = view.findViewById(R.id.authConfCardView);
        groupAuthUserCardView = view.findViewById(R.id.groupAuthUserCardView);
        authUserText = view.findViewById(R.id.authUserText);
        authUserInput = view.findViewById(R.id.authUserInput);
        groupAuthPassCardView = view.findViewById(R.id.groupAuthPassCardView);
        authPassText = view.findViewById(R.id.authPassText);
        authPassInput = view.findViewById(R.id.authPassInput);
        showPassAuthBtn = view.findViewById(R.id.showPassAuthBtn);
        authUpdateCardView = view.findViewById(R.id.authUpdateCardView);
        authUpdateBtn = view.findViewById(R.id.authUpdateBtn);
        AuthRemember = view.findViewById(R.id.AuthRemember);

        setThemes();
        refreshUI(true);
        askPermissionScanWifi();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        helpSSID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                Intent helpOpt = new Intent(devicesActivity, ETHelpActivity.class);
                helpOpt.putExtra("linkURL", "help_ssid");
                startActivity(helpOpt);
            }
        });

        wifiTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.vibrate));
            }
        });

        refreshSSID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.rotate_360));
                //akeText(context, "refresh...", Toast.LENGTH_SHORT).show();

                askPermissionScanWifi();
            }
        });

        spinnerSSID.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                ebssid.setText(item.toString());
            }
        });

        epassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });

        showPassWifiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPassWifiBtn.setSelected(!showPassWifiBtn.isSelected());

                if (!showPassWifiBtn.isSelected()) {
                    showPassWifiBtn.setSelected(false);
                    epassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    showPassWifiBtn.setSelected(true);
                    epassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        eusername.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });

        emessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });

//        _ehost.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                return false;
//            }
//        });

//        _eauth.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                return false;
//            }
//        });

//        _ekey.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                return false;
//            }
//        });

        helpDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                Intent helpOpt = new Intent(devicesActivity, ETHelpActivity.class);
                helpOpt.putExtra("linkURL", "help_device");
                startActivity(helpOpt);
            }
        });

        deviceInfoTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.vibrate));
            }
        });

        helpConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                Intent helpOpt = new Intent(devicesActivity, ETHelpActivity.class);
                helpOpt.putExtra("linkURL", "help_config");
                startActivity(helpOpt);
            }
        });

        configTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.vibrate));
            }
        });

        readConfigBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                if (checkDeviceConn(eipaddr.getText().toString())) {
                    String database_url = "http://192.168.80.1/" + sharedPreferences.getString("ETDeviceModel", "enTouch-demo") + "/deviceConfig.json";

                    new ReadDevices().execute(database_url);
                } else {
                    if (checkConnection()) {
                        String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");
                        String database_url = "https://entouch-1a873.firebaseio.com/entouchdb/" + deviceModel + "/entouchdevice.json";

                        new ReadDevices().execute(database_url);

                        Snackbar.make(view, getResources().getString(R.string.deviceDisconnServerUpdate), 5000).show();
                    } else {
                        Snackbar.make(view, getResources().getString(R.string.deviceDisconn), 5000).show();
                    }
                }
            }
        });

        writeConfigBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                if (checkDeviceConn(eipaddr.getText().toString())) {
                    String database_url = "http://192.168.80.1/" + sharedPreferences.getString("ETDeviceModel", "enTouch-demo" + "/deviceConfig");

                    JSONObject postData = new JSONObject();
                    try {
                        postData.put("ebssid", ebssid.getText().toString());
                        postData.put("epassword", epassword.getText().toString());
//                        postData.put("ehost", ehostname.getText().toString());
//                        postData.put("eauth", edatabase_secret.getText().toString());
//                        postData.put("ekey", eserver_key.getText().toString());
                        postData.put("eusername", eusername.getText().toString());
                        postData.put("emessage", emessage.getText().toString());

                        new SendDevices().execute(database_url, postData.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    parseData.writeLogActivity("logact.log", "Send data ...");
                    parseData.writeLogActivity("logact-in.log", "Kirim data ...");
                    parseData.writeLogActivity("logact-ja.log", "データを送る ...");
                    //Snackbar.make(view, getResources().getString(R.string.deviceConn), 5000).show();
                } else {
                    if (checkConnection()) {
                        String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");

                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("ebssid")
                                .setValue(ebssid.getText().toString());
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("epassword")
                                .setValue(epassword.getText().toString());
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("ehostname")
                                .setValue(getResources().getString(R.string.hostname_url));
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("edatabase_secret")
                                .setValue(getResources().getString(R.string.database_secret));
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("eserver_key")
                                .setValue(getResources().getString(R.string.server_key));
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("eusername")
                                .setValue(eusername.getText().toString());
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("emessage")
                                .setValue(emessage.getText().toString());

                        Snackbar.make(view, getResources().getString(R.string.deviceDisconnServerCommit), 5000).show();
                    } else {
                        Snackbar.make(view, getResources().getString(R.string.deviceDisconn), 5000).show();
                    }
                }
            }
        });

        clearConfigBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));
                String database_url = "http://192.168.80.1/" + sharedPreferences.getString("ETDeviceModel", "enTouch-demo" + "/deviceConfig");

                if (checkDeviceConn(eipaddr.getText().toString())) {
                    JSONObject postData = new JSONObject();
                    try {
                        postData.put("ebssid", "");
                        postData.put("epassword", "");
                        postData.put("ehostname", "");
                        postData.put("edatabase_secret", "");
                        postData.put("eserver_key", "");
                        postData.put("eusername", "");
                        postData.put("emessage", "");

                        new SendDevices().execute(database_url, postData.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    parseData.writeLogActivity("logact.log", "Device configuration removed");
                    parseData.writeLogActivity("logact-in.log", "Konfigurasi perangkat dihapus");
                    parseData.writeLogActivity("logact-ja.log", "デバイス設定が削除されました");
                    //Snackbar.make(view, getResources().getString(R.string.deviceConn), 5000).show();
                } else {
                    if (checkConnection()) {
                        String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");

                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("ebssid").setValue("");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("epassword").setValue("");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("ehostname").setValue("");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("edatabase_secret").setValue("");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("eserver_key").setValue("");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("eusername").setValue("");
                        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("emessage").setValue("");

                        Snackbar.make(view, getResources().getString(R.string.deviceDisconnServerCommit), 5000).show();
                    } else {
                        Snackbar.make(view, getResources().getString(R.string.deviceDisconn), 5000).show();
                    }
                }
            }
        });

        helpAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                Intent helpOpt = new Intent(devicesActivity, ETHelpActivity.class);
                helpOpt.putExtra("linkURL", "help_auth");
                startActivity(helpOpt);
            }
        });

        authConfTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.vibrate));
            }
        });

        showPassAuthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPassAuthBtn.setSelected(!showPassAuthBtn.isSelected());

                if (!showPassAuthBtn.isSelected()) {
                    showPassAuthBtn.setSelected(false);
                    authPassInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    showPassAuthBtn.setSelected(true);
                    authPassInput.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        authUpdateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.click));

                AlertDialog.Builder builder = new AlertDialog.Builder(devicesActivity, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(getResources().getString(R.string.notification_auth_title));

                if (authUserInput.getText().toString().equals("") && authPassInput.getText().toString().equals("")) {
                    builder.setMessage(getResources().getString(R.string.notification_auth_nouserpass));
                } else if (authUserInput.getText().toString().equals("")) {
                    builder.setMessage(getResources().getString(R.string.notification_auth_nouser));
                } else if (authPassInput.getText().toString().equals("")) {
                    builder.setMessage(getResources().getString(R.string.notification_auth_nopass));
                }

                builder.setIcon(R.drawable.ic_info_outline_black_48dp);
                builder.setPositiveButton(getResources().getString(R.string.notification_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!authUserInput.getText().toString().equals("") && !authPassInput.getText().toString().equals("")) {
                            String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");

                            mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("dusername")
                                    .setValue(authUserInput.getText().toString());
                            mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDEVICE).child("dpassword")
                                    .setValue(authPassInput.getText().toString());

                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putString("ETAuthUser", authUserInput.getText().toString());
                            edit.putString("ETAuthPass", authPassInput.getText().toString());
                            edit.apply();
                        }
                    }
                });
                builder.show();
            }
        });

        AuthRemember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (AuthRemember.isChecked()) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putBoolean("ETAuthlogin", true);
                    edit.apply();
                } else {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putBoolean("ETAuthlogin", false);
                    edit.apply();
                }
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        devicesActivity.registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();

        // register connection status listener
        ETAppsActivity.getInstance().setConnectivityListener(this);

        setLanguage();
        refreshUI(false);
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        devicesActivity.unregisterReceiver(mWifiScanReceiver);
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.e(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();

        if (mWifiManager.isWifiEnabled() && wifiDefaultState) {
            mWifiManager.setWifiEnabled(false);
            wifiDefaultState = false;
        }
    }

    @Override
    public void onDetach() {
        Log.e(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Note: If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0) {
            switch (requestCode) {
                case ACCESS_COARSE_LOCATION_REQUEST_CODE:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        scanWifi();
                    }
                    break;

                default:
                    // do nothing
                    break;
            }
        } else {
            Log.e(TAG, "Permission Cancelled!");
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.e(TAG, "onNetworkConnectionChanged");

    }

    public boolean checkConnection() {
        return ETConectivityReceiver.isConnected();
    }

    private void refreshUI(Boolean anim) {
        wifiTitle.setText(getResources().getString(R.string.wifiTitle));
        wifiNetworkText.setText(getResources().getString(R.string.wifiNetworkTitle));
        ssidNameText.setText(getResources().getString(R.string.ssidnameTitle));
        ssidPasswordText.setText(getResources().getString(R.string.ssidpasswordTitle));
        epassword.setHint(getResources().getString(R.string.ssidpasswordEdit));
        ipAddrText.setText(getResources().getString(R.string.ipAddrTitle));
        try {
            String ipaddr = Formatter.formatIpAddress(mWifiInfo.getIpAddress());
            eipaddr.setText(ipaddr);
        } catch (NullPointerException e) {
            eipaddr.setText("");
        }

        deviceInfoTitle.setText(getResources().getString(R.string.devicesinfoTitle));
        if (checkDeviceConn(eipaddr.getText().toString())) {
            estatus.setText(getResources().getString(R.string.deviceConn));
            statusDevice.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.blinking));
            Glide.with(devicesActivity)
                    .load(R.drawable.ic_brightness_1_on_48dp)
                    .apply(RequestOptions.fitCenterTransform())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                    .transition(GenericTransitionOptions.with(R.anim.blinking))
                    .into(statusDevice);
        } else {
            estatus.setText(getResources().getString(R.string.deviceDisconn));
            statusDevice.clearAnimation();
            Glide.with(devicesActivity)
                    .load(R.drawable.ic_brightness_1_off_48dp)
                    .apply(RequestOptions.fitCenterTransform())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                    .apply(RequestOptions.noAnimation())
                    .into(statusDevice);
        }
        deviceStatusText.setText(getResources().getString(R.string.deviceStatusTitle));
        deviceNameText.setText(getResources().getString(R.string.devicesnameTitle));
        eusername.setHint(getResources().getString(R.string.devicesnameEdit));
        deviceMessageText.setText(getResources().getString(R.string.devicesmessageTitle));
        emessage.setHint(getResources().getString(R.string.devicesmessageEdit));

        configTitle.setText(getResources().getString(R.string.configTitle));
        readConfText.setText(getResources().getString(R.string.readConfTitle));
        readConfigBtn.setText(getResources().getString(R.string.readConfButton));
        writeConfText.setText(getResources().getString(R.string.writeConfTitle));
        writeConfigBtn.setText(getResources().getString(R.string.writeConfButton));
        clearConfText.setText(getResources().getString(R.string.clearConfTitle));
        clearConfigBtn.setText(getResources().getString(R.string.clearConfButton));

        authConfTitle.setText(getResources().getString(R.string.authconfTitle));
        authUserText.setText(getResources().getString(R.string.authuserTitle));
        authUserInput.setHint(sharedPreferences.getString("ETAuthUser", "entouch"));
        authPassText.setText(getResources().getString(R.string.authpassTitle));
        authPassInput.setHint(sharedPreferences.getString("ETAuthPass", "entouch"));
        AuthRemember.setText(getResources().getString(R.string.auth_remember));
        if (sharedPreferences.getBoolean("ETAuthlogin", true)) {
            AuthRemember.setChecked(true);
        } else {
            AuthRemember.setChecked(false);
        }
        authUpdateBtn.setText(getResources().getString(R.string.authupdateButton));

        if (anim) {
            wifiCardView.setVisibility(View.VISIBLE);
            wifiTitleCardView.setVisibility(View.VISIBLE);
            wifiTitleCardView.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.show_up_first));
            deviceInfoCardView.setVisibility(View.VISIBLE);
            deviceInfoTitleCardView.setVisibility(View.VISIBLE);
            deviceInfoTitleCardView.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.show_up_second));
//            databaseCardview.setVisibility(View.VISIBLE);
//            databaseTitleCardview.setVisibility(View.VISIBLE);
//            databaseTitleCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.show_up_third));
            configCardView.setVisibility(View.VISIBLE);
            configTitleCardView.setVisibility(View.VISIBLE);
            configTitleCardView.setVisibility(View.VISIBLE);
            authConfTitleCardView.setVisibility(View.VISIBLE);
            authConfTitleCardView.startAnimation(AnimationUtils.loadAnimation(devicesActivity, R.anim.show_up_fourth));
        }
    }

    private void setThemes() {
        configFragment.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        wifiCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        ipAddrCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        essidCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        deviceInfoCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        statusCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
//        databaseCardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        configCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        authConfCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));

        readConfCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        writeConfCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        clearConfCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));

        groupWifiCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupSSIDNameCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupSSIDPassCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupIPAddrCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupDeviceStatusCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupDeviceNameCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupDeviceMsgCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
//        groupHostnameCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
//        groupDBauthCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
//        groupDBServerCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        readConfBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        writeConfBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        clearConfBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupAuthUserCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupAuthPassCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        authUpdateCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        spinnerSSID.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        spinnerSSID.setLinkTextColor(Color.BLUE);
        spinnerSSID.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));

        readConfigBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        writeConfigBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        clearConfigBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        authUpdateBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        epassword.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        eusername.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        emessage.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
//        _ehost.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
//        _eauth.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
//        _ekey.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));

        wifiNetworkText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        ssidNameText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        ebssid.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        ssidPasswordText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        ipAddrText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        eipaddr.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        deviceStatusText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        estatus.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        deviceNameText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        deviceMessageText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
//        dbhostnameText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
//        dbauthText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
//        dbserverkeyText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        authUserText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        authPassText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));

        readConfText.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        writeConfText.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        clearConfText.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            readConfigBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            writeConfigBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            clearConfigBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            authUpdateBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
        } else {
            readConfigBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            writeConfigBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            clearConfigBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            authUpdateBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
        }

    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = devicesActivity.getResources().getConfiguration();
        config.locale = locale;
        devicesActivity.getResources().updateConfiguration(config, devicesActivity.getResources().getDisplayMetrics());
    }

    private void askPermissionScanWifi() {
        boolean canScan = askPermission(ACCESS_COARSE_LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (canScan) {
            scanWifi();
        }
    }

    private boolean askPermission(int requestId, String permissionName) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            // Check if we have permission
            int permission = ActivityCompat.checkSelfPermission(devicesActivity, permissionName);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{permissionName},
                        requestId
                );
                return false;
            }
        }
        return true;
    }

    private boolean checkDeviceConn(String ipAddr) {
        String[] parse = ipAddr.split("\\.");
        boolean data = false;

        if (parse[0].equals("192")) {
            if (parse[1].equals("168")) {
                data = (parse[2].equals("1") || parse[2].equals("32"));
            }
        }
        return data;
    }

    private void scanWifi() {
        try {
            if (!mWifiManager.isWifiEnabled()) {
                mWifiManager.setWifiEnabled(true);
                wifiDefaultState = true;
            }

            mWifiManager.startScan();

            if (mWifiManager.startScan()) {
                List<ScanResult> scanResults = mWifiManager.getScanResults();
                ArrayList<String> ssid = new ArrayList<String>();

                int counter = 0;
                boolean firstScan = true;

                for (ScanResult result : scanResults) {
                    if (firstScan) {
                        ssid.add(counter++, result.SSID);
                        firstScan = false;
                    }
                    ssid.add(counter++, result.SSID);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(devicesActivity, android.R.layout.simple_spinner_item, ssid);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerSSID.setAdapter(adapter);
            }
        } catch (Exception e) {
            // critical error: set to no results and do not die
        }
    }

    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            askPermissionScanWifi();
        }
    }

    private class SendDevices extends AsyncTask<String, String, String> {
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(devicesActivity, R.style.AppCompatAlertDialogStyle);
            progressDialog.setTitle(getResources().getString(R.string.confUpload));
            progressDialog.setMessage(getResources().getString(R.string.writeNotif));
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... Url) {
            String data = "";
            boolean failed = false;

            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) new URL(Url[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");

                httpURLConnection.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes("PostData=" + Url[1]);
                wr.flush();
                wr.close();

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (Exception e) {
                e.printStackTrace();
                failed = true;
                Snackbar.make(view, getResources().getString(R.string.uploadFailed), 5000).show();
            } finally {
                if (httpURLConnection != null) {
                    if (!failed) {
                        Snackbar.make(view, getResources().getString(R.string.uploadSucceed), 5000).show();
                    }
                    httpURLConnection.disconnect();
                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(String... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            progressDialog.setProgress(Integer.parseInt(progress[0]));
            // Dismiss the progress dialog
            //mProgressDialog.dismiss();
        }
    }

    private class ReadDevices extends AsyncTask<String, Integer, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(devicesActivity, R.style.AppCompatAlertDialogStyle);
            progressDialog.setMessage(getResources().getString(R.string.confRetrieve));
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        protected String doInBackground(String... Url) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;
            StringBuffer buffer;

            try {
                URL url = new URL(Url[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                int getContentLength = connection.getContentLength();
                //int currentLength = 0;

                //InputStream stream = connection.getInputStream();
                //reader = new BufferedReader(new InputStreamReader(stream));

                CountingInputStream countingInputStream = new CountingInputStream(connection.getInputStream());
                reader = new BufferedReader(new InputStreamReader(countingInputStream));

                buffer = new StringBuffer();
                String line;

                while ((line = reader.readLine()) != null) {
                    line = line + "\n";
                    buffer.append(line);
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                    //currentLength += line.getBytes("ISO-8859-2").length + 2;
                    publishProgress((int) ((getContentLength * 100) / countingInputStream.getCount()));
                }
                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    ebssid.setText(jsonObject.getString("ebssid"));
                    epassword.setText(jsonObject.getString("epassword"));
//                    ehostname.setText(jsonObject.getString("ehostname"));
//                    edatabase_secret.setText(jsonObject.getString("edatabase_secret"));
//                    eserver_key.setText(jsonObject.getString("eserver_key"));
                    eusername.setText(jsonObject.getString("eusername"));
                    emessage.setText(jsonObject.getString("emessage"));
                } catch (JSONException e) {
                    Snackbar.make(view, getResources().getString(R.string.retrieveFailed), 5000).show();
                    progressDialog.dismiss();

                    e.printStackTrace();
                }
                parseData.writeLogActivity("logact.log", "Request data ...");
                parseData.writeLogActivity("logact-in.log", "Meminta data ...");
                parseData.writeLogActivity("logact-ja.log", "データをリクエスト...");

                Snackbar.make(view, getResources().getString(R.string.retrieveSucceed), 5000).show();
            } else {
                Snackbar.make(view, getResources().getString(R.string.retrieveFailed), 5000).show();
            }

            progressDialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            progressDialog.setProgress(progress[0]);
            // Dismiss the progress dialog
            //mProgressDialog.dismiss();
        }
    }

    public class CountingInputStream extends FilterInputStream {
        long count;

        protected CountingInputStream(InputStream in) {
            super(in);
        }

        public long getCount() {
            return count;
        }

        @Override
        public int read() throws IOException {
            final int read = super.read();
            if (read >= 0) count++;
            return read;
        }

        @Override
        public int read(byte[] b, int off, int len) throws IOException {
            final int read = super.read(b, off, len);
            if (read > 0) count += read;
            return read;
        }

        @Override
        public long skip(long n) throws IOException {
            final long skipped = super.skip(n);
            if (skipped > 0) count += skipped;
            return skipped;
        }
    }
}
