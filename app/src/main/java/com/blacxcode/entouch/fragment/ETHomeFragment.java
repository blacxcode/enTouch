package com.blacxcode.entouch.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.activity.ETHelpActivity;
import com.blacxcode.entouch.activity.ETMainActivity;
import com.blacxcode.entouch.activity.ETNotifyActivity;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by blacXcode on 5/8/2017.
 */

public class ETHomeFragment extends Fragment {
    private static final String TAG = ETHomeFragment.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    protected ETMainActivity mainActivity;
    protected ETJSONParseData parseData;

    protected LinearLayout homeFragment;
    protected View view;
    protected CardView registCardView, regIDCardView, logMsgCardView, logActCardView;
    protected ImageButton hideLogMsg, hideLogAct;
    protected TextView regIDTitle, regIDText, logMsgTitle, logActTitle, logMsgText, logActText;
    protected ImageView helpRegID;

    protected Locale locale = null;
    protected Bundle bundle = null;

    protected String getThemes = "ETColorBlueGrey", getLang = "en";

    public ETHomeFragment() {
        // Required empty public constructor
    }

    public static ETHomeFragment newInstance() {
        return new ETHomeFragment();
    }

    @Override
    public void onAttach(Context context) {
        Log.e(TAG, "onAttach");
        super.onAttach(context);

        if (context instanceof ETMainActivity) {
            mainActivity = (ETMainActivity) context;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            mainActivity.getResources().updateConfiguration(newConfig, mainActivity.getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragment_home, parent, false);

        sharedPreferences = mainActivity.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        parseData = new ETJSONParseData(mainActivity);

        homeFragment = view.findViewById(R.id.homeFragment);
        regIDTitle = view.findViewById(R.id.regIDTitle);
        regIDText = view.findViewById(R.id.regIDText);
        regIDCardView = view.findViewById(R.id.regIDCardView);
        registCardView = view.findViewById(R.id.registCardView);
        helpRegID = view.findViewById(R.id.helpRegID);

        logMsgCardView = view.findViewById(R.id.logMsgCardView);
        logMsgTitle = view.findViewById(R.id.logMsgTitle);
        hideLogMsg = view.findViewById(R.id.hideLogMsg);
        //logmsgWebView = view.findViewById(R.id.logmsgWebView);
        //logmsgWebView.setVerticalScrollBarEnabled(false);
        logMsgText = view.findViewById(R.id.logMsgText);
        logMsgText.setMovementMethod(new ScrollingMovementMethod());

        logActCardView = view.findViewById(R.id.logActCardView);
        logActTitle = view.findViewById(R.id.logActTitle);
        hideLogAct = view.findViewById(R.id.hideLogAct);
        logActText = view.findViewById(R.id.logActText);
        logActText.setMovementMethod(new ScrollingMovementMethod());

        setThemes();
        refreshUI();
        displayFirebaseRegId();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

//        try {
//            bundle = getActivity().getIntent().getExtras();
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//
//        if (bundle != null) {
//            String getTitle = bundle.getString("title");
//            String getMessage = bundle.getString("message");
//            String getLocation = bundle.getString("location");
//
//            getActivity().setIntent(null);
//
//            Intent notify = new Intent(mainActivity, ETNotifyActivity.class);
//            notify.putExtra("title", getTitle);
//            notify.putExtra("message", getMessage);
//            notify.putExtra("location", getLocation);
//            startActivity(notify);
//            //Toast.makeText(context, "ADA BUNDLE INTENT", Toast.LENGTH_LONG).show();
//        }

        helpRegID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));

                Intent helpOpt = new Intent(mainActivity, ETHelpActivity.class);
                helpOpt.putExtra("linkURL", "help_regid");
                startActivity(helpOpt);
            }
        });

        regIDTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.vibrate));
            }
        });

        regIDCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet animationSet = new AnimationSet(true);//false means don't share interpolators
                animationSet.addAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.rotate_360));
                animationSet.addAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.show_up));

                if (regIDText.getText().equals(getResources().getString(R.string.regDevice))) {
                    registCardView.startAnimation(animationSet);
                    String devicekey = sharedPreferences.getString("devicekey", "");
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        regIDText.setText(Html.fromHtml("<p>" + devicekey + "</p>", Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        regIDText.setText(Html.fromHtml("<p>" + devicekey + "</p>"));
                    }
                } else if (regIDText.getText().equals(getResources().getString(R.string.notregDevice))) {
                    // do something
                } else {
                    registCardView.startAnimation(animationSet);
                    regIDText.setText(getResources().getString(R.string.regDevice));
                }

                refreshUI();
            }
        });

        regIDCardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!regIDText.getText().equals(getResources().getString(R.string.regDevice))
                        && !regIDText.getText().equals(getResources().getString(R.string.notregDevice))) {

                    try {
                        ClipboardManager clipboardManager = (ClipboardManager) mainActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clipData = ClipData.newPlainText("copy id", regIDText.getText());
                        clipboardManager.setPrimaryClip(clipData);

                        Snackbar.make(v, getResources().getString(R.string.copyID), Snackbar.LENGTH_LONG).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        });

        logMsgTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.vibrate));

                readLogmsg();
            }
        });

        hideLogMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideLogMsg.setSelected(!hideLogMsg.isSelected());

                if (hideLogMsg.isSelected()) {
                    AnimationSet animationSet = new AnimationSet(false);
                    animationSet.addAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.rotate_up));
                    animationSet.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            hideLogMsg.setSelected(true);
                            logMsgCardView.setVisibility(View.GONE);
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putBoolean("isHideLogmsg", true);
                            edit.apply();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    v.setAnimation(animationSet);
                    v.startAnimation(animationSet);
                } else {
                    AnimationSet animationSet = new AnimationSet(false);
                    animationSet.addAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.rotate_down));
                    animationSet.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            hideLogMsg.setSelected(false);
                            logMsgCardView.setVisibility(View.VISIBLE);
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putBoolean("isHideLogmsg", false);
                            edit.apply();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    v.setAnimation(animationSet);
                    v.startAnimation(animationSet);
                }
            }
        });

        logActTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.vibrate));
            }
        });

        hideLogAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideLogAct.setSelected(!hideLogAct.isSelected());

                if (hideLogAct.isSelected()) {
                    AnimationSet animationSet = new AnimationSet(false);
                    animationSet.addAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.rotate_up));
                    animationSet.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            hideLogAct.setSelected(true);
                            logActCardView.setVisibility(View.GONE);
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putBoolean("isHideLogact", true);
                            edit.apply();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    v.setAnimation(animationSet);
                    v.startAnimation(animationSet);
                } else {
                    AnimationSet animationSet = new AnimationSet(false);
                    animationSet.addAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.rotate_down));
                    animationSet.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            hideLogAct.setSelected(false);
                            logActCardView.setVisibility(View.VISIBLE);
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putBoolean("isHideLogact", false);
                            edit.apply();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    v.setAnimation(animationSet);
                    v.startAnimation(animationSet);
                }
            }
        });

        logActText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hideLogMsg.isSelected() || (hideLogMsg.isSelected() &&
                        !regIDText.getText().equals(getResources().getString(R.string.regDevice)) &&
                        !regIDText.getText().equals(getResources().getString(R.string.notregDevice)))) {
                    try {
                        ((ETMainActivity) getActivity()).hideFab(1000);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();

        refreshUI();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        refreshUI();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.e(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.e(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    private void refreshUI() {
        /*Device Id*/
        regIDTitle.setText(getResources().getString(R.string.regDeviceTitle));
        if (regIDText.getText().equals(getResources().getString(R.string.regDevice))) {
            helpRegID.setVisibility(View.GONE);
            regIDTitle.setVisibility(View.GONE);
            regIDText.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.blinking));

            regIDText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
            regIDText.setGravity(Gravity.CENTER);
            regIDText.setText(getResources().getString(R.string.regDevice));

        } else if (regIDText.getText().equals(getResources().getString(R.string.notregDevice))) {
            helpRegID.setVisibility(View.GONE);
            regIDTitle.setVisibility(View.GONE);
            regIDText.clearAnimation();
            regIDText.animate().cancel();
            regIDText.setTextColor(Color.RED);
            regIDText.setGravity(Gravity.CENTER);
            regIDText.setText(R.string.notregDevice);
        } else {

            String devicekey = sharedPreferences.getString("devicekey", "");

            helpRegID.setVisibility(View.VISIBLE);
            regIDTitle.setVisibility(View.VISIBLE);
            regIDTitle.setText(getResources().getString(R.string.regDeviceTitle));

            regIDText.clearAnimation();
            regIDText.animate().cancel();
            regIDText.setGravity(Gravity.START);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                regIDText.setText(Html.fromHtml("<p>" + devicekey + "</p>", Html.FROM_HTML_MODE_LEGACY));
            } else {
                regIDText.setText(Html.fromHtml("<p>" + devicekey + "</p>"));
            }
        }

        /*Log Notification*/
        logMsgTitle.setText(getResources().getString(R.string.logmsgTitle));
        if (sharedPreferences.getBoolean("isHideLogmsg", false)) {
            hideLogMsg.setSelected(true);
            logMsgCardView.setVisibility(View.GONE);
        } else {
            hideLogMsg.setSelected(false);
            logMsgCardView.setVisibility(View.VISIBLE);
        }
        readLogmsg();

        /*Log Activity*/
        logActTitle.setText(getResources().getString(R.string.logactTitle));
        if (sharedPreferences.getBoolean("isHideLogact", false)) {
            hideLogAct.setSelected(true);
            logActCardView.setVisibility(View.GONE);
        } else {
            hideLogAct.setSelected(false);
            logActCardView.setVisibility(View.VISIBLE);
        }
        readLogact();
    }

    private void displayFirebaseRegId() {
        String devicekey = sharedPreferences.getString("devicekey", "");
        Log.e(TAG, "Firebase Reg ID: " + devicekey);

        if (!TextUtils.isEmpty(devicekey)) {
            regIDText.setText(getResources().getString(R.string.regDevice));
        } else {
            regIDText.setText(getResources().getString(R.string.notregDevice));
        }
    }

    private void setThemes() {
        homeFragment.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        regIDCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        logMsgCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        logActCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));

        regIDText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        logMsgText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        logActText.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = mainActivity.getResources().getConfiguration();
        config.locale = locale;
        mainActivity.getResources().updateConfiguration(config, mainActivity.getResources().getDisplayMetrics());
    }

    public String readLogmsg() {
        logMsgText.setMovementMethod(new ScrollingMovementMethod());
        logMsgText.setMovementMethod(LinkMovementMethod.getInstance());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            logMsgText.setText(Html.fromHtml(parseData.readFromInternalFile(getResources().getString(R.string.log_filename_msg)), Html.FROM_HTML_MODE_COMPACT));
        } else {
            logMsgText.setText(Html.fromHtml(parseData.readFromInternalFile(getResources().getString(R.string.log_filename_msg))));
        }
        return logMsgText.toString();
    }

    public String readLogact() {
        logActText.setMovementMethod(new ScrollingMovementMethod());
        logActText.setMovementMethod(LinkMovementMethod.getInstance());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            logActText.setText(Html.fromHtml(parseData.readFromInternalFile(getResources().getString(R.string.log_filename_act)), Html.FROM_HTML_MODE_LEGACY));
        } else {
            logActText.setText(Html.fromHtml(parseData.readFromInternalFile(getResources().getString(R.string.log_filename_act))));
        }
        return logActText.toString();
    }
}
