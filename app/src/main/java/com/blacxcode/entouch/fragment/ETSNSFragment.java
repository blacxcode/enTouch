package com.blacxcode.entouch.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.activity.ETMainActivity;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.data.ETClient;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class ETSNSFragment extends Fragment {
    private static final String TAG = ETSNSFragment.class.getSimpleName();
    private static final String TAG_FACEBOOK = "facebook";
    private final String PENDING_ACTION_BUNDLE_KEY = "com.blacxcode.entouch:PendingAction";
    protected SharedPreferences sharedPreferences;

    protected ETMainActivity mainActivity;
    protected ETJSONParseData parseData;

    protected RelativeLayout snsFragment;
    protected LoginButton loginButton;
    protected Button postStatusUpdateButton;
    protected Button requestAccessButton;
    protected PendingAction pendingAction = PendingAction.NONE;
    protected CallbackManager callbackManager;
    protected ProfileTracker profileTracker;
    protected ShareDialog shareDialog;
    protected TextView greeting;
    protected ImageView profilePicImageView;
    protected View view;

    protected DatabaseReference mFirebaseDB;
    protected FirebaseDatabase mFirebaseInstance;

    protected Locale locale = null;

    private boolean postingEnabled = false;
    private boolean canPresentShareDialog;
    private String getThemes = "ETColorBlueGrey", getLang = "en";
    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onCancel() {
            Log.d(TAG_FACEBOOK, "Canceled");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d(TAG_FACEBOOK, String.format("Error: %s", error.toString()));
            String title = getResources().getString(R.string.error);
            String alertMessage = error.getMessage();
            showResult(title, alertMessage);

            parseData.writeLogActivity("logact.log", title + "->\t" + alertMessage);
            parseData.writeLogActivity("logact-in.log", title + "->\t" + alertMessage);
            parseData.writeLogActivity("logact-ja.log", title + "->\t" + alertMessage);
        }

        @Override
        public void onSuccess(Sharer.Result result) {
            Log.d(TAG_FACEBOOK, "Success!");
            if (result.getPostId() != null) {
                String title = getResources().getString(R.string.success);
                String alertMessage = getResources().getString(R.string.successfully_posted_post);
                showResult(title, alertMessage);

                parseData.writeLogActivity("logact.log", title + "->\t" + alertMessage);
                parseData.writeLogActivity("logact-in.log", title + "->\t" + alertMessage);
                parseData.writeLogActivity("logact-ja.log", title + "->\t" + alertMessage);
            }
        }

        private void showResult(String title, String alertMessage) {
            new AlertDialog.Builder(mainActivity, R.style.AppCompatAlertDialogStyle)
                    .setTitle(title)
                    .setMessage(alertMessage)
                    .setPositiveButton(getResources().getString(R.string.ok), null)
                    .show();
        }
    };

    public ETSNSFragment() {
        // Required empty public constructor
    }

    public static ETSNSFragment newInstance() {
        return new ETSNSFragment();
    }

    @Override
    public void onAttach(Context context) {
        Log.e(TAG, "onAttach");
        super.onAttach(context);

        if (context instanceof ETMainActivity) {
            mainActivity = (ETMainActivity) context;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            mainActivity.getResources().updateConfiguration(newConfig, mainActivity.getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragment_sns, parent, false);

        sharedPreferences = mainActivity.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        parseData = new ETJSONParseData(mainActivity);

        snsFragment = view.findViewById(R.id.snsFragment);

        profilePicImageView = view.findViewById(R.id.profilePicture);
        greeting = view.findViewById(R.id.greeting);
        loginButton = view.findViewById(R.id.loginButton);
        postStatusUpdateButton = view.findViewById(R.id.postStatusUpdateButton);
        requestAccessButton = view.findViewById(R.id.requestAccessButton);

        // If using in a fragment
        loginButton.setFragment(this);

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, shareCallback);

        // Can we present the share dialog for regular links?
        canPresentShareDialog = ShareDialog.canShow(ShareLinkContent.class);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDB = mFirebaseInstance.getReference(ETConfig.DB_ENTOUCH);

        Glide.with(mainActivity)
                .load(R.drawable.com_facebook_profile_picture_blank_square)
                .thumbnail(0.5f)
                .apply(RequestOptions.circleCropTransform())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                .into(profilePicImageView);

        greeting.setText(getResources().getString(R.string.login_greeting));
        greeting.setAlpha(0.9f);
        greeting.setTypeface(null, Typeface.ITALIC);

        setThemes();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                postingEnabled = true;
                postStatusUpdateButton.setVisibility(View.VISIBLE);
                requestAccessButton.setVisibility(View.VISIBLE);

                handlePendingAction();
                refreshUI();

                parseData.writeLogActivity("logact.log", "Login access successfully");
                parseData.writeLogActivity("logact-in.log", "Akses masuk berhasil");
                parseData.writeLogActivity("logact-ja.log", "ログインアクセスが正常に終了");
            }

            @Override
            public void onCancel() {
                if (pendingAction != PendingAction.NONE) {
                    showAlert();
                    pendingAction = PendingAction.NONE;
                }

                refreshUI();

                parseData.writeLogActivity("logact.log", "Login access canceled");
                parseData.writeLogActivity("logact-in.log", "Akses masuk dibatalkan");
                parseData.writeLogActivity("logact-ja.log", "ログインアクセスがキャンセルされました");
            }

            @Override
            public void onError(FacebookException exception) {
                if (pendingAction != PendingAction.NONE && exception instanceof FacebookAuthorizationException) {
                    showAlert();
                    pendingAction = PendingAction.NONE;
                }

                refreshUI();

                parseData.writeLogActivity("logact.log", "Login access unsuccessfully ...");
                parseData.writeLogActivity("logact-in.log", "Akses masuk tidak berhasil ...");
                parseData.writeLogActivity("logact-ja.log", "ログインに失敗しました...");
            }

            private void showAlert() {
                new AlertDialog.Builder(mainActivity, R.style.AppCompatAlertDialogStyle)
                        .setTitle(getResources().getString(R.string.cancelled))
                        .setMessage(getResources().getString(R.string.permission_not_granted))
                        .setPositiveButton(getResources().getString(R.string.ok), null)
                        .show();
            }
        });

        if (savedInstanceState != null) {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
            pendingAction = PendingAction.valueOf(name);
        }

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

                refreshUI();
                handlePendingAction();
            }
        };

        postStatusUpdateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onClickPostStatusUpdate();
            }
        });

        requestAccessButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    if (!sharedPreferences.getString("ETDeviceModel", "").equals("") && !sharedPreferences.getString("devicekey", "").equals("")) {
                        sendRequest();

                        requestAccessButton.setEnabled(false);
                        requestAccessButton.setText(getResources().getString(R.string.waiting_request));
                    } else if (sharedPreferences.getString("ETDeviceModel", "").equals("")) {
                        Snackbar.make(view, getResources().getString(R.string.no_hostname), Snackbar.LENGTH_LONG).show();
                    } else if (sharedPreferences.getString("devicekey", "").equals("")) {
                        Snackbar.make(view, getResources().getString(R.string.notregDevice), Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }

                parseData.writeLogActivity("logact.log", "Request access to the device");
                parseData.writeLogActivity("logact-in.log", "Meminta akses ke perangkat");
                parseData.writeLogActivity("logact-ja.log", "デバイスへのアクセスを要求する");
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> permissionNeeds = Arrays.asList("public_profile", "user_photos", "friends_photos", "email", "user_birthday", "user_friends", "publish_actions", "user_status");
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), permissionNeeds);

                if (!postingEnabled) {
                    postingEnabled = true;
                    postStatusUpdateButton.setVisibility(View.VISIBLE);
                    requestAccessButton.setVisibility(View.VISIBLE);
                } else {
                    postingEnabled = false;
                    postStatusUpdateButton.setVisibility(View.GONE);
                    requestAccessButton.setVisibility(View.GONE);
                }

                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (object != null) {
                            Log.e("Me Request", object.toString());

                            try {
                                Log.e("Email request", object.getString("email"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, link, email, cover_id, source, gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends"));
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.activateApp(getActivity());

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.deactivateApp(getActivity());
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.e(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();

        try {
            profileTracker.stopTracking();
        } catch (Exception e) {
            Log.e("profileTracker", e.toString());
        }
    }

    @Override
    public void onDetach() {
        Log.e(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void refreshUI() {
        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;
        postStatusUpdateButton.setEnabled(enableButtons || canPresentShareDialog);

        Profile profile = Profile.getCurrentProfile();
        if (enableButtons && profile != null) {
            try {
                Glide.with(mainActivity)
                        .load(profile.getProfilePictureUri(200, 200).toString())
                        .thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(profilePicImageView);

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            String name = profile.getFirstName();
            greeting.setText(getResources().getString(R.string.hello_user, name));

            postingEnabled = true;
            postStatusUpdateButton.setVisibility(View.VISIBLE);
            postStatusUpdateButton.setText(getResources().getString(R.string.post_status));

            requestAccessButton.setVisibility(View.VISIBLE);
            checkAccess();

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("userid", profile.getId());
            editor.apply();
        } else {
            logOut();
        }
    }

    private void setThemes() {
        snsFragment.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        greeting.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = mainActivity.getResources().getConfiguration();
        config.locale = locale;
        mainActivity.getResources().updateConfiguration(config, mainActivity.getResources().getDisplayMetrics());
    }

    private void logOut() {
        try {
            Glide.with(mainActivity)
                    .load(R.drawable.com_facebook_profile_picture_blank_square)
                    .thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                    .into(profilePicImageView);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        greeting.setText(getResources().getString(R.string.login_greeting));
        greeting.setAlpha(0.9f);
        greeting.setTypeface(null, Typeface.ITALIC);

        loginButton.setVisibility(View.VISIBLE);

        postingEnabled = false;
        postStatusUpdateButton.setVisibility(View.GONE);

        requestAccessButton.setVisibility(View.GONE);

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("userid", "0");
        edit.putString("devicestatus", "rejected");
        edit.apply();

        parseData.writeLogActivity("logact.log", "Logout account");
        parseData.writeLogActivity("logact-in.log", "Keluar akun");
        parseData.writeLogActivity("logact-ja.log", "ログアウトアカウント");
    }

    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case NONE:
                break;
            case POST_STATUS_UPDATE:
                postStatusUpdate();
                break;
            default:
                // do nothing
                break;
        }
    }

    private void onClickPostStatusUpdate() {
        performPublish(PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);
    }

    private void postStatusUpdate() {
        Profile profile = Profile.getCurrentProfile();

        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setQuote(getResources().getString(R.string.apps_promo))
                .setContentUrl(Uri.parse(getResources().getString(R.string.apps_link)))
                .build();
        if (canPresentShareDialog) {
            shareDialog.show(linkContent);
        } else if (profile != null && hasPublishPermission()) {
            ShareApi.share(linkContent, shareCallback);
        } else {
            pendingAction = PendingAction.POST_STATUS_UPDATE;
        }
    }

    private boolean hasPublishPermission() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
    }

    private void performPublish(PendingAction action, boolean allowNoToken) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null || allowNoToken) {
            pendingAction = action;
            handlePendingAction();
        }
    }

    public void sendRequest() {
        Profile profile = Profile.getCurrentProfile();

        String devicename = Build.MODEL;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
        String statusdate = simpledateformat.format(calendar.getTime());

        String devicekey = sharedPreferences.getString("devicekey", "");

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("userid", profile.getId());
        edit.putString("devicestatus", "request");
        edit.apply();

        mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                .child(ETConfig.DB_ENTOUCHCLIENT).child(profile.getId()).child("devicekey").setValue(devicekey);
        mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                .child(ETConfig.DB_ENTOUCHCLIENT).child(profile.getId()).child("devicename").setValue(devicename);
        mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                .child(ETConfig.DB_ENTOUCHCLIENT).child(profile.getId()).child("devicestatus").setValue("request");
        mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                .child(ETConfig.DB_ENTOUCHCLIENT).child(profile.getId()).child("statusdate").setValue(statusdate);
        mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                .child(ETConfig.DB_ENTOUCHCLIENT).child(profile.getId()).child("username").setValue(profile.getFirstName());
    }

    private void checkAccess() {
        retrieveData();

        if (sharedPreferences.getString("devicestatus", "").equals("approved")) {
            requestAccessButton.setEnabled(false);
            requestAccessButton.setText(getResources().getString(R.string.request_approved));

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("devicestatus", "approved");
            edit.apply();
        } else if (sharedPreferences.getString("devicestatus", "").equals("request")) {
            requestAccessButton.setEnabled(false);
            requestAccessButton.setText(getResources().getString(R.string.waiting_request));

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("devicestatus", "request");
            edit.apply();
        } else {
            requestAccessButton.setEnabled(true);
            requestAccessButton.setText(getResources().getString(R.string.request_access));

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("devicestatus", "rejected");
            edit.apply();
        }
    }

    public void retrieveData() {
        Profile profile = Profile.getCurrentProfile();

        //Get datasnapshot at your "users" root node
        DatabaseReference ref = mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                .child(ETConfig.DB_ENTOUCHCLIENT);
        ref.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            ETClient data = dataSnapshot1.getValue(ETClient.class);

                            if (data != null) {
                                if (profile.getId().equals(dataSnapshot1.getKey())) {
                                    SharedPreferences.Editor edit = sharedPreferences.edit();
                                    edit.putString("devicestatus", data.devicestatus);
                                    edit.apply();
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                }
        );
    }

    private enum PendingAction {
        NONE,
        POST_STATUS_UPDATE
    }
}