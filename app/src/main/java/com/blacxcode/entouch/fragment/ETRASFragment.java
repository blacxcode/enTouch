package com.blacxcode.entouch.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.activity.ETAppsActivity;
import com.blacxcode.entouch.activity.ETHelpActivity;
import com.blacxcode.entouch.activity.ETMainActivity;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.data.ETClient;
import com.blacxcode.entouch.data.ETData;
import com.blacxcode.entouch.service.ETConectivityReceiver;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class ETRASFragment extends Fragment implements ETConectivityReceiver.ETConectivityReceiverListener {
    private static final String TAG = ETRASFragment.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    protected ETMainActivity mainActivity;
    protected ETJSONParseData parseData;

    protected CardView statusCardview, port1Cardview, port2Cardview, port3Cardview, port4Cardview, port5Cardview, port6Cardview, port7Cardview, port8Cardview,
                        port1TitleCardview, port2TitleCardview, port3TitleCardview, port4TitleCardview, port5TitleCardview, port6TitleCardview, port7TitleCardview, port8TitleCardview;
    protected ToggleButton toggleButton1, toggleButton2, toggleButton3, toggleButton4, toggleButton5, toggleButton6, toggleButton7, toggleButton8;
    protected ImageButton imageButton1, imageButton2, imageButton3, imageButton4, imageButton5, imageButton6, imageButton7, imageButton8;
    protected EditText  editText1, editText2, editText3, editText4, editText5, editText6, editText7, editText8;
    protected TextView infoStatus, textView1, textView2, textView3, textView4, textView5, textView6, textView7, textView8,
            port1Title, port2Title, port3Title, port4Title, port5Title, port6Title, port7Title, port8Title;
    protected View view;
    protected ImageView lockDevice, statusPort1, statusPort2, statusPort3, statusPort4, statusPort5, statusPort6, statusPort7, statusPort8, help;
    protected LinearLayout panelControl, rasFragment;

    protected DatabaseReference mFirebaseDB;
    protected FirebaseDatabase mFirebaseInstance;

    protected Locale locale = null;

    private String datas[] = new String[40];
    private String retrieveDatas[] = new String[40];
    private String getThemes = "ETColorBlueGrey", getLang = "en";

    public ETRASFragment() {
        // Required empty public constructor
    }

    public static ETRASFragment newInstance() {
        return new ETRASFragment();
    }

    @Override
    public void onAttach(Context context) {
        Log.e(TAG, "onAttach");
        super.onAttach(context);

        if (context instanceof ETMainActivity) {
            mainActivity = (ETMainActivity) context;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            mainActivity.getResources().updateConfiguration(newConfig, mainActivity.getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragment_remote_access, parent, false);

        sharedPreferences = mainActivity.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        parseData = new ETJSONParseData(mainActivity);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDB = mFirebaseInstance.getReference(ETConfig.DB_ENTOUCH);

        rasFragment = view.findViewById(R.id.rasFragment);
        statusCardview = view.findViewById(R.id.statusCardview);
        lockDevice = view.findViewById(R.id.lockDevice);
        help = view.findViewById(R.id.help);
        lockDevice.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.blinking));
        panelControl = view.findViewById(R.id.panelControl);
        infoStatus = view.findViewById(R.id.statusText);

        port1TitleCardview = view.findViewById(R.id.port1TitleCardview);
        port1Title = view.findViewById(R.id.port1Title);
        port1Cardview = view.findViewById(R.id.port1Cardview);
        statusPort1 = view.findViewById(R.id.statusPort1);
        textView1 = view.findViewById(R.id.TextView1);
        editText1 = view.findViewById(R.id.EditText1);
        imageButton1 = view.findViewById(R.id.ImageButton1);
        toggleButton1 = view.findViewById(R.id.toggleButton1);

        port2TitleCardview = view.findViewById(R.id.port2TitleCardview);
        port2Title = view.findViewById(R.id.port2Title);
        port2Cardview = view.findViewById(R.id.port2Cardview);
        statusPort2 = view.findViewById(R.id.statusPort2);
        textView2 = view.findViewById(R.id.TextView2);
        editText2 = view.findViewById(R.id.EditText2);
        imageButton2 = view.findViewById(R.id.ImageButton2);
        toggleButton2 = view.findViewById(R.id.toggleButton2);

        port3TitleCardview = view.findViewById(R.id.port3TitleCardview);
        port3Title = view.findViewById(R.id.port3Title);
        port3Cardview = view.findViewById(R.id.port3Cardview);
        statusPort3 = view.findViewById(R.id.statusPort3);
        textView3 = view.findViewById(R.id.TextView3);
        editText3 = view.findViewById(R.id.EditText3);
        imageButton3 = view.findViewById(R.id.ImageButton3);
        toggleButton3 = view.findViewById(R.id.toggleButton3);

        port4TitleCardview = view.findViewById(R.id.port4TitleCardview);
        port4Title = view.findViewById(R.id.port4Title);
        port4Cardview = view.findViewById(R.id.port4Cardview);
        statusPort4 = view.findViewById(R.id.statusPort4);
        textView4 = view.findViewById(R.id.TextView4);
        editText4 = view.findViewById(R.id.EditText4);
        imageButton4 = view.findViewById(R.id.ImageButton4);
        toggleButton4 = view.findViewById(R.id.toggleButton4);

        port5TitleCardview = view.findViewById(R.id.port5TitleCardview);
        port5Title = view.findViewById(R.id.port5Title);
        port5Cardview = view.findViewById(R.id.port5Cardview);
        statusPort5 = view.findViewById(R.id.statusPort5);
        textView5 = view.findViewById(R.id.TextView5);
        editText5 = view.findViewById(R.id.EditText5);
        imageButton5 = view.findViewById(R.id.ImageButton5);
        toggleButton5 = view.findViewById(R.id.toggleButton5);

        port6TitleCardview = view.findViewById(R.id.port6TitleCardview);
        port6Title = view.findViewById(R.id.port6Title);
        port6Cardview = view.findViewById(R.id.port6Cardview);
        statusPort6 = view.findViewById(R.id.statusPort6);
        textView6 = view.findViewById(R.id.TextView6);
        editText6 = view.findViewById(R.id.EditText6);
        imageButton6 = view.findViewById(R.id.ImageButton6);
        toggleButton6 = view.findViewById(R.id.toggleButton6);

        port7TitleCardview = view.findViewById(R.id.port7TitleCardview);
        port7Title = view.findViewById(R.id.port7Title);
        port7Cardview = view.findViewById(R.id.port7Cardview);
        statusPort7 = view.findViewById(R.id.statusPort7);
        textView7 = view.findViewById(R.id.TextView7);
        editText7 = view.findViewById(R.id.EditText7);
        imageButton7 = view.findViewById(R.id.ImageButton7);
        toggleButton7 = view.findViewById(R.id.toggleButton7);

        port8TitleCardview = view.findViewById(R.id.port8TitleCardview);
        port8Title = view.findViewById(R.id.port8Title);
        port8Cardview = view.findViewById(R.id.port8Cardview);
        statusPort8 = view.findViewById(R.id.statusPort8);
        textView8 = view.findViewById(R.id.TextView8);
        editText8 = view.findViewById(R.id.EditText8);
        imageButton8 = view.findViewById(R.id.ImageButton8);
        toggleButton8 = view.findViewById(R.id.toggleButton8);

        convertBinary(0);
        retrieveData();
        retrieveAccess();
        setThemes();
        refreshUI();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));

                Intent helpOpt = new Intent(mainActivity, ETHelpActivity.class);
                helpOpt.putExtra("linkURL", "help_ras");
                startActivity(helpOpt);
            }
        });

        toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton1.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[0] = "1";
                    } else {
                        datas[0] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-1 : " + datas[0]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-1 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス1のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton1.setSelected(!imageButton1.isSelected());
                if (imageButton1.isSelected()) {
                    editText1.setVisibility(View.VISIBLE);
                    textView1.setVisibility(View.GONE);
                } else {
                    textView1.setText(editText1.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle1", editText1.getText().toString());
                    edit.apply();

                    editText1.setVisibility(View.GONE);
                    textView1.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });

        toggleButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton2.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[1] = "1";
                    } else {
                        datas[1] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-2 : " + datas[1]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-2 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス2のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton2.setSelected(!imageButton2.isSelected());
                if (imageButton2.isSelected()) {
                    editText2.setVisibility(View.VISIBLE);
                    textView2.setVisibility(View.GONE);
                } else {
                    textView2.setText(editText2.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle2", editText2.getText().toString());
                    edit.apply();

                    editText2.setVisibility(View.GONE);
                    textView2.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });

        toggleButton3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton3.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[2] = "1";
                    } else {
                        datas[2] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-3 : " + datas[2]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-3 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス3のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton3.setSelected(!imageButton3.isSelected());
                if (imageButton3.isSelected()) {
                    editText3.setVisibility(View.VISIBLE);
                    textView3.setVisibility(View.GONE);
                } else {
                    textView3.setText(editText3.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle3", editText3.getText().toString());
                    edit.apply();

                    editText3.setVisibility(View.GONE);
                    textView3.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });

        toggleButton4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton4.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[3] = "1";
                    } else {
                        datas[3] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-4 : " + datas[3]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-4 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス4のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton4.setSelected(!imageButton4.isSelected());
                if (imageButton4.isSelected()) {
                    editText4.setVisibility(View.VISIBLE);
                    textView4.setVisibility(View.GONE);
                } else {
                    textView4.setText(editText4.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle4", editText4.getText().toString());
                    edit.apply();

                    editText4.setVisibility(View.GONE);
                    textView4.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });

        toggleButton5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton5.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[4] = "1";
                    } else {
                        datas[4] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-5 : " + datas[4]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-5 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス5のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton5.setSelected(!imageButton5.isSelected());
                if (imageButton5.isSelected()) {
                    editText5.setVisibility(View.VISIBLE);
                    textView5.setVisibility(View.GONE);
                } else {
                    textView5.setText(editText5.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle5", editText5.getText().toString());
                    edit.apply();

                    editText5.setVisibility(View.GONE);
                    textView5.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });

        toggleButton6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton6.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[5] = "1";
                    } else {
                        datas[5] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-6 : " + datas[5]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-6 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス6のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton6.setSelected(!imageButton6.isSelected());
                if (imageButton6.isSelected()) {
                    editText6.setVisibility(View.VISIBLE);
                    textView6.setVisibility(View.GONE);
                } else {
                    textView6.setText(editText6.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle6", editText6.getText().toString());
                    edit.apply();

                    editText6.setVisibility(View.GONE);
                    textView6.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });

        toggleButton7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton7.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[6] = "1";
                    } else {
                        datas[6] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-7 : " + datas[6]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-7 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス7のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton7.setSelected(!imageButton7.isSelected());
                if (imageButton7.isSelected()) {
                    editText7.setVisibility(View.VISIBLE);
                    textView7.setVisibility(View.GONE);
                } else {
                    textView7.setText(editText7.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle7", editText7.getText().toString());
                    edit.apply();

                    editText7.setVisibility(View.GONE);
                    textView7.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });

        toggleButton8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkConnection()) {
                    toggleButton8.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.zoom_in));
                    if (isChecked) {
                        datas[7] = "1";
                    } else {
                        datas[7] = "0";
                    }
                    setACS();
                    retrieveData();
                    parseData.writeLogActivity("logact.log", "Updating data on device-8 : " + datas[7]);
                    parseData.writeLogActivity("logact-in.log", "Memperbarui data pada perangkat-8 : " + datas[0]);
                    parseData.writeLogActivity("logact-ja.log", "デバイス8のデータの更新 : " + datas[0]);
                } else {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        imageButton8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mainActivity, R.anim.click));
                imageButton8.setSelected(!imageButton8.isSelected());
                if (imageButton8.isSelected()) {
                    editText8.setVisibility(View.VISIBLE);
                    textView8.setVisibility(View.GONE);
                } else {
                    textView8.setText(editText8.getText().toString());
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETTitle8", editText8.getText().toString());
                    edit.apply();

                    editText8.setVisibility(View.GONE);
                    textView8.setVisibility(View.VISIBLE);
                }
                refreshDevName();
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        // register connection status listener
        ETAppsActivity.getInstance().setConnectivityListener(this);

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.e(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.e(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.e(TAG, "onNetworkConnectionChanged");

    }

    public boolean checkConnection() {
        return ETConectivityReceiver.isConnected();
    }

    private void refreshUI() {
        /*Permission Status*/
        try {
            if (checkConnection()) {
                if (sharedPreferences.getString("devicestatus", "").equals("approved")) {
                    panelControl.setVisibility(View.VISIBLE);
                    statusCardview.setVisibility(View.GONE);
                } else if (sharedPreferences.getString("devicestatus", "").equals("rejected") ||
                        sharedPreferences.getString("devicestatus", "").equals("request") ||
                        sharedPreferences.getString("devicestatus", "").equals("")) {
                    infoStatus.setText(getResources().getString(R.string.notAllow));
                    panelControl.setVisibility(View.GONE);
                    statusCardview.setVisibility(View.VISIBLE);
                }
            } else {
                infoStatus.setText(getResources().getString(R.string.notData));
                panelControl.setVisibility(View.GONE);
                statusCardview.setVisibility(View.VISIBLE);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        refreshDevName();
        refreshDevStatus();
    }

    private void refreshDevName() {
        port1Title.setText(getResources().getString(R.string.port1Title));
        editText1.setText(sharedPreferences.getString("ETTitle1", ""));
        editText1.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle1", "").equals("")) {
            textView1.setText(getResources().getString(R.string.noDeviceName));
            textView1.setAlpha(0.3f);
        } else {
            textView1.setText(sharedPreferences.getString("ETTitle1", ""));
            textView1.setAlpha(0.9f);
        }

        port2Title.setText(getResources().getString(R.string.port2Title));
        editText2.setText(sharedPreferences.getString("ETTitle2", ""));
        editText2.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle2", "").equals("")) {
            textView2.setText(getResources().getString(R.string.noDeviceName));
            textView2.setAlpha(0.3f);
        } else {
            textView2.setText(sharedPreferences.getString("ETTitle2", ""));
            textView1.setAlpha(0.9f);
        }

        port3Title.setText(getResources().getString(R.string.port3Title));
        editText3.setText(sharedPreferences.getString("ETTitle3", ""));
        editText3.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle3", "").equals("")) {
            textView3.setText(getResources().getString(R.string.noDeviceName));
            textView3.setAlpha(0.3f);
        } else {
            textView3.setText(sharedPreferences.getString("ETTitle3", ""));
            textView1.setAlpha(0.9f);
        }

        port4Title.setText(getResources().getString(R.string.port4Title));
        editText4.setText(sharedPreferences.getString("ETTitle4", ""));
        editText4.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle4", "").equals("")) {
            textView4.setText(getResources().getString(R.string.noDeviceName));
            textView4.setAlpha(0.3f);
        } else {
            textView4.setText(sharedPreferences.getString("ETTitle4", ""));
            textView1.setAlpha(0.9f);
        }

        port5Title.setText(getResources().getString(R.string.port5Title));
        editText5.setText(sharedPreferences.getString("ETTitle5", ""));
        editText5.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle5", "").equals("")) {
            textView5.setText(getResources().getString(R.string.noDeviceName));
            textView5.setAlpha(0.3f);
        } else {
            textView5.setText(sharedPreferences.getString("ETTitle5", ""));
            textView1.setAlpha(0.9f);
        }

        port6Title.setText(getResources().getString(R.string.port6Title));
        editText6.setText(sharedPreferences.getString("ETTitle6", ""));
        editText6.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle6", "").equals("")) {
            textView6.setText(getResources().getString(R.string.noDeviceName));
            textView6.setAlpha(0.3f);
        } else {
            textView6.setText(sharedPreferences.getString("ETTitle6", ""));
            textView1.setAlpha(0.9f);
        }

        port7Title.setText(getResources().getString(R.string.port7Title));
        editText7.setText(sharedPreferences.getString("ETTitle7", ""));
        editText7.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle7", "").equals("")) {
            textView7.setText(getResources().getString(R.string.noDeviceName));
            textView7.setAlpha(0.3f);
        } else {
            textView7.setText(sharedPreferences.getString("ETTitle7", ""));
            textView1.setAlpha(0.9f);
        }

        port8Title.setText(getResources().getString(R.string.port8Title));
        editText8.setText(sharedPreferences.getString("ETTitle8", ""));
        editText8.setHint(getResources().getString(R.string.adddevice));
        if (sharedPreferences.getString("ETTitle8", "").equals("")) {
            textView8.setText(getResources().getString(R.string.noDeviceName));
            textView8.setAlpha(0.3f);
        } else {
            textView8.setText(sharedPreferences.getString("ETTitle8", ""));
            textView1.setAlpha(0.9f);
        }
    }

    private void refreshDevStatus() {
        toggleButton1.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton1.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[0].equals("1")) {
            toggleButton1.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort1);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton1.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton1.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort1);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton1.setBackgroundColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColor300(getThemes), null));
            }
        }

        toggleButton2.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton2.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[1].equals("1")) {
            toggleButton2.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort2);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton2.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton2.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort2);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton2.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }
        }

        toggleButton3.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton3.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[2].equals("1")) {
            toggleButton3.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort3);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton3.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton3.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton3.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort3);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton3.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton3.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }
        }

        toggleButton4.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton4.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[3].equals("1")) {
            toggleButton4.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort4);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton4.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton4.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton4.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort4);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton4.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton4.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }
        }

        toggleButton5.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton5.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[4].equals("1")) {
            toggleButton5.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort5);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton5.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton5.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton5.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort5);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton5.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton5.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }
        }

        toggleButton6.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton6.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[5].equals("1")) {
            toggleButton6.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort6);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton6.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton6.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton6.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort6);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton6.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton6.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }
        }

        toggleButton7.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton7.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[6].equals("1")) {
            toggleButton7.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort7);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton7.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton7.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton7.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort7);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton7.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton7.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }
        }

        toggleButton8.setTextOn(getResources().getString(R.string.deviceOn));
        toggleButton8.setTextOff(getResources().getString(R.string.deviceOff));
        if(retrieveDatas[7].equals("1")) {
            toggleButton8.setChecked(true);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_on_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort8);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton8.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                toggleButton8.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }
        } else {
            toggleButton8.setChecked(false);
            try {
                Glide.with(mainActivity)
                        .load(R.drawable.ic_brightness_1_off_48dp)
                        .apply(RequestOptions.fitCenterTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(statusPort8);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton8.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor300(getThemes))));
            } else {
                toggleButton8.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }
        }
    }

    private void setThemes() {
        statusCardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        rasFragment.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        port1Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        port2Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        port3Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        port4Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        port5Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        port6Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        port7Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        port8Cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));

        toggleButton1.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toggleButton2.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toggleButton3.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toggleButton4.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toggleButton5.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toggleButton6.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toggleButton7.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toggleButton8.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        editText1.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        editText2.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        editText3.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        editText4.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        editText5.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        editText6.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        editText7.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        editText8.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));

        infoStatus.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView1.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView2.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView3.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView4.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView5.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView6.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView7.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        textView8.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = mainActivity.getResources().getConfiguration();
        config.locale = locale;
        mainActivity.getResources().updateConfiguration(config, mainActivity.getResources().getDisplayMetrics());
    }

    public void retrieveData() {
        //Get datasnapshot at your "users" root node
        DatabaseReference ref = mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                                            .child(ETConfig.DB_ENTOUCHDATA);
        ref.addValueEventListener(
            new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //Get map of users in datasnapshot
                    ETData data = dataSnapshot.getValue(ETData.class);

                    if (data != null) {
                        try {
                            convertBinary(Integer.valueOf(data.iotras));
                            refreshDevStatus();
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //handle databaseError
                }
            }
        );
    }

    public void retrieveAccess() {
        //Get datasnapshot at your "users" root node
        DatabaseReference ref = mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                                            .child(ETConfig.DB_ENTOUCHCLIENT);
        ref.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot
                        for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                            ETClient data = dataSnapshot1.getValue(ETClient.class);

                            if (data != null) {
                                if (sharedPreferences.getString("userid", "").equals(dataSnapshot1.getKey())) {
                                    SharedPreferences.Editor edit = sharedPreferences.edit();
                                    edit.putString("devicestatus", data.devicestatus);
                                    edit.apply();

                                    refreshUI();
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });
    }

    public void setACS() {
        try {
            StringBuilder pdata = new StringBuilder("");

            for(int i = 0; i < 8; i++) {
                pdata.append(datas[i]);
            }

            int decimalValue = Integer.valueOf(pdata.toString(), 2);
            mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                    .child(ETConfig.DB_ENTOUCHDATA).child("iotras")
                    .setValue(String.valueOf(decimalValue));
        } catch (Exception e) {
            Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    public void convertBinary(int num) {
        int binary[] = new int[10];
        int index = 0;
        int data = 7;

        while(num > 0){
            binary[index++] = num%2;
            num = num/2;
        }

        for(int i = 0; i < 8; i++) {
            int r = data--;

            retrieveDatas[i] = String.valueOf(binary[r]);
            datas[i] = String.valueOf(binary[r]);
        }
    }
}
