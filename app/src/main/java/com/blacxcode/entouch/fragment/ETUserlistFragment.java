package com.blacxcode.entouch.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.activity.ETAppsActivity;
import com.blacxcode.entouch.activity.ETDevicesActivity;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.data.ETClient;
import com.blacxcode.entouch.data.ETUserlistData;
import com.blacxcode.entouch.service.ETConectivityReceiver;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.ui.adapter.ETUserlistAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by blacXcode on 2/1/2018.
 */

public class ETUserlistFragment extends Fragment implements SearchView.OnQueryTextListener, ETConectivityReceiver.ETConectivityReceiverListener {
    private static final String TAG = ETUserlistFragment.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    protected ETDevicesActivity devicesActivity;

    protected LinearLayout userlistFragment;
    protected Toolbar toolbar_userlist_act;
    protected SwipeRefreshLayout SwipeRefreshLayoutUserlist;
    protected RecyclerView RecyclerViewUserlist;
    protected ETUserlistAdapter userListAdapter;
    protected List<ETUserlistData> userList;
    protected View view;
    protected LinearLayoutManager linearLayoutManager;

    protected DatabaseReference mFirebaseDB;
    protected FirebaseDatabase mFirebaseInstance;

    protected Locale locale = null;

    private String getThemes = "ETColorBlueGrey", getLang = "en";

    public ETUserlistFragment() {
        // Required empty public constructor
    }

    public static ETUserlistFragment newInstance() {
        return new ETUserlistFragment();
    }

    @Override
    public void onAttach(Context context) {
        Log.e(TAG, "onAttach");
        super.onAttach(context);

        if (context instanceof ETDevicesActivity) {
            devicesActivity = (ETDevicesActivity) context;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            devicesActivity.getResources().updateConfiguration(newConfig, devicesActivity.getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragment_userlist, parent, false);

        sharedPreferences = devicesActivity.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        userList = new ArrayList<>();

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDB = mFirebaseInstance.getReference(ETConfig.DB_ENTOUCH);

        userlistFragment = view.findViewById(R.id.userlistFragment);
        SwipeRefreshLayoutUserlist = view.findViewById(R.id.RefreshLayoutUserlist);

        toolbar_userlist_act = view.findViewById(R.id.toolbar_userlist_act);
        try {
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar_userlist_act);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        RecyclerViewUserlist = view.findViewById(R.id.RecyclerViewUserlist);
        RecyclerViewUserlist.setHasFixedSize(true);
        RecyclerViewUserlist.setNestedScrollingEnabled(false);

        linearLayoutManager = new LinearLayoutManager(devicesActivity);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerViewUserlist.setLayoutManager(linearLayoutManager);

        userListAdapter = new ETUserlistAdapter(devicesActivity, userList, ETUserlistFragment.this);
        RecyclerViewUserlist.setItemAnimator(new DefaultItemAnimator()); //try
        RecyclerViewUserlist.setAdapter(userListAdapter);
        RecyclerViewUserlist.invalidate(); //try

        SwipeRefreshLayoutUserlist.setEnabled(true);

        retrieveData();
        setThemes();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        SwipeRefreshLayoutUserlist.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        // register connection status listener
        ETAppsActivity.getInstance().setConnectivityListener(this);
        setLanguage();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.e(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.e(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu_search, MenuInflater inflater) {
        Log.e(TAG, "onCreateOptionsMenu");

        // Inflate the menu; this adds items to the action bar if it is present
        inflater.inflate(R.menu.menu_userlist, menu_search);

        final MenuItem item = menu_search.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<ETUserlistData> filteredModelList = filter(userList, newText);
        userListAdapter.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.e(TAG, "onNetworkConnectionChanged");

    }

    public boolean checkConnection() {
        return ETConectivityReceiver.isConnected();
    }

    private List<ETUserlistData> filter(List<ETUserlistData> models, String query) {
        query = query.toLowerCase();

        final List<ETUserlistData> filteredModelList = new ArrayList<>();
        for (ETUserlistData data : models) {

            final String getUsername, getDeviceStatus;

            getUsername = data.getUsername().toLowerCase();
            getDeviceStatus = data.getDeviceStatus().toLowerCase();

            if (getUsername.contains(query) || getDeviceStatus.contains(query)) {
                filteredModelList.add(data);
            }
        }
        return filteredModelList;
    }

    private void setThemes() {
        userlistFragment.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        toolbar_userlist_act.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = devicesActivity.getResources().getConfiguration();
        config.locale = locale;
        devicesActivity.getResources().updateConfiguration(config, devicesActivity.getResources().getDisplayMetrics());
    }

    public void refreshItems() {
        retrieveData();
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        SwipeRefreshLayoutUserlist.setRefreshing(false);
    }


    public void retrieveData() {
        //Get datasnapshot at your "users" root node
        DatabaseReference ref = mFirebaseDB.child(sharedPreferences.getString("ETDeviceModel", "enTouch-demo"))
                .child(ETConfig.DB_ENTOUCHCLIENT);
        ref.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        userList.clear();
                        //Get map of users in datasnapshot
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            ETUserlistData userlistData = new ETUserlistData();

                            ETClient data = dataSnapshot1.getValue(ETClient.class);

                            if (data != null) {
                                userlistData.setUserId(dataSnapshot1.getKey());
                                userlistData.setDevicekey(data.devicekey);
                                userlistData.setDeviceName(data.devicename);
                                userlistData.setDeviceStatus(data.devicestatus);
                                userlistData.setStatusDate(data.statusdate);
                                userlistData.setUsername(data.username);
                            }

                            userList.add(userlistData);
                        }
                        userListAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                }
        );
    }
}
