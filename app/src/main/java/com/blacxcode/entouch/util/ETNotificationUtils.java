package com.blacxcode.entouch.util;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.ui.ETSetLocalLanguage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by blacXcode on 5/8/2017.
 */

public class ETNotificationUtils {

    private static String TAG = ETNotificationUtils.class.getSimpleName();

    private Context context;

    public ETNotificationUtils(Context context) {
        this.context = context;
    }

    public void showNotificationBG(final String title, final String message, final String location, Intent intent) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);

        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

        // notification icon
        final int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //final PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        final PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

        Uri notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/raw/notification");

        String getUri = sharedPreferences.getString("ETNotifUri", notification.toString());
        Uri notifSound = Uri.parse(getUri);

        showSmallNotificationBG(mBuilder, icon, title, message, location, resultPendingIntent, notifSound);

        if (sharedPreferences.getString("ETCheckSound", "").equals("ON")) {
            // play notification sound
            playNotificationSound();
        }
    }

    private void showSmallNotificationBG(NotificationCompat.Builder mBuilder,
                                         int icon,
                                         String title,
                                         String message,
                                         String location,
                                         PendingIntent resultPendingIntent,
                                         Uri notifSound) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.addLine(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALL))
                .setStyle(inboxStyle)
                .setContentIntent(resultPendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ETConfig.NOTIFICATION_ID, notification);
    }

    public void showNotificationMessage(String title,
                                        String message,
                                        String location,
                                        String timeStamp,
                                        Intent intent) {

        showNotificationMessage(title, message, location, timeStamp, intent, null);
    }

    public void showNotificationMessage(final String title,
                                        final String message,
                                        final String location,
                                        final String timeStamp,
                                        Intent intent,
                                        String imageUrl) {

        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

        // notification icon
        final int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

        final Uri notifSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/raw/notification");

        if (!TextUtils.isEmpty(imageUrl)) {

            if (!imageUrl.equals("") && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                Bitmap bitmap = getBitmapFromURL(imageUrl);

                if (bitmap != null) {
                    showBigNotification(bitmap, mBuilder, icon, title, message, location, timeStamp, resultPendingIntent, notifSound);
                } else {
                    showSmallNotification(mBuilder, icon, title, message, location, timeStamp, resultPendingIntent, notifSound);
                }
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, location, timeStamp, resultPendingIntent, notifSound);
            playNotificationSound();
        }
    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder,
                                       int icon,
                                       String title,
                                       String message,
                                       String location,
                                       String timeStamp,
                                       PendingIntent resultPendingIntent,
                                       Uri notifSound) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.addLine(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(notifSound)
                .setStyle(inboxStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ETConfig.NOTIFICATION_ID, notification);
    }

    private void showBigNotification(Bitmap bitmap,
                                     NotificationCompat.Builder mBuilder,
                                     int icon,
                                     String title,
                                     String message,
                                     String location,
                                     String timeStamp,
                                     PendingIntent resultPendingIntent,
                                     Uri notifSound) {

        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(notifSound)
                .setStyle(bigPictureStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ETConfig.NOTIFICATION_ID_BIG_IMAGE, notification);
    }

    private Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void playNotificationSound() {
        try {
            Uri notification;
            String getUri;

            SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);

            notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/raw/notification");
            getUri = sharedPreferences.getString("ETNotifUri", notification.toString());
            notification = Uri.parse(getUri);

            ETRingtonesService ringtonesService = new ETRingtonesService(context);
            ringtonesService.playNotification(notification);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resumeNotificationSound() {
        try {
            Uri notification;
            String getUri;

            SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);

            notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/raw/notification");
            getUri = sharedPreferences.getString("ETNotifUri", notification.toString());
            notification = Uri.parse(getUri);

            ETRingtonesService ringtonesService = new ETRingtonesService(context);
            ringtonesService.resumeNotification(notification);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pauseNotificationSound() {
        try {
            Uri notification;
            String getUri;

            SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);

            notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/raw/notification");
            getUri = sharedPreferences.getString("ETNotifUri", notification.toString());
            notification = Uri.parse(getUri);

            ETRingtonesService ringtonesService = new ETRingtonesService(context);
            ringtonesService.pauseNotification(notification);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopNotificationSound() {
        try {
            ETRingtonesService ringtonesService = new ETRingtonesService(context);
            ringtonesService.stopNotification();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPlayNotificationSound() {
        try {
            ETRingtonesService ringtonesService = new ETRingtonesService(context);
            return ringtonesService.isNotification();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }

    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private long getTimeMilliSec(String timeStamp) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("ETLanguage", 0);

        ETSetLocalLanguage setLocalLanguage = new ETSetLocalLanguage(context);
        Locale locale = new Locale(setLocalLanguage.getLocale(getLang));
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", locale);
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
