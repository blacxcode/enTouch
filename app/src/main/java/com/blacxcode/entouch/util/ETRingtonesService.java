package com.blacxcode.entouch.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import com.blacxcode.entouch.config.ETConfig;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by blacXcode on 5/17/2017.
 */

public class ETRingtonesService {
    public Context context;
    private static MediaPlayer mediaPlayer = null;

    public ETRingtonesService(Context context) {
        this.context = context;
    }

    public void playNotification(Uri Notification) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
//        Float getLeftVol = sharedPreferences.getFloat("ETLeftVol", 0.0f);
//        Float getRightVol = sharedPreferences.getFloat("ETRightVol", 0.0f);

        stopNotification();

        mediaPlayer = MediaPlayer.create(context, Notification);

        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setLooping(false);
//            mediaPlayer.setVolume(getLeftVol, getRightVol);
            mediaPlayer.start();
        }
    }

    public void resumeNotification(Uri Notification) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
//        Float getLeftVol = sharedPreferences.getFloat("ETLeftVol", 0.0f);
//        Float getRightVol = sharedPreferences.getFloat("ETRightVol", 0.0f);
        int length = sharedPreferences.getInt("ETMPlayerLength", 0);

        mediaPlayer = MediaPlayer.create(context, Notification);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setLooping(false);
//        mediaPlayer.setVolume(getLeftVol, getRightVol);
        mediaPlayer.seekTo(length);
        mediaPlayer.start();
    }

    public void pauseNotification(Uri Notification) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);

        mediaPlayer = MediaPlayer.create(context, Notification);
        mediaPlayer.pause();
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("ETMPlayerLength", mediaPlayer.getCurrentPosition());
        edit.apply();

    }

    public void stopNotification() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }
    }

    public boolean isNotification() {
        return mediaPlayer.isPlaying();
    }
}