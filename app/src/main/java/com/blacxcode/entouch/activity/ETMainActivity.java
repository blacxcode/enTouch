package com.blacxcode.entouch.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.fragment.ETCoreListenerFragment;
import com.blacxcode.entouch.fragment.ETHomeFragment;
import com.blacxcode.entouch.fragment.ETRASFragment;
import com.blacxcode.entouch.fragment.ETSNSFragment;
import com.blacxcode.entouch.ui.ETDefaultAnimationHandler;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.messaging.FirebaseMessaging;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;
import com.oguzdev.circularfloatingactionmenu.library.animation.DefaultAnimationHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ETMainActivity extends AppCompatActivity {
    private static final String TAG = ETMainActivity.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private InterstitialAd mInterstitialAd;
    private FloatingActionButton fab;
    private FloatingActionMenu rightLowerMenu;
    protected Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private FrameLayout maincontainer;
    private ImageView imgThemes, imgSettings, imgAbout, imgDevices, imgQuit;
    private SubActionButton btnThemes, btnSettings, btnAbout, btnDevices, btnQuit;
    private SubActionButton.Builder itemBuilder;
    private TextView tabHome, tabACS, tabSNS;

    private Handler handler;
    private Runnable runnable;

    protected Locale locale = null;

    private int getTabPos = 0;
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            AnimationSet s = new AnimationSet(false);

            if (position == 0) {
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                tabHome.setAnimation(s);
                getTabPos = 0;
                showFloatingActionButton();
                //checkConnection();
            } else if (position == 1) {
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                tabACS.setAnimation(s);
                getTabPos = 1;
                hideFloatingActionButton();
                //checkConnection();
            } else {
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                tabSNS.setAnimation(s);
                getTabPos = 2;
                hideFloatingActionButton();
                //checkConnection();
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    private String getThemes = "ETColorBlueGrey", getLang = "en";

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        mInterstitialAd = new InterstitialAd(ETMainActivity.this);

        fab = findViewById(R.id.fab_main);
        viewPager = findViewById(R.id.viewpager_main_act);
        tabLayout = findViewById(R.id.tabs_main_act);
        maincontainer = findViewById(R.id.maincontainer);

        SetFloatingActionButton();

        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

//        loadInterstitial();

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (maincontainer != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            ETCoreListenerFragment coreListenerFragment = new ETCoreListenerFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            coreListenerFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.maincontainer, coreListenerFragment);
            fragmentTransaction.commit();
        }

        refreshUI();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ETConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ETConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(ETConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");
                    String location = intent.getStringExtra("location");

                    //Toast.makeText(ETMainActivity.this,  "ETMainActivity LatLng :" + location , Toast.LENGTH_LONG).show();

                    Intent notify = new Intent(ETMainActivity.this, ETNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    notify.putExtra("location", location);
                    startActivity(notify);

                    //ETJSONParseData.writeLogActivity(context, "logact.log", "Received notification from " + title);
                }
            }
        };
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();

        if (getTabPos == 0) {
            showFloatingActionButton();
        } else {
            hideFloatingActionButton();
        }
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        if (getTabPos == 0) {
            showFloatingActionButton();
        } else {
            hideFloatingActionButton();
        }

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ETNotificationUtils.clearNotifications(this);

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        hideFloatingActionButton();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();

        hideFloatingActionButton();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();

        hideFloatingActionButton();
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed");

        if (getTabPos == 0) {
            if (fab.getRotation() == 180) {
                fab.performClick();
            } else {
                moveTaskToBack(true);
            }
        } else if (getTabPos == 1) {
            viewPager.setCurrentItem(0);
        } else if (getTabPos == 2) {
            viewPager.setCurrentItem(1);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.e(TAG, "onKeyDown");

        switch (keyCode) {
            case KeyEvent.KEYCODE_MENU:
                //your Action code
                if (getTabPos == 0) {
                    fab.performClick();
                }
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        //Log.e(TAG, "dispatchTouchEvent");

        // Your code here
        if (getTabPos == 0) {
            if (fab.getRotation() == 180) {
                fab.performClick();
            }
        }
        return super.dispatchTouchEvent(event);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
//    @Override
//    public void onNetworkConnectionChanged(boolean isConnected) {
//        Log.e(TAG, "onNetworkConnectionChanged");
//
//        showNetworkInfo(isConnected);
//    }
    private void loadInterstitial() {
        final AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //.addTestDevice("15C0119EA7504242")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void showInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.i("showInterstitial", "Ad did not load");
            //Toast.makeText(ETMainActivity.this, "Ad did not load", Toast.LENGTH_SHORT).show();
        }
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }

                if (getTabPos == 0) {
                    showFloatingActionButton();
                    if (fab.getRotation() == 180) {
                        fab.performClick();
                    }
                } else {
                    hideFloatingActionButton();
                }

                Log.i("onAdLoaded", "Ad loaded.");
                //Toast.makeText(ETMainActivity.this, "Ad loaded.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.i("onAdFailedToLoad", "Ad failed to load with error code : " + errorCode);

                if (getTabPos == 0) {
                    showFloatingActionButton();
                    if (fab.getRotation() == 180) {
                        fab.performClick();
                    }
                } else {
                    hideFloatingActionButton();
                }
                //Toast.makeText(ETMainActivity.this, "Ad failed to load with error code : " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.i("onAdOpened", "Ad opened.");

                if (getTabPos == 0) {
                    showFloatingActionButton();
                    if (fab.getRotation() == 180) {
                        fab.performClick();
                    }
                } else {
                    hideFloatingActionButton();
                }
                //Toast.makeText(ETMainActivity.this, "Ad opened.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                showInterstitial();
                Log.i("onAdClosed", "Ad closed.");

                if (getTabPos == 0) {
                    showFloatingActionButton();
                    if (fab.getRotation() == 180) {
                        fab.performClick();
                    }
                } else {
                    hideFloatingActionButton();
                }
                //Toast.makeText(ETMainActivity.this, "Ad closed.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.i("onAdLeftApplication", "Ad left application.");

                if (getTabPos == 0) {
                    showFloatingActionButton();
                    if (fab.getRotation() == 180) {
                        fab.performClick();
                    }
                } else {
                    hideFloatingActionButton();
                }
                //Toast.makeText(ETMainActivity.this, "Ad left application.", Toast.LENGTH_SHORT).show();
            }
        });
        return interstitialAd;
    }

    private void setupTabIcons() {
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));

        tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setTag(getResources().getString(R.string.fragment_name_home));
        tabHome.setText(getResources().getString(R.string.fragment_name_home));
        tabHome.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_notifications_active_white_48dp, 0, 0);
        tabHome.setAnimation(s);
        tabLayout.getTabAt(0).setCustomView(tabHome);

        tabACS = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabACS.setTag(getResources().getString(R.string.fragment_name_ras));
        tabACS.setText(getResources().getString(R.string.fragment_name_ras));
        tabACS.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_settings_remote_white_48dp, 0, 0);
        tabACS.setAnimation(s);
        tabLayout.getTabAt(1).setCustomView(tabACS);

        tabSNS = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabSNS.setTag(getResources().getString(R.string.fragment_name_sns));
        tabSNS.setText(getResources().getString(R.string.fragment_name_sns));
        tabSNS.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_person_white_48dp, 0, 0);
        tabSNS.setAnimation(s);
        tabLayout.getTabAt(2).setCustomView(tabSNS);
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFrag(new ETHomeFragment(), getString(R.string.fragment_name_home));
        viewPagerAdapter.addFrag(new ETRASFragment(), getString(R.string.fragment_name_ras));
        viewPagerAdapter.addFrag(new ETSNSFragment(), getString(R.string.fragment_name_sns));
        viewPager.setAdapter(viewPagerAdapter);
    }

    public void SetFloatingActionButton() {
        String getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");

        LayerDrawable layerDrawable = (LayerDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.circle_floating_item, null).getCurrent();
        GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.gradientDrawble);
        gradientDrawable.setColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));

        itemBuilder = new SubActionButton.Builder(this);
        itemBuilder.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_floating_item, null)).build();

        imgThemes = new ImageView(this);
        imgThemes.setImageResource(R.drawable.ic_color_lens_white_48dp);

        imgSettings = new ImageView(this);
        imgSettings.setImageResource(R.drawable.ic_settings_white_48dp);

        imgDevices = new ImageView(this);
        imgDevices.setImageResource(R.drawable.ic_memory_white_48dp);

        imgAbout = new ImageView(this);
        imgAbout.setImageResource(R.drawable.ic_info_outline_white_48dp);

        imgQuit = new ImageView(this);
        imgQuit.setImageResource(R.drawable.ic_exit_to_app_white_48dp);

        btnThemes = itemBuilder.setContentView(imgThemes).build();
        btnSettings = itemBuilder.setContentView(imgSettings).build();
        btnDevices = itemBuilder.setContentView(imgDevices).build();
        btnAbout = itemBuilder.setContentView(imgAbout).build();
        btnQuit = itemBuilder.setContentView(imgQuit).build();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        rightLowerMenu = new FloatingActionMenu.Builder(this)
                .setRadius((size.x / 2) - (size.x / 5))
                .enableAnimations()
                .setAnimationHandler(new DefaultAnimationHandler())
                .addSubActionView(btnQuit)
                .addSubActionView(btnAbout)
                .addSubActionView(btnDevices)
                .addSubActionView(btnSettings)
                .addSubActionView(btnThemes)
                .attachTo(fab)
                .build();

        rightLowerMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @Override
            public void onMenuOpened(FloatingActionMenu menu) {
                cancelFab();
                fab.setRotation(0);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 180);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fab, pvhR);
                animation.start();
            }

            @Override
            public void onMenuClosed(FloatingActionMenu menu) {
                cancelFab();
                fab.setRotation(180);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 0);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fab, pvhR);
                animation.start();
            }
        });

        btnThemes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(1000, true));
                v.setAnimation(s);

                Thread animeThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;

                            while (waited < 1000) {
                                sleep(50);
                                waited += 50;
                            }

                            Intent intent = new Intent(ETMainActivity.this, ETThemesActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                };
                animeThread.start();

                rightLowerMenu.close(true);
                hideFloatingActionButton();
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(1000, true));
                v.setAnimation(s);

                Thread animeThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;

                            while (waited < 1000) {
                                sleep(50);
                                waited += 50;
                            }

                            Intent intent = new Intent(ETMainActivity.this, ETSettingsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                };
                animeThread.start();

                rightLowerMenu.close(true);
                hideFloatingActionButton();
            }
        });

        btnDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(1000, true));
                v.setAnimation(s);

                Thread animeThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;

                            while (waited < 1000) {
                                sleep(50);
                                waited += 50;
                            }

                            boolean getAuthLogin = sharedPreferences.getBoolean("ETAuthlogin", false);
                            Intent intent;
                            if (getAuthLogin) {
                                intent = new Intent(ETMainActivity.this, ETDevicesActivity.class);
                            } else {
                                intent = new Intent(ETMainActivity.this, ETAuthorityActivity.class);
                            }
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                };
                animeThread.start();

                rightLowerMenu.close(true);
                hideFloatingActionButton();
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(1000, true));
                v.setAnimation(s);

                Thread animeThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;

                            while (waited < 1000) {
                                sleep(50);
                                waited += 50;
                            }

                            Intent intent = new Intent(ETMainActivity.this, ETAboutActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                };
                animeThread.start();

                rightLowerMenu.close(true);
                hideFloatingActionButton();
            }
        });

        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(ETMainActivity.this, R.anim.zoom_out));
                s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(1000, true));
                v.setAnimation(s);

                moveTaskToBack(true);
            }
        });
    }

    void refreshUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes))));
        fab.setRippleColor(getResources().getColor(ETSetColorTheme.getColorPrimary(getThemes)));
        SetFloatingActionButton();

        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(ETSetColorTheme.getColor200(getThemes)));
        tabLayout.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));

        maincontainer.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));

        tabHome.setText(getResources().getString(R.string.fragment_name_home));
        tabACS.setText(getResources().getString(R.string.fragment_name_ras));
        tabSNS.setText(getResources().getString(R.string.fragment_name_sns));
    }

    public void showFloatingActionButton() {
        fab.show();
        fab.setVisibility(View.VISIBLE);
    }

    public void hideFloatingActionButton() {
        fab.hide();
        fab.setVisibility(View.GONE);
        rightLowerMenu.close(true);
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

//    public boolean checkConnection() {
//        boolean isConnected = ETConectivityReceiver.isConnected();
//        showNetworkInfo(isConnected);
//        return isConnected;
//    }

    // Showing the status in Snackbar
//    public void showNetworkInfo(boolean isConnected) {
//        String message;
//
//        if (isConnected) {
//            message = getResources().getString(R.string.connected);
//            Snackbar snackbar = Snackbar.make(findViewById(R.id.fab_main), message, Snackbar.LENGTH_LONG);
//            View view = snackbar.getView();
//            TextView textView = view.findViewById(android.support.design.R.id.snackbar_text);
//            textView.setTextColor(Color.GREEN);
//            //snackbar.show();
//        } else {
//            message = getResources().getString(R.string.disconnect);
//            Snackbar snackbar = Snackbar.make(findViewById(R.id.fab_main), message, Snackbar.LENGTH_LONG);
//            View view = snackbar.getView();
//            TextView textView = view.findViewById(android.support.design.R.id.snackbar_text);
//            textView.setTextColor(Color.RED);
//            snackbar.show();
//        }
//    }

    public void showFab(final int duration) {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {

                Animation animation = new AlphaAnimation(0.1f, 1.0f);
                animation.setDuration(duration);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        cancelFab();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                fab.startAnimation(animation);
            }
        };
        handler.postDelayed(runnable, 10000);
    }

    public void hideFab(final int duration) {
        Animation animation = new AlphaAnimation(1.0f, 0.1f);
        animation.setDuration(duration);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fab.setAlpha(0.1f);
                showFab(duration);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fab.startAnimation(animation);
    }

    public void cancelFab() {
        fab.clearAnimation();
        fab.setAlpha(1.0f);
        if (runnable != null) {
            handler.removeCallbacks(runnable);
            runnable = null;
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
