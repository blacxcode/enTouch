package com.blacxcode.entouch.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.ui.ETDefaultAnimationHandler;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.blacxcode.entouch.util.ETViewPagerScroller;
import com.google.firebase.messaging.FirebaseMessaging;

import java.lang.reflect.Field;
import java.util.Locale;

public class ETAboutActivity extends AppCompatActivity {
    private static final int SLIDER_WHATSNEW = 0;
    private static final int SLIDER_ABOUT = 1;
    private static final int SLIDER_LICENSE = 2;
    private static final int SLIDER_HELP = 3;
    private static final int SLIDE_TIME_TRANSITION = 500;
    private static final String TAG = ETAboutActivity.class.getSimpleName();
    protected SharedPreferences sharedPreferences;

    private ETViewPagerScroller viewPagerScroller;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private ViewPager viewPager;
    private LinearLayout dotsLayout;
    private MyViewPagerAdapter myViewPagerAdapter;
    private TextView[] dots;
    private ImageView prevNavigation, nextNavigation, logo;
    private Handler handler;
    private Runnable runnable;

    protected Locale locale = null;

    private String[] activityTitles;
    private String getThemes = "ETColorBlueGrey", getLang = "en";
    private int[] layouts;
    private int slideItemIndex = 0;
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            slideItemIndex = position;
            setToolbarTitle();

            //MyViewPagerAdapter viewPagerAdapter = new MyViewPagerAdapter();
            //viewPagerAdapter.loadContent(position);
            showNavigation(1000);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        prevNavigation = findViewById(R.id.prevNavigation);
        nextNavigation = findViewById(R.id.nextNavigation);
        prevNavigation.setVisibility(View.GONE);
        nextNavigation.setVisibility(View.GONE);

        activityTitles = getResources().getStringArray(R.array.slide_item_activity_titles);

        layouts = new int[]{
                R.layout.slider_whatsnew,
                R.layout.slider_about,
                R.layout.slider_license,
                R.layout.slider_help
        };

        addBottomDots(0);

        setCustomScrollerToViewPager();
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        setToolbarTitle();
        refreshUI();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ETConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ETConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(ETConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");
                    String location = intent.getStringExtra("location");

                    //Toast.makeText(ETAboutActivity.this,  "ETMainActivity LatLng :" + location , Toast.LENGTH_LONG).show();

                    Intent notify = new Intent(ETAboutActivity.this, ETNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    notify.putExtra("location", location);
                    startActivity(notify);
                }
            }
        };

        prevNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(500, true));
                v.setAnimation(s);

                if (slideItemIndex == 3) {
                    viewPager.setCurrentItem(2);
                } else if (slideItemIndex == 2) {
                    viewPager.setCurrentItem(1);
                } else if (slideItemIndex == 1) {
                    viewPager.setCurrentItem(0);
                } else {
                    //prevNavigation.setEnabled(false);
                    prevNavigation.startAnimation(AnimationUtils.loadAnimation(ETAboutActivity.this, R.anim.hide));
                }
            }
        });

        nextNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(500, true));
                v.setAnimation(s);

                if (slideItemIndex == 0) {
                    viewPager.setCurrentItem(1);
                } else if (slideItemIndex == 1) {
                    viewPager.setCurrentItem(2);
                } else if (slideItemIndex == 2) {
                    viewPager.setCurrentItem(3);
                } else {
                    //nextNavigation.setEnabled(false);
                    nextNavigation.startAnimation(AnimationUtils.loadAnimation(ETAboutActivity.this, R.anim.hide));
                }
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ETNotificationUtils.clearNotifications(this);

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        // Your code here
        if (!(event.getAction() == MotionEvent.ACTION_SCROLL)
                || !(event.getAction() == MotionEvent.ACTION_BUTTON_PRESS)) {
            prevNavigation.setVisibility(View.VISIBLE);
            nextNavigation.setVisibility(View.VISIBLE);

            showNavigation(1000);
        }

        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e(TAG, "onOptionsItemSelected");

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void refreshUI() {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null)));
        }

        prevNavigation.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColorPrimary(getThemes)));
        nextNavigation.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColorPrimary(getThemes)));
    }

    private void setToolbarTitle() {
        //getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(activityTitles[slideItemIndex]);
        }
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int colorsActive;
        int colorsInactive;

        colorsActive = ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColor900(getThemes), null);
        colorsInactive = ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimary(getThemes), null);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                dots[i].setText(Html.fromHtml("&#8226;", Html.FROM_HTML_MODE_LEGACY));
            } else {
                dots[i].setText(Html.fromHtml("&#8226;"));
            }
            dots[i].setTextSize(40);
            dots[i].setTextColor(colorsInactive);
            dots[i].setGravity(Gravity.CENTER_HORIZONTAL);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[currentPage].setTextSize(40);
            dots[currentPage].startAnimation(AnimationUtils.loadAnimation(ETAboutActivity.this, R.anim.zoom_out));
            dots[currentPage].setTextColor(colorsActive);
            dots[currentPage].setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    private void setCustomScrollerToViewPager() {
        setCustomScrollerToViewPager(new LinearInterpolator(), SLIDE_TIME_TRANSITION);
    }

    private void setCustomScrollerToViewPager(Interpolator interpolator, int duration) {
        try {
            viewPagerScroller = new ETViewPagerScroller(this, interpolator, duration);
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager, viewPagerScroller);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void showNavigation(int duration) {
        hideNavigationOff();

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(duration);
        Animation animation2 = new AlphaAnimation(0.0f, 1.0f);
        animation2.setDuration(duration);

        switch (slideItemIndex) {
            case SLIDER_WHATSNEW:
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        nextNavigation.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                nextNavigation.startAnimation(animation);
                break;

            case SLIDER_ABOUT:
            case SLIDER_LICENSE:
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        prevNavigation.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                prevNavigation.startAnimation(animation);

                animation2.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        nextNavigation.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                nextNavigation.startAnimation(animation2);
                break;

            case SLIDER_HELP:
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        prevNavigation.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                prevNavigation.startAnimation(animation);
                break;

            default:
                // do nothing
                break;
        }

        hideNavigationOn(duration);
    }

    public void hideNavigationOn(final int duration) {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {

                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(duration);
                Animation animation2 = new AlphaAnimation(1.0f, 0.0f);
                animation2.setDuration(duration);

                switch (slideItemIndex) {
                    case SLIDER_WHATSNEW:
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                nextNavigation.setEnabled(false);
                                hideNavigationOff();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        nextNavigation.startAnimation(animation);
                        break;

                    case SLIDER_ABOUT:
                    case SLIDER_LICENSE:
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                prevNavigation.setEnabled(false);
                                hideNavigationOff();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        prevNavigation.startAnimation(animation);

                        animation2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                nextNavigation.setEnabled(false);
                                hideNavigationOff();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        nextNavigation.startAnimation(animation2);
                        break;

                    case SLIDER_HELP:
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                prevNavigation.setEnabled(false);
                                hideNavigationOff();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        prevNavigation.startAnimation(animation);
                        break;

                    default:
                        // do nothing
                        break;
                }
            }
        };
        handler.postDelayed(runnable, 5000);
    }

    public void hideNavigationOff() {
        prevNavigation.startAnimation(AnimationUtils.loadAnimation(ETAboutActivity.this, R.anim.hide));
        nextNavigation.startAnimation(AnimationUtils.loadAnimation(ETAboutActivity.this, R.anim.hide));

        if (runnable != null) {
            handler.removeCallbacks(runnable);
            runnable = null;
        }
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        private CardView whatsnew_cardview, about_cardview, acknowledgements_cardview, help_cardview;
        private TextView whatsnew_header, about_header, about_app_name, about_app_version;
        private WebView whatsnew_content, about_content, acknowledgements_content, help_content;

        MyViewPagerAdapter() {
            // Required empty public constructor
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);

            container.addView(view);
            loadContent(position);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            destroyWebView(position);
            container.removeView(view);
        }

        @SuppressLint("Deprecation")
        private void loadContent(int position) {
            setLanguage();
            switch (position) {
                case SLIDER_WHATSNEW:
                    whatsnew_cardview = findViewById(R.id.whatsnew_cardview);
                    whatsnew_header = findViewById(R.id.whatsnew_header);
                    whatsnew_content = findViewById(R.id.whatsnew_content);

                    whatsnew_cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
                    String whatsnewheader = getResources().getString(R.string.slide_whatsnew) + getResources().getString(R.string.versionName);
                    whatsnew_header.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
                    whatsnew_header.setText(whatsnewheader);

                    whatsnew_content.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
                    whatsnew_content.setVerticalScrollBarEnabled(false);
                    whatsnew_content.loadData(getResources().getString(R.string.whats_new), "text/html", "utf-8");
                    break;

                case SLIDER_ABOUT:
                    about_cardview = findViewById(R.id.about_cardview);
                    logo = findViewById(R.id.logo);
                    about_header = findViewById(R.id.about_header);
                    about_app_name = findViewById(R.id.about_app_name);
                    about_app_version = findViewById(R.id.about_app_version);
                    about_content = findViewById(R.id.about_content);

                    String AppVersion = getResources().getString(R.string.app_version) + getResources().getString(R.string.versionName);
                    about_cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes))); //bug
                    about_header.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
                    about_header.setText(getResources().getString(R.string.slide_about));
                    about_app_name.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
                    about_app_name.setText(getResources().getString(R.string.app_name));
                    about_app_version.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
                    about_app_version.setText(AppVersion);

                    about_content.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
                    about_content.setVerticalScrollBarEnabled(false);
                    about_content.loadData(getResources().getString(R.string.about), "text/html", "utf-8");

                    logo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AnimationSet animationSet = new AnimationSet(false);
                            animationSet.addAnimation(AnimationUtils.loadAnimation(ETAboutActivity.this, R.anim.sequential_about_show_up));
                            animationSet.addAnimation(AnimationUtils.loadAnimation(ETAboutActivity.this, R.anim.sequential_about_click));
                            logo.startAnimation(animationSet);
                        }
                    });
                    break;

                case SLIDER_LICENSE:
                    acknowledgements_cardview = findViewById(R.id.license_cardview);
                    acknowledgements_content = findViewById(R.id.license_content);

                    acknowledgements_cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
                    acknowledgements_content.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

                    acknowledgements_content.setVerticalScrollBarEnabled(false);
                    acknowledgements_content.loadData(getResources().getString(R.string.license), "text/html", "utf-8");
                    break;

                case SLIDER_HELP:
                    help_cardview = findViewById(R.id.help_cardview);
                    help_content = findViewById(R.id.help_content);

                    help_cardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
                    help_content.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

                    help_content.getSettings().setJavaScriptEnabled(true);
                    help_content.getSettings().setAppCacheEnabled(true);
                    help_content.getSettings().setLoadWithOverviewMode(true);
                    help_content.getSettings().setUseWideViewPort(true);
                    WebSettings settings = help_content.getSettings();
                    settings.setDomStorageEnabled(true);

                    help_content.loadUrl(getResources().getString(R.string.help_url));
                    help_content.setWebViewClient(new WebViewClient() {

                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            //return super.shouldOverrideUrlLoading(view, url);
                            if (url.equals(getResources().getString(R.string.help_url)) || url.equals(getResources().getString(R.string.help_url) + "?m=1")) {
                                view.loadUrl(url);
                                return super.shouldOverrideUrlLoading(view, getResources().getString(R.string.help_url));
                            } else if (url.equals(getResources().getString(R.string.privacypolicy_url)) || url.equals(getResources().getString(R.string.privacypolicy_url) + "?m=1")) {
                                view.loadUrl(url);
                                return super.shouldOverrideUrlLoading(view, getResources().getString(R.string.privacypolicy_url));
                            } else if (url.equals(getResources().getString(R.string.termservices_url)) || url.equals(getResources().getString(R.string.termservices_url) + "?m=1")) {
                                view.loadUrl(url);
                                return super.shouldOverrideUrlLoading(view, getResources().getString(R.string.termservices_url));
                            } else {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                view.getContext().startActivity(intent);
                                return true;
                            }
                        }

                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                            String help = "file:///android_asset/" + getResources().getString(R.string.help);
                            help_content.loadUrl(help);
                        }
                    });
                    break;

                default:
                    // do nothing
                    break;
            }
        }

        private void destroyWebView(int position) {
            switch (position) {
                case SLIDER_WHATSNEW:
                    if (whatsnew_content != null) {
                        whatsnew_content.clearHistory();
                        whatsnew_content.clearCache(true);
                        whatsnew_content.loadUrl("about:blank");
                        whatsnew_content.onPause();
                        whatsnew_content.removeAllViews();
                        whatsnew_content.destroyDrawingCache();
                        whatsnew_content.pauseTimers();
                        whatsnew_content.destroy();
                        whatsnew_content = null;
                    }
                    break;

                case SLIDER_ABOUT:
                    if (about_content != null) {
                        about_content.clearHistory();
                        about_content.clearCache(true);
                        about_content.loadUrl("about:blank");
                        about_content.onPause();
                        about_content.removeAllViews();
                        about_content.destroyDrawingCache();
                        about_content.pauseTimers();
                        about_content.destroy();
                        about_content = null;
                    }
                    break;

                case SLIDER_LICENSE:
                    if (acknowledgements_content != null) {
                        acknowledgements_content.clearHistory();
                        acknowledgements_content.clearCache(true);
                        acknowledgements_content.loadUrl("about:blank");
                        acknowledgements_content.onPause();
                        acknowledgements_content.removeAllViews();
                        acknowledgements_content.destroyDrawingCache();
                        acknowledgements_content.pauseTimers();
                        acknowledgements_content.destroy();
                        acknowledgements_content = null;
                    }
                    break;

                case SLIDER_HELP:
                    help_content.clearHistory();
                    help_content.clearCache(true);
                    help_content.loadUrl("about:blank");
                    help_content.onPause();
                    help_content.removeAllViews();
                    help_content.destroyDrawingCache();
                    help_content.pauseTimers();
                    help_content.destroy();
                    help_content = null;
                    break;

                default:
                    // do nothing
                    break;
            }
        }

    }
}
