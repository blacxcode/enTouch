package com.blacxcode.entouch.activity;

import android.app.Application;
import android.util.Log;

import com.blacxcode.entouch.service.ETConectivityReceiver;

/**
 * Created by blacXcode on 2/6/2018.
 */

public class ETAppsActivity extends Application {
    private static final String TAG = ETAppsActivity.class.getSimpleName();

    private static ETAppsActivity mInstance;

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        super.onCreate();

        mInstance = this;
    }

    public static synchronized ETAppsActivity getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ETConectivityReceiver.ETConectivityReceiverListener listener) {
        ETConectivityReceiver.connectivityReceiverListener = listener;
    }
}
