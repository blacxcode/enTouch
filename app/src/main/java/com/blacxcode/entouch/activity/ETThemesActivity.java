package com.blacxcode.entouch.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETDefaultAnimationHandler;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import java.util.Locale;

public class ETThemesActivity extends AppCompatActivity {
    private static final String TAG = ETThemesActivity.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    protected ETJSONParseData parseData;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private TextView themesTitle, themesNavigation;
    private FloatingActionButton setColor1, setColor2, setColor3, setColor4, setColor5,
            setColor6, setColor7, setColor8, setColor9, setColor10, setColor11, setColor12,
            setColor13, setColor14;
    SwipeBackLayout swipeBackLayout;
    RelativeLayout mainLayout;

    protected Locale locale = null;

    private String getThemes = "ETColorBlueGrey", getLang = "en";

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.TRANSPARENT);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        parseData = new ETJSONParseData(ETThemesActivity.this);

        swipeBackLayout = findViewById(R.id.swipeBackLayout);
        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);
        mainLayout = findViewById(R.id.mainLayout);
        themesTitle = findViewById(R.id.themesTitle);
        themesNavigation = findViewById(R.id.themesNavigation);
        setColor1 = findViewById(R.id.set_color1);
        setColor2 = findViewById(R.id.set_color2);
        setColor3 = findViewById(R.id.set_color3);
        setColor4 = findViewById(R.id.set_color4);
        setColor5 = findViewById(R.id.set_color5);
        setColor6 = findViewById(R.id.set_color6);
        setColor7 = findViewById(R.id.set_color7);
        setColor8 = findViewById(R.id.set_color8);
        setColor9 = findViewById(R.id.set_color9);
        setColor10 = findViewById(R.id.set_color10);
        setColor11 = findViewById(R.id.set_color11);
        setColor12 = findViewById(R.id.set_color12);
        setColor13 = findViewById(R.id.set_color13);
        setColor14 = findViewById(R.id.set_color14);

        mainLayout.startAnimation(AnimationUtils.loadAnimation(ETThemesActivity.this, R.anim.slide_from_right));

        refreshUI();

        setColor1.setOnClickListener(view -> {
            setColor("ETColorPink");
            clearCheck();
            setColor1.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor2.setOnClickListener(view -> {
            setColor("ETColorPurple");
            clearCheck();
            setColor2.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor3.setOnClickListener(view -> {
            setColor("ETColorDeepPurple");
            clearCheck();
            setColor3.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor4.setOnClickListener(view -> {
            setColor("ETColorBlue");
            clearCheck();
            setColor4.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor5.setOnClickListener(view -> {
            setColor("ETColorLightBlue");
            clearCheck();
            setColor5.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor6.setOnClickListener(view -> {
            setColor("ETColorCyan");
            clearCheck();
            setColor6.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor7.setOnClickListener(view -> {
            setColor("ETColorLightGreen");
            clearCheck();
            setColor7.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor8.setOnClickListener(view -> {
            setColor("ETColorLime");
            clearCheck();
            setColor8.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor9.setOnClickListener(view -> {
            setColor("ETColorGrey");
            clearCheck();
            setColor9.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor10.setOnClickListener(view -> {
            setColor("ETColorBlueGrey");
            clearCheck();
            setColor10.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor11.setOnClickListener(view -> {
            setColor("ETColorBrown");
            clearCheck();
            setColor11.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor12.setOnClickListener(view -> {
            setColor("ETColorTeal");
            clearCheck();
            setColor12.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor13.setOnClickListener(view -> {
            setColor("ETColorIndigo");
            clearCheck();
            setColor13.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        setColor14.setOnClickListener(view -> {
            setColor("ETColorRed");
            clearCheck();
            setColor14.setImageResource(R.drawable.ic_check_white_48dp);
            runAnim(view);
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ETConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ETConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(ETConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");
                    String location = intent.getStringExtra("location");

                    //Toast.makeText(ETThemesActivity.this,  "ETMainActivity LatLng :" + location , Toast.LENGTH_LONG).show();

                    Intent notify = new Intent(ETThemesActivity.this, ETNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    notify.putExtra("location", location);
                    startActivity(notify);
                }
            }
        };
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ETNotificationUtils.clearNotifications(this);

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    public void runAnim(View view) {
        String getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(AnimationUtils.loadAnimation(ETThemesActivity.this, R.anim.zoom_in));
        s.addAnimation(ETDefaultAnimationHandler.DisapperAnimation(1000, true));
        view.setAnimation(s);

        Thread animeThread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;

                    while (waited < 2000) {
                        sleep(50);
                        waited += 50;
                    }
                    Intent intent = new Intent(ETThemesActivity.this, ETMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    // do nothing
                }
            }
        };
        animeThread.start();
        themesTitle.setText(getResources().getString(R.string.themes_choose));
        themesTitle.setTextColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));
    }

    public void refreshUI() {
        //themesTitle.setBackgroundColor(ContextCompat.getColor(this, ETSetColorTheme.getColorPrimary(getThemes)));
        themesTitle.setTextColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));
        themesTitle.setText(getResources().getString(R.string.themes_title));

        themesNavigation.setTextColor(Color.WHITE);
        themesNavigation.setText(getResources().getString(R.string.themes_navigation));
        themesNavigation.startAnimation(AnimationUtils.loadAnimation(ETThemesActivity.this, R.anim.blinking));

        switch (getThemes) {
            case "ETColorPink":
                setColor1.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorPurple":
                setColor2.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorDeepPurple":
                setColor3.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorBlue":
                setColor4.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorLightBlue":
                setColor5.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorCyan":
                setColor6.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorLightGreen":
                setColor7.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorLime":
                setColor8.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorGrey":
                setColor9.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "ETColorBlueGrey":
                setColor10.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorBrown":
                setColor11.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorTeal":
                setColor12.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            case "ETColorIndigo":
                setColor13.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "ETColorRed":
                setColor14.setImageResource(R.drawable.ic_check_white_48dp);
                break;

            default:
                // do nothing
                break;
        }
    }

    public void clearCheck() {
        setColor1.setImageResource(android.R.color.transparent);
        setColor2.setImageResource(android.R.color.transparent);
        setColor3.setImageResource(android.R.color.transparent);
        setColor4.setImageResource(android.R.color.transparent);
        setColor5.setImageResource(android.R.color.transparent);
        setColor6.setImageResource(android.R.color.transparent);
        setColor7.setImageResource(android.R.color.transparent);
        setColor8.setImageResource(android.R.color.transparent);
        setColor9.setImageResource(android.R.color.transparent);
        setColor10.setImageResource(android.R.color.transparent);
        setColor11.setImageResource(android.R.color.transparent);
        setColor12.setImageResource(android.R.color.transparent);
        setColor13.setImageResource(android.R.color.transparent);
        setColor14.setImageResource(android.R.color.transparent);
    }

    private void setColor(String setColor) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("ETThemes", setColor);
        edit.apply();

        String[] parse = setColor.split("ETColor");

        parseData.writeLogActivity("logact.log", "Theme changed : " + parse[1]);
        parseData.writeLogActivity("logact-in.log", "Tema diubah : " + parse[1]);
        parseData.writeLogActivity("logact-ja.log", "テーマが変更されました : " + parse[1]);
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
