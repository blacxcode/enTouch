package com.blacxcode.entouch.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.fragment.ETConfigFragment;
import com.blacxcode.entouch.fragment.ETCoreListenerFragment;
import com.blacxcode.entouch.fragment.ETUserlistFragment;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by blacXcode on 2/1/2018.
 */

public class ETDevicesActivity extends AppCompatActivity {
    private static final String TAG = ETDevicesActivity.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    //    protected InterstitialAd mInterstitialAd;
    protected Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FrameLayout devicescontainer;
    private TextView tabConfig, tabUserlist;

    protected Locale locale = null;

    private int getTabPos = 0;
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            AnimationSet s = new AnimationSet(false);

            if (position == 0) {
                s.addAnimation(AnimationUtils.loadAnimation(ETDevicesActivity.this, R.anim.zoom_in));
                tabConfig.setAnimation(s);
                getTabPos = 0;
            } else {
                s.addAnimation(AnimationUtils.loadAnimation(ETDevicesActivity.this, R.anim.zoom_in));
                tabUserlist.setAnimation(s);
                getTabPos = 1;
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    private String getThemes = "ETColorBlueGrey", getLang = "en";

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        viewPager = findViewById(R.id.viewpager_devices_act);
        tabLayout = findViewById(R.id.tabs_devices_act);
//        mInterstitialAd = newInterstitialAd();
        devicescontainer = findViewById(R.id.devicescontainer);

        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        //loadInterstitial();

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (devicescontainer != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            ETCoreListenerFragment coreListenerFragment = new ETCoreListenerFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            coreListenerFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.devicescontainer, coreListenerFragment);
            fragmentTransaction.commit();
        }

        refreshUI();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ETConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ETConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(ETConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");
                    String location = intent.getStringExtra("location");

                    //Toast.makeText(ETDevicesActivity.this,  "ETMainActivity LatLng :" + location , Toast.LENGTH_LONG).show();

                    Intent notify = new Intent(ETDevicesActivity.this, ETNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    notify.putExtra("location", location);
                    startActivity(notify);
                }
            }
        };
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ETNotificationUtils.clearNotifications(this);
        //Toast.makeText(this, "onResume Activity", Toast.LENGTH_SHORT).show();

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed");

        if (getTabPos == 0) {
            Intent intent = new Intent(ETDevicesActivity.this, ETMainActivity.class);
            startActivity(intent);
        } else if (getTabPos == 1) {
            viewPager.setCurrentItem(0);
        }
    }

//    private void loadInterstitial() {
//        final AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice("15C0119EA7504242")
//                .build();
//        mInterstitialAd.loadAd(adRequest);
//    }

//    private void showInterstitial() {
//        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
//            mInterstitialAd.show();
//        } else {
//            Log.i("showInterstitial", "Ad did not load");
//            //Toast.makeText(ETDevicesActivity.this, "Ad did not load", Toast.LENGTH_SHORT).show();
//        }
//    }

//    private InterstitialAd newInterstitialAd() {
//        InterstitialAd interstitialAd = new InterstitialAd(this);
//        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial));
//        interstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                }
//                Log.i("onAdLoaded", "Ad loaded.");
//                //Toast.makeText(ETDevicesActivity.this, "Ad loaded.", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                super.onAdFailedToLoad(errorCode);
//                Log.i("onAdFailedToLoad", "Ad failed to load with error code : " + errorCode);
//                //Toast.makeText(ETDevicesActivity.this, "Ad failed to load with error code : " + errorCode, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdOpened() {
//                super.onAdOpened();
//                Log.i("onAdOpened", "Ad opened.");
//                //Toast.makeText(ETDevicesActivity.this, "Ad opened.", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdClosed() {
//                super.onAdClosed();
//                showInterstitial();
//                Log.i("onAdClosed", "Ad closed.");
//                //Toast.makeText(ETDevicesActivity.this, "Ad closed.", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdLeftApplication() {
//                super.onAdLeftApplication();
//                Log.i("onAdLeftApplication", "Ad left application.");
//                //Toast.makeText(ETDevicesActivity.this, "Ad left application.", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return interstitialAd;
//    }

    void refreshUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));
            window.setNavigationBarColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));
        }

        tabLayout.setSelectedTabIndicatorColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorAccent(getThemes), null));
        tabLayout.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));

        devicescontainer.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));
    }

    private void setupTabIcons() {
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(AnimationUtils.loadAnimation(ETDevicesActivity.this, R.anim.zoom_out));

        tabConfig = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabConfig.setTag(getResources().getString(R.string.fragment_name_config));
        tabConfig.setText(getResources().getString(R.string.fragment_name_config));
        tabConfig.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_memory_white_48dp, 0, 0);
        tabConfig.setAnimation(s);
        tabLayout.getTabAt(0).setCustomView(tabConfig);

        tabUserlist = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabUserlist.setTag(getResources().getString(R.string.fragment_name_userlist));
        tabUserlist.setText(getResources().getString(R.string.fragment_name_userlist));
        tabUserlist.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_supervisor_account_white_48dp, 0, 0);
        tabUserlist.setAnimation(s);
        tabLayout.getTabAt(1).setCustomView(tabUserlist);
    }

    private void setupViewPager(ViewPager viewPager) {
        ETDevicesActivity.ViewPagerAdapter adapter = new ETDevicesActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ETConfigFragment(), getString(R.string.fragment_name_config));
        adapter.addFrag(new ETUserlistFragment(), getString(R.string.fragment_name_userlist));
        viewPager.setAdapter(adapter);
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
