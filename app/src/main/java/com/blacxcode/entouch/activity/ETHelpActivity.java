package com.blacxcode.entouch.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import java.util.Locale;

/**
 * Created by blacXcode on 2/21/2018.
 */

public class ETHelpActivity extends AppCompatActivity {
    private final static int DEFAULT_SWIPING_THRESHOLD = 200; //20
    private final static int DEFAULT_SWIPED_THRESHOLD = 1000; //100
    private static final String TAG = ETHelpActivity.class.getSimpleName();
    protected SharedPreferences sharedPreferences;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private CardView HelpCardView;
    private WebView HelpWebView;
    private TextView HelpTitle;
    private ImageView HelpIcon;
    private SwipeBackLayout swipeBackLayout;

    protected Locale locale = null;

    private String getThemes = "ETColorBlueGrey", getLang = "en";

    private float xDown, xUp;
    private float yDown, yUp;
    private float xMove, yMove;

    public void onAttachedToWindow() {
        Log.e(TAG, "onAttachedToWindow");
        super.onAttachedToWindow();

        Window window = getWindow();
        window.setFormat(PixelFormat.TRANSPARENT);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        HelpCardView = findViewById(R.id.HelpCardView);
        HelpWebView = findViewById(R.id.HelpWebView);
        HelpTitle = findViewById(R.id.HelpTitle);
        HelpIcon = findViewById(R.id.HelpIcon);
        swipeBackLayout = findViewById(R.id.swipeBackLayout);

        loadHelpUrl(getData());
        refreshUI(true);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ETConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ETConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(ETConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");
                    String location = intent.getStringExtra("location");

                    //Toast.makeText(ETAuthorityActivity.this,  "ETMainActivity LatLng :" + location , Toast.LENGTH_LONG).show();

                    Intent notify = new Intent(ETHelpActivity.this, ETNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    notify.putExtra("location", location);
                    startActivity(notify);
                }
            }
        };

        HelpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ETHelpActivity.this, R.anim.click));
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(ETHelpActivity.this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(ETHelpActivity.this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ETNotificationUtils.clearNotifications(ETHelpActivity.this);

        setLanguage();
        refreshUI(false);
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        LocalBroadcastManager.getInstance(ETHelpActivity.this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();

        disposeHelpUrl();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        //Log.e(TAG, "dispatchTouchEvent");

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // user started touching the screen
                xDown = event.getX();
                yDown = event.getY();
                break;

            case MotionEvent.ACTION_UP:   // user stopped touching the screen
                xUp = event.getX();
                yUp = event.getY();

                final boolean swipedHorizontally = Math.abs(xUp - xDown) > DEFAULT_SWIPED_THRESHOLD;
                final boolean swipedVertically = Math.abs(yUp - yDown) > DEFAULT_SWIPED_THRESHOLD;

                if (swipedHorizontally) {
                    final boolean swipedRight = xUp > xDown;
                    final boolean swipedLeft = xUp < xDown;

                    if (swipedRight) {
                        //isEventConsumed = swipeListener.onSwipedRight(event);
                        //Toast.makeText(this, "swipedRight", Toast.LENGTH_LONG).show();
                        //swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);
                    }
                    if (swipedLeft) {
                        //isEventConsumed |= swipeListener.onSwipedLeft(event);
                        //Toast.makeText(this, "swipedLeft", Toast.LENGTH_LONG).show();
                        //swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.RIGHT);
                    }
                }

                if (swipedVertically) {
                    final boolean swipedDown = yDown < yUp;
                    final boolean swipedUp = yDown > yUp;
                    if (swipedDown) {
                        //isEventConsumed |= swipeListener.onSwipedDown(event);
                        //Toast.makeText(this, "swipedDown", Toast.LENGTH_LONG).show();
                        //swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.TOP);
                    }
                    if (swipedUp) {
                        //isEventConsumed |= swipeListener.onSwipedUp(event);
                        //Toast.makeText(this, "swipedUp", Toast.LENGTH_LONG).show();
                        //swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.BOTTOM);
                    }
                }
                break;

            case MotionEvent.ACTION_MOVE: // user is moving finger on the screen
                xMove = event.getX();
                yMove = event.getY();
                final boolean isSwipingHorizontally = Math.abs(xMove - xDown) > DEFAULT_SWIPING_THRESHOLD;
                final boolean isSwipingVertically = Math.abs(yMove - yDown) > DEFAULT_SWIPING_THRESHOLD;

                if (isSwipingHorizontally) {
                    final boolean isSwipingRight = xMove > xDown;
                    final boolean isSwipingLeft = xMove < xDown;

                    if (isSwipingRight) {
                        //swipeListener.onSwipingRight(event);
                        //Toast.makeText(this, "isSwipingRight", Toast.LENGTH_LONG).show();
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);
                    }
                    if (isSwipingLeft) {
                        //swipeListener.onSwipingLeft(event);
                        //Toast.makeText(this, "isSwipingLeft", Toast.LENGTH_LONG).show();
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.RIGHT);
                    }
                }

                if (isSwipingVertically) {
                    final boolean isSwipingDown = yDown < yMove;
                    final boolean isSwipingUp = yDown > yMove;

                    if (isSwipingDown) {
                        //swipeListener.onSwipingDown(event);
                        //Toast.makeText(this, "isSwipingDown", Toast.LENGTH_LONG).show();
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);
                    }
                    if (isSwipingUp) {
                        //swipeListener.onSwipingUp(event);
                        //Toast.makeText(this, "isSwipingUp", Toast.LENGTH_LONG).show();
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.RIGHT);
                    }
                }
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    public void refreshUI(boolean anim) {
        HelpTitle.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        HelpCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        HelpWebView.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));

        if (anim) {
            HelpCardView.startAnimation(AnimationUtils.loadAnimation(ETHelpActivity.this, R.anim.show_up));
        }
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private String getData() {
        String help_url = null;
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String getHelp = bundle.getString("linkURL");

            switch (getHelp) {
                case "help_login":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_ssid":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_device":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_config":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_auth":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;

                case "help_regid":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;

                case "help_ras":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;

                case "help_find":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_lang":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_notif":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_alert":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_tts":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_log":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
                case "help_rd":
                    help_url = "file:///android_asset/" + getResources().getString(R.string.help_no_content);
                    break;
            }
        }

        return help_url;
    }

    public void loadHelpUrl(String help_url) {
        HelpWebView.getSettings().setJavaScriptEnabled(true);
        HelpWebView.getSettings().setAppCacheEnabled(true);
        HelpWebView.getSettings().setLoadWithOverviewMode(true);
        HelpWebView.getSettings().setUseWideViewPort(true);
        WebSettings settings = HelpWebView.getSettings();
        settings.setDomStorageEnabled(true);

        HelpWebView.loadUrl(help_url);
    }

    public void disposeHelpUrl() {
        if (HelpWebView != null) {
            HelpWebView.clearHistory();
            HelpWebView.clearCache(true);
            HelpWebView.loadUrl("about:blank");
            HelpWebView.onPause();
            HelpWebView.removeAllViews();
            HelpWebView.destroyDrawingCache();
            HelpWebView.pauseTimers();
            HelpWebView.destroy();
            HelpWebView = null;
        }
    }
}
