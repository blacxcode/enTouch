package com.blacxcode.entouch.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.blacxcode.entouch.util.ETRingtonesService;
import com.google.common.base.Optional;
import com.liuguangqiang.swipeback.SwipeBackLayout;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.speech.tts.TextToSpeech.Engine.ACTION_CHECK_TTS_DATA;

/**
 * Created by blacXcode on 5/28/2017.
 */

public class ETNotifyActivity extends AppCompatActivity {
    private final static int DEFAULT_SWIPING_THRESHOLD = 200; //20
    private final static int DEFAULT_SWIPED_THRESHOLD = 1000; //100
    private static final String TAG = ETNotifyActivity.class.getSimpleName();
    private static List languageProfiles;
    private static LanguageDetector languageDetector;
    private static TextObjectFactory textObjectFactory;
    protected SharedPreferences sharedPreferences;
    protected ETJSONParseData parseData;
    private ETSplashActivity langEngineDetection;
    private ImageButton hideMe, showMaps;
    private TextView headerMessage, pushMessage, langDetect, dateDetect;
    private TextToSpeech tts;
    private CardView alertTitleCardview, alertCardview, pushMessageCardview, langDetectCardView, dateDetectCardView;
    private FloatingActionButton ttsPlayBtn;
    private SwipeBackLayout swipeBackLayout;
    protected View view;
    private Handler handlerPlaytss;
    private Handler handlerLoading;
    private Runnable runnablePlaytss;
    protected Locale locale = null;
    protected Bundle bundle = null;

    private String detectedLanguage = "", getThemes = "ETColorBlueGrey", getLang = "en";
    private int volume = 0, progressStatus = 0;
    private float getSpeechrate = 0.0f, getPitch = 0.0f;
    private String getTitle = "", getMessage = "", getLocation = "";

    private float xDown, xUp;
    private float yDown, yUp;
    private float xMove, yMove;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        bundle = getIntent().getExtras();

        setLanguage();

        parseData = new ETJSONParseData(ETNotifyActivity.this);

        view = findViewById(R.id.LinearLayout);
        swipeBackLayout = findViewById(R.id.swipeBackLayout);
        headerMessage = findViewById(R.id.headerMessage);
        pushMessage = findViewById(R.id.pushMessage);
        langDetect = findViewById(R.id.langDetect);
        dateDetect = findViewById(R.id.dateDetect);
        hideMe = findViewById(R.id.hideMe);
        showMaps = findViewById(R.id.showMaps);
        alertTitleCardview = findViewById(R.id.alertTitleCardview);
        alertCardview = findViewById(R.id.alertCardview);
        pushMessageCardview = findViewById(R.id.pushMessageCardview);
        langDetectCardView = findViewById(R.id.langDetectCardView);
        dateDetectCardView = findViewById(R.id.dateDetectCardView);
        ttsPlayBtn = findViewById(R.id.ttsPlayBtn);

        getSpeechrate = sharedPreferences.getFloat("ETSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("ETPitch", 1.0f);
        volume = sharedPreferences.getInt("ETVolume", 8);
        volume = volume / 10;

        handlerLoading = new Handler();

        initTTS();
        isNotif();

        refreshUI();

        headerMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ETNotifyActivity.this, R.anim.vibrate));
                alertTitleCardview.clearAnimation();

                ETNotificationUtils notificationUtils = new ETNotificationUtils(ETNotifyActivity.this);
                notificationUtils.stopNotificationSound();
            }
        });

        ttsPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ETNotifyActivity.this, R.anim.click));
                alertTitleCardview.clearAnimation();

                ttsPlayBtn.setSelected(!ttsPlayBtn.isSelected());

                if (ttsPlayBtn.isSelected()) {
                    ETNotificationUtils notificationUtils = new ETNotificationUtils(ETNotifyActivity.this);
                    notificationUtils.stopNotificationSound();

                    speak(pushMessage.getText().toString());
                    isPlaytss();
                } else {
                    if (tts.isSpeaking()) {
                        tts.stop();

                        if (runnablePlaytss != null) {
                            handlerPlaytss.removeCallbacks(runnablePlaytss);
                            runnablePlaytss = null;
                        }
                    }
                }
            }
        });

        alertTitleCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertTitleCardview.clearAnimation();

                ETNotificationUtils notificationUtils = new ETNotificationUtils(ETNotifyActivity.this);
                notificationUtils.stopNotificationSound();
            }
        });

        hideMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ETNotificationUtils notificationUtils = new ETNotificationUtils(ETNotifyActivity.this);
                notificationUtils.stopNotificationSound();

                AnimationSet animationSet = new AnimationSet(false);
                animationSet.addAnimation(AnimationUtils.loadAnimation(ETNotifyActivity.this, R.anim.close_up));
                animationSet.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        alertTitleCardview.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        closeNotif();
                        alertTitleCardview.setVisibility(View.GONE);
                        onBackPressed();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                alertTitleCardview.setAnimation(animationSet);
                alertTitleCardview.startAnimation(animationSet);
            }
        });

        showMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ETNotifyActivity.this, R.anim.click));
                alertTitleCardview.clearAnimation();

                ETNotificationUtils notificationUtils = new ETNotificationUtils(ETNotifyActivity.this);
                notificationUtils.stopNotificationSound();

                Intent maps = new Intent(ETNotifyActivity.this, ETMapsActivity.class);
                maps.putExtra("location", getLocation);
                maps.putExtra("sender", getTitle);
                startActivity(maps);
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        if (tts == null) {
            initTTS();
        }

        setLanguage();
        refreshUI();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();

        closeNotif();

        // tts.shutdown should implemnent in shutdown.
        if (tts != null) {
            tts.shutdown();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // user started touching the screen
                xDown = event.getX();
                yDown = event.getY();
                break;

            case MotionEvent.ACTION_UP:   // user stopped touching the screen
                xUp = event.getX();
                yUp = event.getY();

                final boolean swipedHorizontally = Math.abs(xUp - xDown) > DEFAULT_SWIPED_THRESHOLD;
                final boolean swipedVertically = Math.abs(yUp - yDown) > DEFAULT_SWIPED_THRESHOLD;

                if (swipedHorizontally) {
                    final boolean swipedRight = xUp > xDown;
                    final boolean swipedLeft = xUp < xDown;

                    if (swipedRight) {
                        // do something
                    }
                    if (swipedLeft) {
                        // do something
                    }
                }

                if (swipedVertically) {
                    final boolean swipedDown = yDown < yUp;
                    final boolean swipedUp = yDown > yUp;
                    if (swipedDown) {
                        // do something
                    }
                    if (swipedUp) {
                        // do something
                    }
                }
                break;

            case MotionEvent.ACTION_MOVE: // user is moving finger on the screen
                xMove = event.getX();
                yMove = event.getY();
                final boolean isSwipingHorizontally = Math.abs(xMove - xDown) > DEFAULT_SWIPING_THRESHOLD;
                final boolean isSwipingVertically = Math.abs(yMove - yDown) > DEFAULT_SWIPING_THRESHOLD;

                if (isSwipingHorizontally) {
                    final boolean isSwipingRight = xMove > xDown;
                    final boolean isSwipingLeft = xMove < xDown;

                    if (isSwipingRight) {
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);
                    }
                    if (isSwipingLeft) {
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.RIGHT);
                    }
                }

                if (isSwipingVertically) {
                    final boolean isSwipingDown = yDown < yMove;
                    final boolean isSwipingUp = yDown > yMove;

                    if (isSwipingDown) {
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.TOP);
                    }
                    if (isSwipingUp) {
                        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.BOTTOM);
                    }
                }
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    public void refreshUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        pushMessageCardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor200(getThemes)));

        alertCardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));

        langDetectCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
        dateDetectCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));

        ttsPlayBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes))));
        ttsPlayBtn.setRippleColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimary(getThemes), null));

        pushMessage.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        langDetect.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        dateDetect.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));

        alertTitleCardview.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
    }

    private void isNotif() {
        if (bundle != null) {
            getTitle = bundle.getString("title");
            getMessage = bundle.getString("message");
            getLocation = bundle.getString("location");

            String setTitle = getTitle + getResources().getString(R.string.says);
            headerMessage.setText(setTitle);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                pushMessage.setText(Html.fromHtml(getMessage, Html.FROM_HTML_MODE_LEGACY));
            } else {
                pushMessage.setText(Html.fromHtml(getMessage));
            }

            langEngineDetection = new ETSplashActivity();

            try {
                detectedLanguage = langEngineDetection.detectLang(pushMessage.getText().toString());
            } catch (NullPointerException e) {
                e.printStackTrace();

                //load all languages:
                try {
                    languageProfiles = new LanguageProfileReader().readAllBuiltIn();
                } catch (IOException i) {
                    i.printStackTrace();
                }

                //build language detector:
                languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                        .withProfiles(languageProfiles)
                        .build();

                //create a text object factory
                textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText();

                detectedLanguage = detectLang(pushMessage.getText().toString());

                // Initialize a new instance of progress dialog
                final ProgressDialog pd = new ProgressDialog(ETNotifyActivity.this);

                // Set progress dialog style horizontal
                pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                // Set the progress dialog background color transparent
                pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                pd.setIndeterminate(false);
                // Finally, show the progress dialog
                pd.show();
                // Set the progress status zero on each button click
                progressStatus = 0;

                // Start the lengthy operation in a background thread
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (progressStatus < 100) {
                            // Update the progress status
                            progressStatus += 1;

                            // Try to sleep the thread for 20 milliseconds
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            // Update the progress bar
                            handlerLoading.post(new Runnable() {
                                @Override
                                public void run() {
                                    // Update the progress status
                                    pd.setProgress(progressStatus);
                                    // If task execution completed
                                    if (progressStatus == 100) {
                                        // Dismiss/hide the progress dialog
                                        pd.dismiss();
                                    }
                                }
                            });
                        }
                    }
                }).start(); // Start the operation
            }

            if (detectedLanguage == null || detectedLanguage.equals("")) {
                ttsPlayBtn.setVisibility(View.GONE);
            } else {
                ttsPlayBtn.setVisibility(View.VISIBLE);
                langDetect.setText(detectedLanguage);
            }

            if (getLocation == null || getLocation.equals("")) {
                showMaps.setVisibility(View.GONE);
            } else {
                showMaps.setVisibility(View.VISIBLE);
            }

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
            String statusdate = simpledateformat.format(calendar.getTime());
            dateDetect.setText(statusdate);

            alertTitleCardview.setVisibility(View.VISIBLE);
            alertTitleCardview.startAnimation(AnimationUtils.loadAnimation(ETNotifyActivity.this, R.anim.home_alert_zoom_out));

            parseData.writeLogActivity("logact.log", "Receive notifications from " + getTitle);
            parseData.writeLogActivity("logact-in.log", "Menerima notifikasi dari " + getTitle);
            parseData.writeLogActivity("logact-ja.log", getTitle + "さん, からの通知を受け取る");


            parseData.writeLogMessage("logmsg.log",
                    "<br><font size=\"1\"><p align=\"right\"><i>" + statusdate + "</i></p></font>" +
                            "<font size=\"2\"><p p align=\"left\"><b>from&ensp;&ensp;&ensp;&ensp;&ensp;: </b>" + getTitle +
                            "<br><b>location&ensp;&ensp;: </b>" + "<a href=\"https://www.google.com/maps/search/?api=1&query=" + getLocation + "\" target=\"_blank\">" + getLocation + "</a>" +
                            "<br><b>message&ensp;: </b></p>" +
                            "<p align=\"justify\">" + getMessage + "</p></font><br>"
            );

            parseData.writeLogMessage("logmsg-in.log",
                    "<br><font size=\"1\"><p align=\"right\"><i>" + statusdate + "</i></p></font>" +
                            "<font size=\"2\"><p p align=\"left\"><b>dari&ensp;&ensp;&ensp;&ensp;&ensp;: </b>" + getTitle +
                            "<br><b>lokasi&ensp;&ensp;: </b>" + "<a href=\"https://www.google.com/maps/search/?api=1&query=" + getLocation + "\" target=\"_blank\">" + getLocation + "</a>" +
                            "<br><b>pesan&ensp;&ensp;: </b></p>" +
                            "<p align=\"justify\">" + getMessage + "</p></font><br>"
            );

            parseData.writeLogMessage("logmsg-ja.log",
                    "<br><font size=\"1\"><p align=\"right\"><i>" + statusdate + "</i></p></font>" +
                            "<font size=\"2\"><p p align=\"left\"><b>から&ensp;&ensp;&ensp;&ensp;&ensp;: </b>" + getTitle +
                            "<br><b>ロケーション&ensp;&ensp;: </b>" + "<a href=\"https://www.google.com/maps/search/?api=1&query=" + getLocation + "\" target=\"_blank\">" + getLocation + "</a>" +
                            "<br><b>メッセージ&ensp;: </b></p>" +
                            "<p align=\"justify\">" + getMessage + "</p></font><br>"
            );
        }
    }

    private void initTTS() {
        tts = new TextToSpeech(ETNotifyActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.ERROR || status == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), Snackbar.LENGTH_LONG).show();
                }

                if (status == TextToSpeech.SUCCESS) {
                    tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String utteranceId) {
                            ttsPlayBtn.setSelected(true);
                            //Toast.makeText(ETNotifyActivity.this, "TextToSpeech onStart", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onDone(String utteranceId) {
                            ttsPlayBtn.setSelected(false);
                            //Toast.makeText(ETNotifyActivity.this, "TextToSpeech onDone", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(String utteranceId) {
                            ttsPlayBtn.setSelected(false);
                            //Toast.makeText(ETNotifyActivity.this, "TextToSpeech onError", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    @SuppressLint("Deprecation")
    private void speak(String stts) {
        getSpeechrate = sharedPreferences.getFloat("ETSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("ETPitch", 1.0f);

        float pitch = (getPitch == 0.0f) ? 0.1f : getPitch;
        float speechrate = (getSpeechrate == 0.0f) ? 0.1f : getSpeechrate;

        if (checkVoiceData(detectedLanguage)) {
            Locale setLang = new Locale(detectedLanguage);

            tts.setLanguage(setLang);
            tts.setPitch(pitch);
            tts.setSpeechRate(speechrate);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null, null);
            } else {
                tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    private boolean checkVoiceData(String lang) {
        int availability;
        boolean available = false;

        Locale locale = new Locale(lang);

        availability = tts.isLanguageAvailable(locale);
        switch (availability) {
            case TextToSpeech.LANG_COUNTRY_AVAILABLE:
            case TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:
            case TextToSpeech.LANG_AVAILABLE:
                //Snackbar.make(view, getResources().getString(R.string.ttsLangAvailable), Snackbar.LENGTH_SHORT).show();
                available = true;
                break;

            case TextToSpeech.LANG_NOT_SUPPORTED:
                Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), Snackbar.LENGTH_SHORT).show();

                Intent intent = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                ArrayList<String> languages = new ArrayList<>();

                languages.add(detectedLanguage);
                intent.putStringArrayListExtra(ACTION_CHECK_TTS_DATA, languages);
                startActivity(intent);

                available = false;
                break;

            case TextToSpeech.LANG_MISSING_DATA:
                Snackbar.make(view, getResources().getString(R.string.ttsLangMissingData), Snackbar.LENGTH_SHORT).show();

                available = false;
                break;

            default:
                break;
        }

        return available;
    }

    private void isPlaytss() {
        handlerPlaytss = new Handler();
        runnablePlaytss = new Runnable() {

            @Override
            public void run() {
                if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                    try {
                        if (tts.isSpeaking()) {
                            ttsPlayBtn.setSelected(true);
                        } else {
                            ttsPlayBtn.setSelected(false);

                            if (runnablePlaytss != null) {
                                handlerPlaytss.removeCallbacks(runnablePlaytss);
                                runnablePlaytss = null;
                            }
                        }
                        ttsPlayBtn.refreshDrawableState();
                    } catch (Exception e) {
                        Log.e("Runnable", "{0} exception caught.", e);
                    }
                }
                handlerPlaytss.postDelayed(this, 1000);
            }
        };
        handlerPlaytss.postDelayed(runnablePlaytss, 1000);
    }

    public String detectLang(String text) {
        TextObject textObject = textObjectFactory.forText(text);
        Optional<LdLocale> lang = languageDetector.detect(textObject);
        LdLocale locale = lang.orNull();
        return locale == null ? null : locale.getLanguage();
    }

    private void closeNotif() {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("ETNOTIF", false);
        edit.apply();

        ttsPlayBtn.setSelected(false);

        if (tts != null) {
            if (tts.isSpeaking()) {
                tts.stop();
            }
        }

        ETRingtonesService ringtonesService = new ETRingtonesService(ETNotifyActivity.this);
        ringtonesService.stopNotification();

        if (runnablePlaytss != null) {
            handlerPlaytss.removeCallbacks(runnablePlaytss);
            runnablePlaytss = null;
        }

        ETNotificationUtils notificationUtils = new ETNotificationUtils(ETNotifyActivity.this);
        notificationUtils.stopNotificationSound();
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
