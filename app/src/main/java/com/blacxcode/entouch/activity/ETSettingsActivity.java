package com.blacxcode.entouch.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETAESCrypto;
import com.blacxcode.entouch.util.ETHexConvert;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ETSettingsActivity extends AppCompatActivity {
    private static final String TAG = ETSettingsActivity.class.getSimpleName();

    private static final int ACCESS_COARSE_LOCATION_REQUEST_CODE = 100;
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 41;
    private static final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 42;

    protected SharedPreferences sharedPreferences;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ETNotificationUtils notificationUtils;
    protected ETJSONParseData parseData;
    private ETAESCrypto crypto;
    ETHexConvert hexConvert;

    protected Toolbar toolbar;
    private LinearLayout settingLayout;
    private InterstitialAd mInterstitialAd;
    private CardView searchDeviceTitleCardView, searchDeviceCardView, groupSearchDeviceCardView, searchDeviceBtnCardView,
            languageTitleCardView, languageCardView, groupLangCardView,
            notifTitleCardView, notifCardView, groupPathNotifCardView, pathNotifInfoCardView, setNotifBtnCardView,
            notifBrowseBtnCardView, groupVolumeNotifCardView, volumeNotifCardView, playNotifBtnCardView, //lvolumeCardView, rvolumeCardView,
            alertTitleCardView, alertCardView, alertInfoCardView, alertToggleCardView,
            ttsTitleCardView, ttsCardView, groupTTSCardView, speechRateCardView, pitchCardView, ttsBtnCardView,
            logTitleCardView, logCardView, delLogMsgCardView, delLogMsgBtnCardView, delLogActCardView, delLogActBtnCardView,
            rdTitleCardView, rdCardView, rdInfoCardView, rdBtnCardView;
    private Button searchDeviceBtn, setNotifBtn, notifBrowseBtn, playNotifBtn, ttsBtn, rdBtn, delLogMsgBtn, delLogActBtn;
    private ImageButton alertNotif;
    private TextView searchDeviceTitle, deviceFound, deviceSelected,
            languageTitle, selectLanguage,
            notifTitle, pathNotifInfo, notifSelectUri, volumeNotifProgress,
            alertTitle, alertInfoText,
            ttsTitle, speechRateProgress, pitchProgress,
            rdTitle, rdInfoText,
            logInfoTitle, delLogMsgText, delLogActText;
    private SeekBar masterVolumeNotif, ttsSpeechRate, ttsPitch;
    private ToggleButton alertToggleBtn;
    private Locale setLang;
    private TextToSpeech tts;
    private AudioManager audioManager;
    private MaterialSpinner spinnerLanguage, spinnerSearch;
    protected View view;
    protected ImageView helpFind, helpLanguage, helpNotif, helpAlert, helpTTS, helpLog, helpRD;

    private WifiManager mWifiManager;
    protected WifiInfo mWifiInfo;
    private WifiScanReceiver mWifiScanReceiver;

    private DatabaseReference mFirebaseDB;
    protected FirebaseDatabase mFirebaseInstance;

    protected Locale locale = null;

    private ArrayList<String> devicesList = null;

    private String getThemes = "ETColorBlueGrey", getLang = "en", oldUri = "";
    private int maxVolume = 0, volume = 7, indexLang = 0;
    private float getPitch = 0.0f, getSpeechrate = 0.0f, maxSpeechrate = 0.0f, maxPitch = 0.0f;
    private boolean wifiDefaultState = false;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");
        indexLang = sharedPreferences.getInt("ETIndexLang", 0);

        setLanguage();

        notificationUtils = new ETNotificationUtils(ETSettingsActivity.this);
        parseData = new ETJSONParseData(ETSettingsActivity.this);
        crypto = new ETAESCrypto();
        hexConvert = new ETHexConvert();
        mInterstitialAd = new InterstitialAd(ETSettingsActivity.this);

        /*Init Firebase*/
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDB = mFirebaseInstance.getReference(ETConfig.DB_ENTOUCH);

        /*Init Wifi*/
        mWifiManager = (WifiManager) ETSettingsActivity.this.getBaseContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        mWifiInfo = mWifiManager.getConnectionInfo();
        mWifiScanReceiver = new WifiScanReceiver();

        /*Setting Layout*/
        toolbar = findViewById(R.id.toolbar_settings_act);
        settingLayout = findViewById(R.id.settingLayout);

        /*Find Devices*/
        searchDeviceTitleCardView = findViewById(R.id.searchDeviceTitleCardView);
        helpFind = findViewById(R.id.helpFind);
        searchDeviceTitle = findViewById(R.id.searchDeviceTitle);
        searchDeviceCardView = findViewById(R.id.searchDeviceCardView);
        groupSearchDeviceCardView = findViewById(R.id.groupSearchDeviceCardView);
        deviceFound = findViewById(R.id.deviceFound);
        deviceSelected = findViewById(R.id.deviceSelected);
        spinnerSearch = findViewById(R.id.spinnerSearch);
        searchDeviceBtnCardView = findViewById(R.id.searchDeviceBtnCardView);
        searchDeviceBtn = findViewById(R.id.searchDeviceBtn);

        /*Language*/
        languageTitleCardView = findViewById(R.id.languageTitleCardView);
        helpLanguage = findViewById(R.id.helpLanguage);
        languageTitle = findViewById(R.id.languageTitle);
        selectLanguage = findViewById(R.id.selectLanguage);
        languageCardView = findViewById(R.id.languageCardView);
        groupLangCardView = findViewById(R.id.groupLangCardView);
        spinnerLanguage = findViewById(R.id.spinnerLanguage);

        /*Sound Notification*/
        notifTitleCardView = findViewById(R.id.notifTitleCardView);
        helpNotif = findViewById(R.id.helpNotif);
        notifTitle = findViewById(R.id.notifTitle);
        notifCardView = findViewById(R.id.notifCardView);
        pathNotifInfo = findViewById(R.id.pathNotifInfo);
        notifSelectUri = findViewById(R.id.notifSelectUri);
        groupPathNotifCardView = findViewById(R.id.groupPathNotifCardView);
        pathNotifInfoCardView = findViewById(R.id.pathNotifInfoCardView);
        setNotifBtnCardView = findViewById(R.id.setNotifBtnCardView);
        setNotifBtn = findViewById(R.id.setNotifBtn);
        notifBrowseBtnCardView = findViewById(R.id.notifBrowseBtnCardView);
        notifBrowseBtn = findViewById(R.id.notifBrowseBtn);
        groupVolumeNotifCardView = findViewById(R.id.groupVolumeNotifCardView);
        volumeNotifProgress = findViewById(R.id.volumeNotifProgress);
        volumeNotifCardView = findViewById(R.id.volumeNotifCardView);
        masterVolumeNotif = findViewById(R.id.masterVolumeNotif);
        playNotifBtnCardView = findViewById(R.id.playNotifBtnCardView);
        playNotifBtn = findViewById(R.id.playNotifBtn);

        /*Alert Notification*/
        alertTitleCardView = findViewById(R.id.alertTitleCardView);
        helpAlert = findViewById(R.id.helpAlert);
        alertTitle = findViewById(R.id.alertTitle);
        alertNotif = findViewById(R.id.alertNotif);
        alertInfoCardView = findViewById(R.id.alertInfoCardView);
        alertInfoText = findViewById(R.id.alertInfoText);
        alertCardView = findViewById(R.id.alertCardView);
        alertToggleCardView = findViewById(R.id.alertToggleCardView);
        alertToggleBtn = findViewById(R.id.alertToggleBtn);

        /*Text to Speak (TTS)*/
        ttsTitleCardView = findViewById(R.id.ttsTitleCardView);
        helpTTS = findViewById(R.id.helpTTS);
        ttsTitle = findViewById(R.id.ttsTitle);
        ttsCardView = findViewById(R.id.ttsCardView);
        groupTTSCardView = findViewById(R.id.groupTTSCardView);
        speechRateProgress = findViewById(R.id.speechRateProgress);
        speechRateCardView = findViewById(R.id.speechRateCardView);
        ttsSpeechRate = findViewById(R.id.ttsSpeechRate);
        pitchCardView = findViewById(R.id.pitchCardView);
        pitchProgress = findViewById(R.id.pitchProgress);
        ttsPitch = findViewById(R.id.ttsPitch);
        ttsBtnCardView = findViewById(R.id.ttsBtnCardView);
        ttsBtn = findViewById(R.id.ttsBtn);

        /*Log*/
        logTitleCardView = findViewById(R.id.logTitleCardView);
        helpLog = findViewById(R.id.helpLog);
        logInfoTitle = findViewById(R.id.logInfoTitle);
        logCardView = findViewById(R.id.logCardView);
        delLogMsgCardView = findViewById(R.id.delLogMsgCardView);
        delLogActCardView = findViewById(R.id.delLogActCardView);
        delLogMsgText = findViewById(R.id.delLogMsgText);
        delLogActText = findViewById(R.id.delLogActText);
        delLogMsgBtnCardView = findViewById(R.id.delLogMsgBtnCardView);
        delLogActBtnCardView = findViewById(R.id.delLogActBtnCardView);
        delLogMsgBtn = findViewById(R.id.delLogMsgBtn);
        delLogActBtn = findViewById(R.id.delLogActBtn);

        /*Remote Devices*/
        rdTitleCardView = findViewById(R.id.rdTitleCardView);
        helpRD = findViewById(R.id.helpRD);
        rdTitle = findViewById(R.id.rdTitle);
        rdCardView = findViewById(R.id.rdCardView);
        rdInfoCardView = findViewById(R.id.rdInfoCardView);
        rdInfoText = findViewById(R.id.rdInfoText);
        rdBtnCardView = findViewById(R.id.rdBtnCardView);
        rdBtn = findViewById(R.id.rdBtn);

        /*Init Audio & get system volume*/
        audioManager = (AudioManager) ETSettingsActivity.this.getSystemService(Context.AUDIO_SERVICE);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        /*Init set max Volume & TTS for seek bar*/
        masterVolumeNotif.setMax(150);
        ttsSpeechRate.setMax(35);
        ttsPitch.setMax(30);

        setSupportActionBar(toolbar);

        askPermissionScanWifi();
        initTTS();
        setThemes();

        refreshUI(true);
        //loadInterstitial();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ETConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ETConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(ETConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");
                    String location = intent.getStringExtra("location");

                    //Toast.makeText(ETSettingsActivity.this,  "ETMainActivity LatLng :" + location , Toast.LENGTH_LONG).show();

                    Intent notify = new Intent(ETSettingsActivity.this, ETNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    notify.putExtra("location", location);
                    startActivity(notify);
                }
            }
        };

        helpFind.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            Intent helpOpt = new Intent(ETSettingsActivity.this, ETHelpActivity.class);
            helpOpt.putExtra("linkURL", "help_find");
            startActivity(helpOpt);
        });

        searchDeviceTitle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.vibrate));
        });

        spinnerSearch.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                try {
                    askPermissionWriteData(item.toString());
                    String deviceModel = askPermissionReadData();

                    if (!deviceModel.isEmpty() || !deviceModel.equals("")) {
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("ETDeviceModel", deviceModel);
                        edit.apply();

                        String disDeviceModel = getResources().getString(R.string.deviceSelected) + " " + sharedPreferences.getString("ETDeviceModel", getResources().getString(R.string.device_model));
                        deviceSelected.setText(disDeviceModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        searchDeviceBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            askPermissionScanWifi();

            ArrayAdapter<String> deviceAdapter = new ArrayAdapter<String>(ETSettingsActivity.this, android.R.layout.simple_spinner_item, devicesList);
            deviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSearch.setAdapter(deviceAdapter);

            String data = getResources().getString(R.string.deviceFound) + " " + (devicesList.size() > 0 ? -1 : 0);
            deviceFound.setText(data);
        });

        helpLanguage.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            Intent helpOpt = new Intent(ETSettingsActivity.this, ETHelpActivity.class);
            helpOpt.putExtra("linkURL", "help_lang");
            startActivity(helpOpt);
        });

        languageTitle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.vibrate));
        });

        spinnerLanguage.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, getResources().getString(R.string.languageChoose) + item + " (" + position + ")", 5000).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(ETSettingsActivity.this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(getResources().getString(R.string.notification_language_title));
                builder.setMessage(getResources().getString(R.string.notification_language));
                builder.setPositiveButton(getResources().getString(R.string.notification_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences.Editor setLanguage = sharedPreferences.edit();

                        switch (position) {
                            case 0:
                                setLanguage.putString("ETLanguage", "en");
                                setLanguage.putInt("ETIndexLang", position);
                                setLanguage.apply();
                                break;
                            case 1:
                                setLanguage.putString("ETLanguage", "in");
                                setLanguage.putInt("ETIndexLang", position);
                                setLanguage.apply();
                                break;
                            case 2:
                                setLanguage.putString("ETLanguage", "ja");
                                setLanguage.putInt("ETIndexLang", position);
                                setLanguage.apply();
                                break;

                            default:
                                break;
                        }
                        onResume();
                    }
                });
                builder.show();
                parseData.writeLogActivity("logact.log", "Language changed");
                parseData.writeLogActivity("logact-in.log", "Bahasa telah diubah");
                parseData.writeLogActivity("logact-ja.log", "言語が変更された");
            }
        });

        helpNotif.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            Intent helpOpt = new Intent(ETSettingsActivity.this, ETHelpActivity.class);
            helpOpt.putExtra("linkURL", "help_notif");
            startActivity(helpOpt);
        });

        notifTitle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.vibrate));
        });

        setNotifBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            AlertDialog.Builder builder = new AlertDialog.Builder(ETSettingsActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(getResources().getString(R.string.notification_soundselect_title));
            builder.setMessage(getResources().getString(R.string.notification_soundselect));
            builder.setIcon(R.drawable.ic_info_outline_black_48dp);

            builder.setPositiveButton(getResources().getString(R.string.notification_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    String setUri = sharedPreferences.getString("setUri", "");

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETNotifUri", setUri);
                    edit.apply();

                    Ringtone ringtone = RingtoneManager.getRingtone(ETSettingsActivity.this, Uri.parse(setUri));
                    notifSelectUri.setText(ringtone.getTitle(ETSettingsActivity.this));

                    if (notificationUtils.isPlayNotificationSound()) {
                        notificationUtils.stopNotificationSound();
                        notificationUtils.playNotificationSound();
                        playNotifBtn.setText(getResources().getString(R.string.stopButton));
                    } else {
                        notificationUtils.playNotificationSound();
                        playNotifBtn.setText(getResources().getString(R.string.stopButton));
                    }

                    setNotifBtn.setVisibility(View.GONE);
                    parseData.writeLogActivity("logact.log", "Sound notification has been changed");
                    parseData.writeLogActivity("logact-in.log", "Pemberitahuan suara telah diubah");
                    parseData.writeLogActivity("logact-ja.log", "サウンド通知が変更されました");
                }
            });

            builder.setNegativeButton(getResources().getString(R.string.notification_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETNotifUri", oldUri);
                    edit.putString("setUri", oldUri);
                    edit.apply();

                    Ringtone ringtone = RingtoneManager.getRingtone(ETSettingsActivity.this, Uri.parse(oldUri));
                    notifSelectUri.setText(ringtone.getTitle(ETSettingsActivity.this));

                    setNotifBtn.setVisibility(View.GONE);
                }
            });
            builder.show();
        });

        notifBrowseBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            oldUri = sharedPreferences.getString("ETNotifUri", "");
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("audio/mpeg");
                startActivityForResult(
                        Intent.createChooser(intent, getResources().getString(R.string.soundChoose)),
                        READ_EXTERNAL_STORAGE_REQUEST_CODE
                );
            } else {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("audio/mpeg");
                startActivityForResult(
                        Intent.createChooser(intent, getResources().getString(R.string.soundChoose)),
                        READ_EXTERNAL_STORAGE_REQUEST_CODE
                );
            }
        });

        masterVolumeNotif.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                volume = progress / 10;
                String txtVolume = getResources().getString(R.string.volumeTitle) + "<b>" + volume + "</b>:<b>" + maxVolume + "</b>";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    volumeNotifProgress.setText(Html.fromHtml(txtVolume, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    volumeNotifProgress.setText(Html.fromHtml(txtVolume));
                }

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putInt("ETVolume", progress);
                edit.apply();

                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!notificationUtils.isPlayNotificationSound()) {
                    notificationUtils.playNotificationSound();
                    playNotifBtn.setText(getResources().getString(R.string.stopButton));
                }
            }
        });

        playNotifBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            if (notificationUtils.isPlayNotificationSound()) {
                notificationUtils.stopNotificationSound();
                playNotifBtn.setText(getResources().getString(R.string.playButton));
            } else {
                notificationUtils.playNotificationSound();
                playNotifBtn.setText(getResources().getString(R.string.stopButton));
            }
        });

        helpAlert.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            Intent helpOpt = new Intent(ETSettingsActivity.this, ETHelpActivity.class);
            helpOpt.putExtra("linkURL", "help_alert");
            startActivity(helpOpt);
        });

        alertTitle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.vibrate));
        });

        alertNotif.setOnClickListener(view -> {
            if (alertNotif.isSelected()) {
                view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.shake));
            }
        });

        alertToggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                alertToggleBtn.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

                if (isChecked) {
                    alertToggleBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertToggleBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
                    } else {
                        alertToggleBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
                    }
                    alertNotif.setSelected(true);

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETCheckSound", "ON");
                    edit.apply();
                    parseData.writeLogActivity("logact.log", "Sound notification is turned on");
                    parseData.writeLogActivity("logact-in.log", "Notifikasi suara dihidupkan");
                    parseData.writeLogActivity("logact-ja.log", "サウンド通知がオンになっている");
                } else {
                    alertToggleBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertToggleBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
                    } else {
                        alertToggleBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
                    }
                    alertNotif.setSelected(false);

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ETCheckSound", "OFF");
                    edit.apply();
                    parseData.writeLogActivity("logact.log", "Sound notification is turned off");
                    parseData.writeLogActivity("logact-in.log", "Notifikasi suara dimatikan");
                    parseData.writeLogActivity("logact-ja.log", "音声通知がオフになっている");
                }
            }
        });

        helpTTS.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            Intent helpOpt = new Intent(ETSettingsActivity.this, ETHelpActivity.class);
            helpOpt.putExtra("linkURL", "help_tts");
            startActivity(helpOpt);
        });

        ttsTitle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.vibrate));
        });

        ttsSpeechRate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxSpeechrate = ((float) ttsSpeechRate.getMax()) / 10;
                float speechrate = ((float) progress) / 10;

                String txtSpeech = getResources().getString(R.string.speechrateTitle) + "<b>" + speechrate + "</b>:<b>" + maxSpeechrate + "</b>";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    speechRateProgress.setText(Html.fromHtml(txtSpeech, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    speechRateProgress.setText(Html.fromHtml(txtSpeech));
                }

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat("ETSpeechrate", speechrate);
                edit.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speakTTS(getResources().getString(R.string.speakConf));
            }
        });

        ttsPitch.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxPitch = ((float) ttsPitch.getMax()) / 10;
                float pitch = ((float) progress) / 10;

                String txtPitch = getResources().getString(R.string.pitchTitle) + "<b>" + pitch + "</b>:<b>" + maxPitch + "</b>";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    pitchProgress.setText(Html.fromHtml(txtPitch, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    pitchProgress.setText(Html.fromHtml(txtPitch));
                }

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat("ETPitch", pitch);
                edit.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speakTTS(getResources().getString(R.string.speakConf));
            }
        });

        ttsBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            maxSpeechrate = ((float) ttsSpeechRate.getMax()) / 10;
            maxPitch = ((float) ttsPitch.getMax()) / 10;

            float speechrate = 1.0f;
            float pitch = 1.0f;

            String txtSpeech = getResources().getString(R.string.speechrateTitle) + "<b>" + speechrate + "</b>:<b>" + maxSpeechrate + "</b>";
            String txtPitch = getResources().getString(R.string.pitchTitle) + "<b>" + pitch + "</b>:<b>" + maxPitch + "</b>";

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putFloat("ETSpeechrate", speechrate);
            edit.putFloat("ETPitch", pitch);
            edit.apply();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                speechRateProgress.setText(Html.fromHtml(txtSpeech, Html.FROM_HTML_MODE_LEGACY));
                pitchProgress.setText(Html.fromHtml(txtPitch, Html.FROM_HTML_MODE_LEGACY));
            } else {
                speechRateProgress.setText(Html.fromHtml(txtSpeech));
                pitchProgress.setText(Html.fromHtml(txtPitch));
            }

            speechrate = speechrate * 10;
            ttsSpeechRate.setProgress(Math.round(speechrate));
            ttsSpeechRate.refreshDrawableState();

            pitch = pitch * 10;
            ttsPitch.setProgress(Math.round(pitch));
            ttsPitch.refreshDrawableState();

            speakTTS(getResources().getString(R.string.speakConf));
        });

        helpLog.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            Intent helpOpt = new Intent(ETSettingsActivity.this, ETHelpActivity.class);
            helpOpt.putExtra("linkURL", "help_log");
            startActivity(helpOpt);
        });

        logInfoTitle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.vibrate));
        });

        delLogMsgBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            AlertDialog.Builder builder = new AlertDialog.Builder(ETSettingsActivity.this, R.style.AppCompatAlertDialogStyle);
            //AlertDialog.Builder builder = new AlertDialog.Builder(lContext, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(getResources().getString(R.string.notification_log_title));
            builder.setMessage(getResources().getString(R.string.notification_log));
            builder.setIcon(R.drawable.ic_info_outline_black_48dp);

            builder.setPositiveButton(getResources().getString(R.string.notification_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Snackbar.make(view, getResources().getString(R.string.logNotif), 5000).show();
                    parseData.writeToInternalFile("logmsg.log", "");
                    parseData.writeToInternalFile("logmsg-in.log", "");
                    parseData.writeToInternalFile("logmsg-ja.log", "");
                }
            });

            builder.setNegativeButton(getResources().getString(R.string.notification_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.show();
        });

        delLogActBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            AlertDialog.Builder builder = new AlertDialog.Builder(ETSettingsActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(getResources().getString(R.string.notification_log_title));
            builder.setMessage(getResources().getString(R.string.notification_log));
            builder.setIcon(R.drawable.ic_info_outline_black_48dp);

            builder.setPositiveButton(getResources().getString(R.string.notification_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Snackbar.make(view, getResources().getString(R.string.logNotif), 5000).show();
                    parseData.writeToInternalFile("logact.log", "");
                    parseData.writeToInternalFile("logact-in.log", "");
                    parseData.writeToInternalFile("logact-ja.log", "");
                }
            });

            builder.setNegativeButton(getResources().getString(R.string.notification_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.show();
        });

        helpRD.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            Intent helpOpt = new Intent(ETSettingsActivity.this, ETHelpActivity.class);
            helpOpt.putExtra("linkURL", "help_rd");
            startActivity(helpOpt);
        });

        rdTitle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.vibrate));
        });

        rdBtn.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.click));

            AlertDialog.Builder builder = new AlertDialog.Builder(ETSettingsActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(getResources().getString(R.string.notification_rd_title));
            builder.setMessage(getResources().getString(R.string.notification_rd));
            builder.setIcon(R.drawable.ic_info_outline_black_48dp);

            builder.setPositiveButton(getResources().getString(R.string.notification_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    reboot();
                    Snackbar.make(view, getResources().getString(R.string.rebootNotif), 5000).show();
                    parseData.writeLogActivity("logact.log", "Device is rebooted");
                    parseData.writeLogActivity("logact-in.log", "Perangkat dihidupkan kembali");
                    parseData.writeLogActivity("logact-ja.log", "デバイスが再起動されました");
                }
            });

            builder.setNegativeButton(getResources().getString(R.string.notification_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.show();
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ETNotificationUtils.clearNotifications(this);

        setLanguage();
        refreshUI(false);
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        unregisterReceiver(mWifiScanReceiver);
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        if (notificationUtils.isPlayNotificationSound()) {
            notificationUtils.stopNotificationSound();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        if (notificationUtils.isPlayNotificationSound()) {
            notificationUtils.stopNotificationSound();
        }
        closeTTS();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();

        notificationUtils.stopNotificationSound();

        if (mWifiManager.isWifiEnabled() && wifiDefaultState) {
            mWifiManager.setWifiEnabled(false);
            wifiDefaultState = false;
        }
        closeTTS();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e(TAG, "onOptionsItemSelected");

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            int getVol = sharedPreferences.getInt("ETVolume", 70);
            if (getVol > 0) {
                getVol = getVol - 10;
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putInt("ETVolume", getVol);
                edit.apply();
            }
            setVolume();

            return true;
        } else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
            int getVol = sharedPreferences.getInt("ETVolume", 70);
            if (getVol < 150) {
                getVol = getVol + 10;
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putInt("ETVolume", getVol);
                edit.apply();
            }
            setVolume();

            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        Log.e(TAG, "onActivityResult");
        if (requestCode == READ_EXTERNAL_STORAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (resultData != null) {
                Uri uri = resultData.getData();

                if (uri != null) {
                    setNotifBtn.setVisibility(View.VISIBLE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("setUri", uri.toString());
                    edit.apply();
                }
            }
        } else {
            setNotifBtn.setVisibility(View.GONE);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("setUri", oldUri);
            edit.apply();
        }
    }

    public void refreshUI(boolean anim) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        setToolbarTitle(getResources().getString(R.string.activity_title_settings));

        /*Find Device*/
        searchDeviceTitle.setText(getResources().getString(R.string.searchDeviceTitle));
        deviceFound.setText(getResources().getString(R.string.deviceFound));
        String disDeviceModel = getResources().getString(R.string.deviceSelected) + " " + sharedPreferences.getString("ETDeviceModel", getResources().getString(R.string.device_model));
        deviceSelected.setText(disDeviceModel);
        searchDeviceBtn.setText(getResources().getString(R.string.searchDeviceButton));

        /*Language*/
        languageTitle.setText(getResources().getString(R.string.languageTitle));
        selectLanguage.setText(getResources().getString(R.string.languageChoose));
        ArrayList<String> languageList = new ArrayList<String>();
        languageList.add(0, getResources().getString(R.string.languageEnglish));
        languageList.add(1, getResources().getString(R.string.languageIndonesian));
        languageList.add(2, getResources().getString(R.string.languageJapanese));
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(ETSettingsActivity.this, android.R.layout.simple_spinner_item, languageList);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLanguage.setAdapter(languageAdapter);
        indexLang = sharedPreferences.getInt("ETIndexLang", 0);
        spinnerLanguage.setSelectedIndex(indexLang);

        /*Sound Notification*/
        notifTitle.setText(getResources().getString(R.string.soundselectTitle));
        pathNotifInfo.setText(getResources().getString(R.string.soundpathInfo));

        oldUri = sharedPreferences.getString("ETNotifUri", "");
        if (oldUri.equals("")) {
            Uri notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + ETSettingsActivity.this.getPackageName() + "/raw/notification");
            SharedPreferences.Editor editoldUri = sharedPreferences.edit();
            editoldUri.putString("ETNotifUri", notification.toString());
            editoldUri.apply();

            oldUri = sharedPreferences.getString("ETNotifUri", "");
        }

        String setUri = sharedPreferences.getString("setUri", "");
        if (!setUri.equals("") && !setUri.equals(oldUri)) {
            Ringtone ringtone = RingtoneManager.getRingtone(ETSettingsActivity.this, Uri.parse(setUri));
            notifSelectUri.setText(ringtone.getTitle(ETSettingsActivity.this));
            setNotifBtn.setVisibility(View.VISIBLE);
        } else {
            Ringtone ringtone = RingtoneManager.getRingtone(ETSettingsActivity.this, Uri.parse(oldUri));
            notifSelectUri.setText(ringtone.getTitle(ETSettingsActivity.this));
            setNotifBtn.setVisibility(View.GONE);
        }
        setNotifBtn.setText(getResources().getString(R.string.soundsetButton));
        notifBrowseBtn.setText(getResources().getString(R.string.soundselectButton));

        setVolume();

        if (notificationUtils.isPlayNotificationSound()) {
            playNotifBtn.setText(getResources().getString(R.string.stopButton));
        } else {
            playNotifBtn.setText(getResources().getString(R.string.playButton));
        }

        /*Alert Notification*/
        alertTitle.setText(getResources().getString(R.string.alertTitle));
        alertInfoText.setText(getResources().getString(R.string.alertInfo));
        alertToggleBtn.setTextOn(getResources().getString(R.string.deviceOn));
        alertToggleBtn.setTextOff(getResources().getString(R.string.deviceOff));

        if (sharedPreferences.getString("ETCheckSound", "ON").equals("ON")) {
            alertToggleBtn.setChecked(true);
            alertNotif.setSelected(true);
            alertToggleBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertToggleBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                alertToggleBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            }

            SharedPreferences.Editor editalertToggleBtn = sharedPreferences.edit();
            editalertToggleBtn.putString("ETCheckSound", "ON");
            editalertToggleBtn.apply();
        } else {
            alertToggleBtn.setChecked(false);
            alertNotif.setSelected(false);
            alertToggleBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertToggleBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            } else {
                alertToggleBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
            }

            SharedPreferences.Editor editalertToggleBtn = sharedPreferences.edit();
            editalertToggleBtn.putString("ETCheckSound", "OFF");
            editalertToggleBtn.apply();
        }

        /*Text to Speak (TTS)*/
        ttsTitle.setText(getResources().getString(R.string.ttsTitle));
        setSpeechrate();
        setPitchrate();
        ttsBtn.setText(getResources().getString(R.string.ttsButton));

        /*Log*/
        logInfoTitle.setText(getResources().getString(R.string.logInfoTitle));
        delLogMsgText.setText(getResources().getString(R.string.delLogMsgTitle));
        delLogMsgBtn.setText(getResources().getString(R.string.delLogButton));
        delLogActText.setText(getResources().getString(R.string.delLogActTitle));
        delLogActBtn.setText(getResources().getString(R.string.delLogButton));

        /*Remote Device*/
        rdTitle.setText(getResources().getString(R.string.rdTitle));
        rdInfoText.setText(getResources().getString(R.string.rdInfo));
        rdBtn.setText(getResources().getString(R.string.rdButton));

        /*Animation*/
        if (anim) {
            searchDeviceCardView.setVisibility(View.VISIBLE);
            searchDeviceTitleCardView.setVisibility(View.VISIBLE);
            searchDeviceTitleCardView.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.show_up_first));
            languageCardView.setVisibility(View.VISIBLE);
            languageTitleCardView.setVisibility(View.VISIBLE);
            languageTitleCardView.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.show_up_second));
            notifCardView.setVisibility(View.VISIBLE);
            notifTitleCardView.setVisibility(View.VISIBLE);
            notifTitleCardView.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.show_up_third));
            alertCardView.setVisibility(View.VISIBLE);
            alertTitleCardView.setVisibility(View.VISIBLE);
            alertTitleCardView.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.show_up_fourth));
            ttsCardView.setVisibility(View.VISIBLE);
            ttsTitleCardView.setVisibility(View.VISIBLE);
            ttsTitleCardView.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.show_up_fifth));
            logCardView.setVisibility(View.VISIBLE);
            logTitleCardView.setVisibility(View.VISIBLE);
            logTitleCardView.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.show_up_sixth));
            if (sharedPreferences.getString("devicestatus", "").equals("approved")) {
                rdCardView.setVisibility(View.VISIBLE);
                rdTitleCardView.setVisibility(View.VISIBLE);
                rdTitleCardView.startAnimation(AnimationUtils.loadAnimation(ETSettingsActivity.this, R.anim.show_up_seventh));
            } else {
                rdTitleCardView.setVisibility(View.GONE);
            }
        }
    }

    private void setThemes() {
        settingLayout.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupSearchDeviceCardView.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        searchDeviceBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupLangCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupPathNotifCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        setNotifBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        notifBrowseBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupVolumeNotifCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        playNotifBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        alertToggleCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        groupTTSCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        ttsBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        rdBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        delLogMsgBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        delLogActBtnCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        searchDeviceCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        notifCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        alertCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        ttsCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        languageCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        rdCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));
        logCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor300(getThemes)));

        pathNotifInfoCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        volumeNotifCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        alertInfoCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        speechRateCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        pitchCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        rdInfoCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        delLogMsgCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));
        delLogActCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor400(getThemes)));

        deviceFound.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        deviceSelected.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        selectLanguage.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        pathNotifInfo.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));

        volumeNotifProgress.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        speechRateProgress.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));
        pitchProgress.setTextColor(getResources().getColor(ETSetColorTheme.getColor900(getThemes)));

        notifSelectUri.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        delLogMsgText.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        delLogActText.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        rdInfoText.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        searchDeviceBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        notifBrowseBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        setNotifBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        playNotifBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        ttsBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        rdBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        delLogMsgBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        delLogActBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));

        spinnerSearch.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        spinnerSearch.setLinkTextColor(Color.BLUE);
        spinnerSearch.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));

        spinnerLanguage.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        spinnerLanguage.setLinkTextColor(Color.BLUE);
        spinnerLanguage.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            searchDeviceBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            notifBrowseBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            setNotifBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            playNotifBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            ttsBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            rdBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            delLogMsgBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
            delLogActBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor500(getThemes))));
        } else {
            searchDeviceBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            notifBrowseBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            setNotifBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            playNotifBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            ttsBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            rdBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            delLogMsgBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
            delLogActBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
        }
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public void setVolume() {
        int getVol = sharedPreferences.getInt("ETVolume", 70);
        masterVolumeNotif.setProgress(getVol);
        masterVolumeNotif.refreshDrawableState();
        volume = masterVolumeNotif.getProgress() / 10;

        String getVolume = getResources().getString(R.string.volumeTitle) + "<b>" + volume + "</b>:<b>" + maxVolume + "</b>";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            volumeNotifProgress.setText(Html.fromHtml(getVolume, Html.FROM_HTML_MODE_LEGACY));
        } else {
            volumeNotifProgress.setText(Html.fromHtml(getVolume));
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("ETVolume", masterVolumeNotif.getProgress());
        edit.apply();
    }

    public void setSpeechrate() {
        getSpeechrate = sharedPreferences.getFloat("ETSpeechrate", 1.0f);
        float speechrate = getSpeechrate * 10;
        ttsSpeechRate.setProgress(Math.round(speechrate));
        ttsSpeechRate.refreshDrawableState();
        maxSpeechrate = ((float) ttsSpeechRate.getMax()) / 10;
        String txtSpeech = getResources().getString(R.string.speechrateTitle) + "<b>" + getSpeechrate + "</b>:<b>" + maxSpeechrate + "</b>";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            speechRateProgress.setText(Html.fromHtml(txtSpeech, Html.FROM_HTML_MODE_LEGACY));
        } else {
            speechRateProgress.setText(Html.fromHtml(txtSpeech));
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putFloat("ETSpeechrate", getSpeechrate);
        edit.apply();

    }

    public void setPitchrate() {
        getPitch = sharedPreferences.getFloat("ETPitch", 1.0f);
        float pitch = getPitch * 10;
        ttsPitch.setProgress(Math.round(pitch));
        ttsPitch.refreshDrawableState();
        maxPitch = ((float) ttsPitch.getMax()) / 10;
        String txtPitch = getResources().getString(R.string.pitchTitle) + "<b>" + getPitch + "</b>:<b>" + maxPitch + "</b>";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            pitchProgress.setText(Html.fromHtml(txtPitch, Html.FROM_HTML_MODE_LEGACY));
        } else {
            pitchProgress.setText(Html.fromHtml(txtPitch));
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putFloat("ETPitch", getPitch);
        edit.apply();
    }

    private void setToolbarTitle(String titleName) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        String getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");

        if (actionBar != null) {
            actionBar.setTitle(titleName);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(ResourcesCompat.getColor(getResources(), ETSetColorTheme.getColorPrimaryDark(getThemes), null)));
        }
    }

    private void loadInterstitial() {
        final AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("15C0119EA7504242")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void showInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.i("showInterstitial", "Ad did not load");
            //Toast.makeText(ETSettingsActivity.this, "Ad did not load", Toast.LENGTH_SHORT).show();
        }
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
                Log.i("onAdLoaded", "Ad loaded.");
                //Toast.makeText(ETSettingsActivity.this, "Ad loaded.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.i("onAdFailedToLoad", "Ad failed to load with error code : " + errorCode);
                //Toast.makeText(ETSettingsActivity.this, "Ad failed to load with error code : " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.i("onAdOpened", "Ad opened.");
                //Toast.makeText(ETSettingsActivity.this, "Ad opened.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                showInterstitial();
                Log.i("onAdClosed", "Ad closed.");
                //Toast.makeText(ETSettingsActivity.this, "Ad closed.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.i("onAdLeftApplication", "Ad left application.");
                //Toast.makeText(ETSettingsActivity.this, "Ad left application.", Toast.LENGTH_SHORT).show();
            }
        });
        return interstitialAd;
    }

    @SuppressLint("Deprecation")
    private void speakTTS(String stts) {
        getSpeechrate = sharedPreferences.getFloat("ETSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("ETPitch", 1.0f);
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLang = new Locale(getLang);
        float pitch = (getPitch == 0.0f) ? 0.1f : getPitch;
        float speechrate = (getSpeechrate == 0.0f) ? 0.1f : getSpeechrate;

        tts.setLanguage(setLang);
        tts.setPitch(pitch);
        tts.setSpeechRate(speechrate);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    private void initTTS() {
        tts = new TextToSpeech(ETSettingsActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.ERROR || status == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), 5000).show();
                } else {
                    getSpeechrate = sharedPreferences.getFloat("ETSpeechrate", 1.0f);
                    getPitch = sharedPreferences.getFloat("ETPitch", 1.0f);

                    float pitch = (getPitch == 0.0f) ? 0.1f : getPitch;
                    float speechrate = (getSpeechrate == 0.0f) ? 0.1f : getSpeechrate;

                    setLang = new Locale(getLang);

                    tts.setLanguage(setLang);
                    tts.setPitch(pitch);
                    tts.setSpeechRate(speechrate);
                }
            }
        });
    }

    private void closeTTS() {
        if (tts != null) {
            if (tts.isSpeaking()) {
                tts.stop();
            }

            tts.shutdown();
        }
    }

    public void reboot() {
        String deviceModel = sharedPreferences.getString("ETDeviceModel", "enTouch-demo");
        mFirebaseDB.child(deviceModel).child(ETConfig.DB_ENTOUCHDATA).child("iotrst").setValue("ON");
    }

    private void askPermissionScanWifi() {
        boolean canScan = askPermission(ACCESS_COARSE_LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (canScan) {
            scanWifi();
        }
    }

    private void askPermissionWriteData(String data) {
        boolean canWrite = askPermission(WRITE_EXTERNAL_STORAGE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (canWrite) {
            writeData(data);
        }
    }

    private void writeData(String data) {
        String edata = "";

        try {
            edata = crypto.ETEncrypt(ETConfig.CONFIG_KEY, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        parseData.writeToExternalFile(ETConfig.CONFIG_DIRNAME, ETConfig.CONFIG_FILENAME, edata);
    }

    private String askPermissionReadData() {
        boolean canRead = askPermission(READ_EXTERNAL_STORAGE_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        String data = "";

        if (canRead) {
            data = readData();
        }

        return data;
    }

    private String readData() {
        String data, edata = "";

        data = parseData.readFromExternalFile(ETConfig.CONFIG_DIRNAME, ETConfig.CONFIG_FILENAME);

        try {
            edata = crypto.ETDecrypt(ETConfig.CONFIG_KEY, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return edata;
    }

    private boolean askPermission(int requestId, String permissionName) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            // Check if we have permission
            int permission = ActivityCompat.checkSelfPermission(ETSettingsActivity.this, permissionName);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{permissionName},
                        requestId
                );
                return false;
            }
        }
        return true;
    }

    private void scanWifi() {
        try {
            if (!mWifiManager.isWifiEnabled()) {
                mWifiManager.setWifiEnabled(true);
                wifiDefaultState = true;
            }

            mWifiManager.startScan();

            if (mWifiManager.startScan()) {
                List<ScanResult> scanResults = mWifiManager.getScanResults();
                devicesList = new ArrayList<String>();

                int counter = 0;
                boolean firstScan = true;

                for (ScanResult result : scanResults) {
                    if (checkDevice(result.SSID)) {
                        if (firstScan) {
                            devicesList.add(counter++, result.SSID);
                            firstScan = false;
                        }
                        devicesList.add(counter++, result.SSID);
                    }
                }
            }
        } catch (Exception e) {
            // critical error: set to no results and do not die
        }
    }

    /*
     * Pengkodeean device :
     *
     * HEX 12 34 56 78
     * yang dipakai 12 dan 56 dengan spesifikasi hex : 45, 4E, 54, 4F, 55, 43, 48*/
    public boolean checkDevice(String data) {
        String[] parse = data.split("-");
        String hex = parse[1];
        String hexOne = "", hexTwo = "";
        boolean statusHexOne = false, statusHexTwo = false;

        if (!parse[1].equals("")) {
            hexOne = hex.substring(0, 2);
            hexTwo = hex.substring(4, 6);
        }

        switch (hexOne) {
            case "45":
            case "4E":
            case "54":
            case "4F":
            case "55":
            case "43":
            case "48":
                statusHexOne = true;
                break;

            default:
                break;
        }

        switch (hexTwo) {
            case "42":
            case "4C":
            case "41":
            case "43":
            case "58":
            case "4F":
            case "44":
            case "45":
                statusHexTwo = true;
                break;

            default:
                break;
        }

        return statusHexOne && statusHexTwo;
    }

    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            askPermissionScanWifi();
        }
    }
}