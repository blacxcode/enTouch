package com.blacxcode.entouch.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Locale;

public class ETAuthorityActivity extends AppCompatActivity {
    private static final String TAG = ETAuthorityActivity.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private EditText authUserInput, authPassInput;
    private TextView authInfo, authTitle, authUsername, authPassword;
    private ImageButton authShowPass;
    private ImageView authorityIcon, helpAuth;
    private Button authLoginBtn;
    private CheckBox authRemember;
    private CardView authLoginCardView, authLoginBtnCardview;

    protected Locale locale = null;

    private String getThemes = "ETColorBlueGrey", getLang = "en";

    public void onAttachedToWindow() {
        Log.e(TAG, "onAttachedToWindow");
        super.onAttachedToWindow();

        Window window = getWindow();
        window.setFormat(PixelFormat.TRANSPARENT);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authority);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        authorityIcon = findViewById(R.id.authorityIcon);
        authTitle = findViewById(R.id.authTitle);
        authUsername = findViewById(R.id.authUsername);
        authUserInput = findViewById(R.id.authUserInput);
        authPassword = findViewById(R.id.authPassword);
        authPassInput = findViewById(R.id.authPassInput);
        authShowPass = findViewById(R.id.authShowPass);
        authLoginBtn = findViewById(R.id.authLoginBtn);
        authRemember = findViewById(R.id.authRemember);
        authInfo = findViewById(R.id.authInfo);
        authLoginCardView = findViewById(R.id.authLoginCardView);
        authLoginBtnCardview = findViewById(R.id.authLoginBtnCardview);
        helpAuth = findViewById(R.id.helpAuth);

        refreshUI(true);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ETConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(ETConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(ETConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");
                    String location = intent.getStringExtra("location");

                    //Toast.makeText(ETAuthorityActivity.this,  "ETMainActivity LatLng :" + location , Toast.LENGTH_LONG).show();

                    Intent notify = new Intent(ETAuthorityActivity.this, ETNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    notify.putExtra("location", location);
                    startActivity(notify);
                }
            }
        };

        authorityIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet animationSet = new AnimationSet(false);
                animationSet.addAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.sequential_about_show_up));
                animationSet.addAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.sequential_about_click));
                animationSet.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        loginCheck();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                v.startAnimation(animationSet);
            }
        });

        authShowPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authShowPass.setSelected(!authShowPass.isSelected());

                if (!authShowPass.isSelected()) {
                    authShowPass.setSelected(false);
                    authPassInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    authShowPass.setSelected(true);
                    authPassInput.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        authLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginCheck();
            }
        });

        helpAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.click));

                Intent helpOpt = new Intent(ETAuthorityActivity.this, ETHelpActivity.class);
                helpOpt.putExtra("linkURL", "help_login");
                startActivity(helpOpt);
            }
        });
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(ETAuthorityActivity.this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(ETAuthorityActivity.this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ETConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        ETNotificationUtils.clearNotifications(ETAuthorityActivity.this);

        setLanguage();
        refreshUI(false);
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        LocalBroadcastManager.getInstance(ETAuthorityActivity.this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    public void refreshUI(boolean anime) {
        if (getThemes.equals("ETColorPink") || getThemes.equals("ETColorRed")) {
            authInfo.setTextColor(Color.BLUE);
        } else {
            authInfo.setTextColor(Color.RED);
        }

        authTitle.setTextColor(Color.WHITE);
        authUsername.setTextColor(Color.WHITE);
        authPassword.setTextColor(Color.WHITE);
        authUserInput.setTextColor(Color.WHITE);
        authPassInput.setTextColor(Color.WHITE);

        authLoginCardView.setVisibility(View.VISIBLE);

        authLoginCardView.setCardBackgroundColor(getResources().getColor(ETSetColorTheme.getColor800(getThemes)));

        authLoginBtn.setTextColor(getResources().getColor(ETSetColorTheme.getColor100(getThemes)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            authLoginBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(ETSetColorTheme.getColor800(getThemes))));
        } else {
            authLoginBtn.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColor500(getThemes)));
        }

        authTitle.startAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.blinking));

        if (anime) {
            authLoginCardView.startAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.show_up));
        }
    }

    public boolean loginCheck() {
        String nUser = authUserInput.getText().toString();
        String nPass = authPassInput.getText().toString();
        String eUser = sharedPreferences.getString("ETAuthUser", "entouch");
        String ePass = sharedPreferences.getString("ETAuthPass", "entouch");

        if (eUser.equals("")) {
            eUser = ETConfig.AUTH_USERNAME;
        }

        if (ePass.equals("")) {
            ePass = ETConfig.AUTH_PASSWORD;
        }

        if (nUser.equals("") && nPass.equals("")) {
            authInfo.setText(getResources().getString(R.string.auth_infonouserpass));
            authInfo.startAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.vibrate));
            return false;
        } else if (nUser.equals(eUser) && nPass.equals(ePass)) {
            runAnimClose();
            if (authRemember.isChecked()) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putBoolean("ETAuthlogin", true);
                edit.apply();
            } else {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putBoolean("ETAuthlogin", false);
                edit.apply();
            }
            return true;
        } else if (!nUser.equals(eUser) && !nPass.equals(ePass)) {
            authInfo.setText(getResources().getString(R.string.auth_infouserpass));
            authInfo.startAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.vibrate));
            return false;
        } else if ((!nUser.equals(eUser) || nUser.equals("")) && nPass.equals(ePass)) {
            authInfo.setText(getResources().getString(R.string.auth_infouser));
            authInfo.startAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.vibrate));
            return false;
        } else if (nUser.equals(eUser) && (!nPass.equals(ePass) || nPass.equals(""))) {
            authInfo.setText(getResources().getString(R.string.auth_infopass));
            authInfo.startAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.vibrate));
            return false;
        } else {
            return false;
        }
    }

    public void runAnimClose() {
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(AnimationUtils.loadAnimation(ETAuthorityActivity.this, R.anim.close_up));
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                authLoginCardView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                authLoginCardView.setVisibility(View.GONE);

                Intent intent = new Intent(ETAuthorityActivity.this, ETDevicesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        authLoginCardView.setAnimation(animationSet);
        authLoginCardView.startAnimation(animationSet);
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
