package com.blacxcode.entouch.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blacxcode.entouch.R;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.service.ETFirebaseInstanceIDService;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.ui.ETSetColorTheme;
import com.blacxcode.entouch.util.ETAESCrypto;
import com.google.common.base.Optional;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * Created by blacXcode on 5/7/2017.
 */

public class ETSplashActivity extends AppCompatActivity {
    protected static final String TAG = ETFirebaseInstanceIDService.class.getSimpleName();

    private static final int WRITE_REQUEST_CODE = 41;
    private static final int READ_REQUEST_CODE = 42;
    private static List languageProfiles;
    private static LanguageDetector languageDetector;
    private static TextObjectFactory textObjectFactory;
    protected SharedPreferences sharedPreferences;
    protected ETJSONParseData parseData;
    private ETAESCrypto crypto;
    private LinearLayout linearLayout;
    private ImageView splash;
    private TextView AppName, AppVersion;
    private Thread splashTread;
    private Handler handlerLoading;
    protected Locale locale = null;

    private String getThemes = "ETColorBlueGrey", getLang = "en";
    private int progressStatus = 0;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.TRANSLUCENT);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        getThemes = sharedPreferences.getString("ETThemes", "ETColorBlueGrey");
        getLang = sharedPreferences.getString("ETLanguage", "en");

        setLanguage();

        parseData = new ETJSONParseData(ETSplashActivity.this);
        crypto = new ETAESCrypto();

        AppName = findViewById(R.id.AppName);
        AppVersion = findViewById(R.id.AppVersion);
        linearLayout = findViewById(R.id.splash_layout);
        splash = findViewById(R.id.splash);

        handlerLoading = new Handler();

        refreshUI(true);
    }

    @Override
    public void onStart() {
        Log.e(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();

        setLanguage();
        refreshUI(false);
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    void refreshUI(boolean anim) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));
            window.setNavigationBarColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));
        }

        linearLayout.setBackgroundColor(getResources().getColor(ETSetColorTheme.getColorPrimaryDark(getThemes)));

        String AppNameText = getResources().getString(R.string.app_name);
        String AppVersionText = getResources().getString(R.string.app_version) + getResources().getString(R.string.versionName);

        AppName.setText(AppNameText);
        AppVersion.setText(AppVersionText);

        if (anim) {
            new getData().execute();
        }
    }

    public String detectLang(String text) {
        TextObject textObject = textObjectFactory.forText(text);
        Optional<LdLocale> lang = languageDetector.detect(textObject);
        LdLocale locale = lang.orNull();
        return locale == null ? null : locale.getLanguage();
    }

    private void askPermissionWriteData(String data) {
        boolean canWrite = askPermission(WRITE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (canWrite) {
            writeData(data);
        }
    }

    private void writeData(String data) {
        String edata = "";

        try {
            edata = crypto.ETEncrypt(ETConfig.CONFIG_KEY, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        parseData.writeToExternalFile(ETConfig.CONFIG_DIRNAME, ETConfig.CONFIG_FILENAME, edata);
    }

    private String askPermissionReadData() {
        boolean canRead = askPermission(READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        String data = "";

        if (canRead) {
            data = readData();
        }

        return data;
    }

    private String readData() {
        String data = "", edata = "";

        data = parseData.readFromExternalFile(ETConfig.CONFIG_DIRNAME, ETConfig.CONFIG_FILENAME);

        try {
            edata = crypto.ETDecrypt(ETConfig.CONFIG_KEY, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return edata;
    }

    private boolean askPermission(int requestId, String permissionName) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            // Check if we have permission
            int permission = ActivityCompat.checkSelfPermission(ETSplashActivity.this, permissionName);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{permissionName},
                        requestId
                );
                return false;
            }
        }
        return true;
    }

    private void StartAnimations() {
        linearLayout.clearAnimation();
        linearLayout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.sequential_splash_alpha));

        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(AnimationUtils.loadAnimation(ETSplashActivity.this, R.anim.sequential_splash_show_up));
        animationSet.addAnimation(AnimationUtils.loadAnimation(ETSplashActivity.this, R.anim.sequential_splash_click));
        splash.startAnimation(animationSet);

        AppName.clearAnimation();
        AppName.startAnimation(AnimationUtils.loadAnimation(this, R.anim.sequential_splash_appname));

        AppVersion.clearAnimation();
        AppVersion.startAnimation(AnimationUtils.loadAnimation(this, R.anim.sequential_splash_appversion));

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;

                    while (waited < 6000) {
                        sleep(100);
                        waited += 100;
                    }

                    Intent intent = new Intent(ETSplashActivity.this, ETMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    ETSplashActivity.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    if (!splashTread.isInterrupted()) splashTread.interrupt();
                    ETSplashActivity.this.finish();
                }
            }
        };
        splashTread.start();
    }

    public void setLanguage() {
        getLang = sharedPreferences.getString("ETLanguage", "en");

        Locale locale = new Locale(getLang);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private class getData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linearLayout.setVisibility(View.INVISIBLE);
            // Initialize a new instance of progress dialog
            pd = new ProgressDialog(ETSplashActivity.this);
            // Set progress dialog style horizontal
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // Set the progress dialog background color transparent
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pd.setIndeterminate(false);
            pd.setCancelable(false);
            pd.setMessage(getResources().getString(R.string.progress_loading));
            // Finally, show the progress dialog
            pd.show();
            // Set the progress status zero on each button click
            progressStatus = 0;
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String deviceModel = askPermissionReadData();
            String deviceModeldefault = sharedPreferences.getString("ETDeviceModel", getResources().getString(R.string.device_model));

            if (!deviceModel.equals("") || deviceModel.isEmpty()) {
                askPermissionWriteData(deviceModel);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("ETDeviceModel", deviceModel);
                edit.apply();
            } else {
                askPermissionWriteData(deviceModeldefault);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("ETDeviceModel", deviceModeldefault);
                edit.apply();
            }

            //load all languages:
            try {
                languageProfiles = new LanguageProfileReader().readAllBuiltIn();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //build language detector:
            languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                    .withProfiles(languageProfiles)
                    .build();

            //create a text object factory
            textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText();

            // Start the lengthy operation in a background thread
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (progressStatus < 100) {
                        // Update the progress status
                        progressStatus += 1;

                        // Try to sleep the thread for 20 milliseconds
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // Update the progress bar
                        handlerLoading.post(new Runnable() {
                            @Override
                            public void run() {
                                // Update the progress status
                                pd.setProgress(progressStatus);
                                // If task execution completed
                                if (progressStatus == 100) {
                                    // Dismiss/hide the progress dialog
                                    pd.setMessage(getResources().getString(R.string.progress_done));
                                    pd.dismiss();
                                }
                            }
                        });
                    }
                }
            }).start(); // Start the operation

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            linearLayout.setVisibility(View.VISIBLE);
            StartAnimations();
        }
    }
}
