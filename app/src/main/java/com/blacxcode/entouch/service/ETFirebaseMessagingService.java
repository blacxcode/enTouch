package com.blacxcode.entouch.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.blacxcode.entouch.activity.ETMainActivity;
import com.blacxcode.entouch.config.ETConfig;
import com.blacxcode.entouch.sync.ETJSONParseData;
import com.blacxcode.entouch.util.ETNotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by blacXcode on 5/7/2017.
 */

public class ETFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = ETFirebaseMessagingService.class.getSimpleName();

    protected ETNotificationUtils notificationUtils;
    protected ETJSONParseData parseData;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTag());
        }

        // Check if message contains a data payload.public Context context;
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleNotification(json.getString("title"), json.getString("message"), json.getString("location"));
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String title, String message, String location) {
        SharedPreferences sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);

        if (!ETNotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent intent = new Intent(ETConfig.PUSH_NOTIFICATION);
            intent.putExtra("title", title);
            intent.putExtra("message", message);
            intent.putExtra("location", location);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

            if (sharedPreferences.getString("ETCheckSound", "").equals("ON")) {
                // play notification sound
                notificationUtils = new ETNotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            }
        } else {
            // If the app is in background, firebase itself handles the notification
            Log.e(TAG, "isBackground: ");
            Intent intent = new Intent(getApplicationContext(), ETMainActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("message", message);
            intent.putExtra("location", location);

            // check for image attachment
            showNotificationBG(getApplicationContext(), title, message, location, intent);
            parseData = new ETJSONParseData(getApplicationContext());

            parseData.writeLogActivity("logact.log", "Receive notifications from " + title);
            parseData.writeLogActivity("logact-in.log", "Menerima notifikasi dari " + title);
            parseData.writeLogActivity("logact-ja.log", title + "さん, からの通知を受け取る");

            parseData.writeLogActivity("logmsg.log",
                    "<br><b>from     \t\t\t: </b>" + title +
                            "<br><b>location : </b>" + "<a href=\"https://www.google.com/maps/search/?api=1&query=\" target=\"_blank\">" + location + "</a>" +
                            "<br><b>message  \t: </b><br>" + message + "<br><br>");
            parseData.writeLogActivity("logmsg-in.log",
                    "<br><b>dari     \t\t\t: </b>" + title +
                            "<br><b>lokasi : </b>" + "<a href=\"https://www.google.com/maps/search/?api=1&query=\" target=\"_blank\">" + location + "</a>" +
                            "<br><b>pesan  \t: </b><br>" + message + "<br><br>");
            parseData.writeLogActivity("logmsg-ja.log",
                    "<br><b>から     \t\t\t: </b>" + title +
                            "<br><b>ロケーション : </b>" + "<a href=\"https://www.google.com/maps/search/?api=1&query=\" target=\"_blank\">" + location + "</a>" +
                            "<br><b>メッセージ  \t: </b><br>" + message + "<br><br>");

        }
    }

    private void handleDataMessage(JSONObject json) {
        SharedPreferences sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("isbackground");
            String location = data.getString("location");
            String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "isBackground: " + location);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);


            if (!ETNotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(ETConfig.PUSH_NOTIFICATION);
                pushNotification.putExtra("title", title);
                pushNotification.putExtra("message", message);
                pushNotification.putExtra("location", location);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                notificationUtils = new ETNotificationUtils(getApplicationContext());
                if (sharedPreferences.getString("ETCheckSound", "").equals("ON")) {
                    notificationUtils.playNotificationSound();
                }
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), ETMainActivity.class);
                resultIntent.putExtra("title", title);
                resultIntent.putExtra("message", message);
                resultIntent.putExtra("location", location);

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, location, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, location, timestamp, resultIntent, imageUrl);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationBG(Context context, String title, String message, String location, Intent intent) {
        notificationUtils = new ETNotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        notificationUtils.showNotificationBG(title, message, location, intent);
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String location, String timeStamp, Intent intent) {
        SharedPreferences sharedPreferences = getSharedPreferences(ETConfig.SHARED_PREF, MODE_PRIVATE);

        notificationUtils = new ETNotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, location, timeStamp, intent);

        if (sharedPreferences.getString("ETCheckSound", "").equals("ON")) {
            // play notification sound
            notificationUtils.playNotificationSound();
        }
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String location, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new ETNotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, location, timeStamp, intent, imageUrl);
    }
}

