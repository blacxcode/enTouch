package com.blacxcode.entouch.data;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by blacXcode on 2/4/2018.
 */

// [START EBIOTData_class]
@IgnoreExtraProperties
public class ETClient {

    public String devicekey;
    public String devicename;
    public String devicestatus;
    public String statusdate;
    public String username;

    public ETClient() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public ETClient(String devicekey, String devicename, String devicestatus, String statusdate, String username) {
        this.devicekey = devicekey;
        this.devicename = devicename;
        this.devicestatus = devicestatus;
        this.statusdate = statusdate;
        this.username = username;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("devicekey", devicekey);
        result.put("devicename", devicename);
        result.put("devicestatus", devicestatus);
        result.put("statusdate", statusdate);
        result.put("username", username);

        return result;
    }
    // [END post_to_map]
}
// [END ETData_class]