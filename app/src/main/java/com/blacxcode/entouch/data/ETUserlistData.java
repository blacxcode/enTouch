package com.blacxcode.entouch.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by blacXcode on 2/2/2018.
 */

public class ETUserlistData implements Parcelable {
    @SerializedName("userid")
    @Expose
    private String userid;

    @SerializedName("devicekey")
    @Expose
    private String devicekey;

    @SerializedName("devicename")
    @Expose
    private String devicename;

    @SerializedName("devicestatus")
    @Expose
    private String devicestatus;

    @SerializedName("statusdate")
    @Expose
    private String statusdate;

    @SerializedName("username")
    @Expose
    private String username;

    public ETUserlistData() {
        // Required empty public constructor
    }

    private ETUserlistData(Parcel in) {
        userid = in.readString();
        devicekey = in.readString();
        devicename = in.readString();
        devicestatus = in.readString();
        statusdate = in.readString();
        username = in.readString();
    }

    public static final Creator<ETUserlistData> CREATOR = new Creator<ETUserlistData>() {

        @Override
        public ETUserlistData createFromParcel(Parcel in) {
            return new ETUserlistData(in);
        }

        public ETUserlistData[] newArray(int size) {
            return new ETUserlistData[size];
        }
    };

    public String getUserId() {
        return userid;
    }

    public void setUserId(String userid) {
        this.userid = userid;
    }

    public String getDevicekey() {
        return devicekey;
    }

    public void setDevicekey(String devicekey) {
        this.devicekey = devicekey;
    }

    public String getDeviceName() {
        return devicename;
    }

    public void setDeviceName(String devicename) {
        this.devicename = devicename;
    }

    public String getDeviceStatus() {
        return devicestatus;
    }

    public void setDeviceStatus(String devicestatus) {
        this.devicestatus = devicestatus;
    }

    public String getStatusDate() {
        return statusdate;
    }

    public void setStatusDate(String statusdate) {
        this.statusdate = statusdate;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userid);
        dest.writeString(username);
        dest.writeString(devicestatus);
        dest.writeString(devicekey);
        dest.writeString(devicename);
        dest.writeString(statusdate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "UserlistData{" +
                "userid='" + userid + '\'' +
                ", username='" + username + '\'' +
                ", status='" + devicestatus + '\'' +
                ", devicekey='" + devicekey + '\'' +
                ", devicename='" + devicename + '\'' +
                ", statusdate='" + statusdate + '\'' +
                '}';
    }
}
