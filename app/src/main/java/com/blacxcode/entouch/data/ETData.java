package com.blacxcode.entouch.data;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by blacXcode on 2/6/2018.
 */
// [START ETData_class]
@IgnoreExtraProperties
public class ETData {
    public String iotras;
    public String iotrst;

    public ETData() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public ETData(String iotras, String iotrst) {
        this.iotras = iotras;
        this.iotrst = iotrst;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("iotras", iotras);
        result.put("iotrst", iotrst);

        return result;
    }
    // [END post_to_map]
}
// [END ETData_class]
